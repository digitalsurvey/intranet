#!/bin/bash
echo "Pulling from Git server"
git pull
echo "Finished pulling"
rm -r /var/www/intranet/*
cp -r /home/intranet/frontend/intranet_app/dist/intranet_app/* /var/www/intranet
echo "finished uploading Website to server"
echo "Restarting nginx"
systemctl restart nginx.service
echo "Restarting database"
systemctl restart intranet.service
echo "Finished all Tasks, Website should be running"