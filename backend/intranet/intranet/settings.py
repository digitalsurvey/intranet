"""
Django settings for intranet project.

Generated by 'django-admin startproject' using Django 4.0.1.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

from distutils.log import debug
import os
from pathlib import Path

import os #for gettin the spatial libraries on windows
if os.name == 'nt':
    import platform
    OSGEO4W = r"C:\OSGeo4W"
   #  if '64' in platform.architecture()[0]:
   #      OSGEO4W += "64"
    assert os.path.isdir(OSGEO4W), "Directory does not exist: " + OSGEO4W
    os.environ['OSGEO4W_ROOT'] = OSGEO4W
    os.environ['GDAL_DATA'] = OSGEO4W + r"\share\gdal"
    os.environ['PROJ_LIB'] = OSGEO4W + r"\share\proj"
    os.environ['PATH'] = OSGEO4W + r"\bin;" + os.environ['PATH']

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-23%a210awru*e3%$hqn0&ry^x*^$51e6vjo0ky%#&bhur+r@66'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['192.168.32.105','*']

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES' : (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    )
}
# Application definition

INSTALLED_APPS = [
    'corsheaders',
    'rest_framework',
    'intranet_app',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    "colorfield",
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST =(
    'http://localhost:8081',
    'http://localhost:8000',
    'http://intranet',
    'http://localhost'
    )

ROOT_URLCONF = 'intranet.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'intranet.wsgi.application'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': 'intranet',
    #     'USER': 'adm_intranet',
    #     'PASSWORD' : 'admin',
    #     'HOST' : '192.168.32.105',
    #     #'HOST': 'localhost',
    #     'PORT' : '',
    # },

    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'intranet',
        'USER': 'adm_intranet',
        'PASSWORD' : 'admin',
        'HOST': 'localhost',
        'PORT' : '',
    }
}


# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = 'de-CH'

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = 'static/'

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Django Logging Information
LOGGING = {
    # Define the logging version
    'version': 1,
    # Enable the existing loggers
    'disable_existing_loggers': False,

    # Define the handlers
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'filename': 'sys.log',
            'level': 'DEBUG',
        },
        'file1': {
            'class': 'logging.FileHandler',
            'filename': 'sys1.log',
            'level': 'DEBUG',
        },
        'file2': {
            'class': 'logging.FileHandler',
            'filename': 'sys2.log',
            'level': 'DEBUG',
        },
    },

   # Define the loggers
    'loggers': {
        # 'django':{
        #     'handlers': ['file'],
        #     'level': 'DEBUG',
        # },
        'intranet':{
            'handlers': ['file1'],
            'level': 'DEBUG',
        },
        'intranet_app':{
            'handlers': ['file1'],
            'level': 'DEBUG',
        },
        # '':{
        #     'handlers': ['file2'],
        #     'level': 'DEBUG',
        # }      
    }
}