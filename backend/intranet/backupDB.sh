#!/bin/bash
current_time=$(date "+%Y%m%d")
file_name=./backups/backup_$current_time.json
python3 -Xutf8 manage.py dumpdata --natural-foreign --natural-primary -e contenttypes -e auth.Permission --indent 2 -o $file_name