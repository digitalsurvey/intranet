# Generated by Django 4.0.1 on 2022-01-27 10:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('intranet_app', '0009_alter_department_company_alter_department_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('date', models.DateField()),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='intranet_app.employee')),
            ],
        ),
        migrations.CreateModel(
            name='manager',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='intranet_app.department')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='intranet_app.employee')),
            ],
        ),
    ]
