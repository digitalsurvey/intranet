# Generated by Django 4.0.1 on 2022-02-15 09:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('intranet_app', '0022_alter_fieldtask_orderdescription'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fieldtask',
            name='morning',
        ),
        migrations.AddField(
            model_name='fieldtask',
            name='time',
            field=models.CharField(choices=[('morning', 'Vormittag'), ('afternoon', 'Nachmittag'), ('day', 'Ganzer Tag')], default='morning', max_length=20),
            preserve_default=False,
        ),
    ]
