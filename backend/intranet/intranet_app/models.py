
from time import strftime
from django.db import models
from colorfield.fields import ColorField
# Create your models here.


class canton(models.Model):
    def __str__(self):
        return self.name
    
    name = models.CharField(max_length=100)
    abbreviation = models.CharField(max_length=5)
    active = models.BooleanField(default=True)


class location(models.Model):
    name = models.CharField(max_length=200)
    street = models.CharField(max_length=200)
    zip = models.CharField(max_length=6)
    city = models.CharField(max_length=200)
    canton = models.ForeignKey(canton, on_delete=models.PROTECT)
    active = models.BooleanField(default=True)


    def __str__(self):
        return self.name

class company(models.Model):

    name = models.CharField(max_length=200)
    shortname = models.CharField(max_length=10)    
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class role(models.Model):
    role = models.CharField(max_length=200)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.role

class department(models.Model):
    name = models.CharField(max_length=200)
    company = models.ForeignKey(company, on_delete=models.PROTECT)
    shortname = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    active = models.BooleanField(default=True)
    color = ColorField(default='#FF0000')

    def __str__(self):
        return self.company.shortname +  ' ' + self.name

class employee(models.Model):    
    def __str__(self):
        return self.firstname + ' ' + self.lastname

    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    username = models.CharField(max_length = 10)
    department = models.ForeignKey(department, on_delete=models.PROTECT)
    workplace = models.ForeignKey(location, on_delete=models.PROTECT)
    role = models.ForeignKey(role, on_delete=models.PROTECT)
    birthday = models.DateField()
    phoneInt = models.IntegerField()
    phoneMob = models.IntegerField()
    email = models.EmailField()
    active = models.BooleanField(default=True)

class manager(models.Model):
    def __str__(self):
        return self.employee.__str__() + " is manager of " + self.department.__str__()
    employee = models.ForeignKey(employee, on_delete=models.PROTECT)
    department = models.ForeignKey(department, on_delete=models.PROTECT)
    date_from = models.DateField()
    date_to = models.DateField(null=True,blank=True,)
    active = models.BooleanField(default=True)



class task(models.Model):
    def __str__(self):
        return self.name + ' for '+ self.employee.username
    employee = models.ForeignKey(employee, on_delete=models.PROTECT)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200,null=True,blank=True)
    date = models.DateField()
    active = models.BooleanField(default=True)
    homeOffice = models.BooleanField(default=False)

class car(models.Model):
    def __str__(self):
        return self.abbreviation + " at " + self.location.name 
    abbreviation = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    company = models.ForeignKey(company, on_delete=models.PROTECT)
    location = models.ForeignKey(location, on_delete=models.PROTECT)
    licensePlate = models.CharField(max_length=10)
    active = models.BooleanField(default=True)
    color = ColorField(default='#FF0000')

class tachy(models.Model):
    def __str__(self):
        return self.abbreviation + " at " + self.location.name
    abbreviation = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    serial = models.CharField(max_length=20)
    functions = models.CharField(max_length=50, null=True, blank=True, default='')
    location = models.ForeignKey(location, on_delete=models.PROTECT)
    active = models.BooleanField(default=True)
    color = ColorField(default='#FF0000')



class gnss(models.Model):
    def __str__(self):
        return self.abbreviation + " at " + self.location.name
    abbreviation = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    serial = models.CharField(max_length=20)
    functions = models.CharField(max_length=50, null=True, blank=True,default='')
    location = models.ForeignKey(location, on_delete=models.PROTECT)
    active = models.BooleanField(default=True)
    color = ColorField(default='#FF0000')


class fieldTask(models.Model):
    def __str__(self):
        return  self.orderDescription  +' ' + self.date.strftime("%x")
    timeChoices = (
        ('morning', 'Vormittag'),
        ('afternoon', 'Nachmittag'),
        ('day', 'Ganzer Tag'),
        ('night', 'Nacht')  
    )
    orderNO = models.CharField(max_length=20, null=True,blank=True,)
    orderDescription = models.TextField()
    date = models.DateField()
    time = models.CharField(max_length=20, choices=timeChoices)
    location = models.ForeignKey(location, on_delete=models.PROTECT)
    operator = models.ForeignKey(employee, on_delete=models.PROTECT, null=True,blank=True,)
    carAmount = models.PositiveIntegerField()
    tachyAmount = models.PositiveIntegerField()
    gnssAmount = models.PositiveIntegerField()
    employeeAmount = models.PositiveIntegerField()
    comment= models.CharField(max_length=100, null=True, blank=True,)
    released = models.BooleanField(default=False)

class deviceForTask(models.Model):
    def __str__(self):
        return "devices for " + self.fieldTask.orderDescription
    fieldTask = models.ForeignKey(fieldTask, on_delete=models.CASCADE)
    car = models.ForeignKey(car, on_delete=models.PROTECT, null=True,blank=True,)
    tachy = models.ForeignKey(tachy, on_delete=models.PROTECT, null=True,blank=True,)
    gnss = models.ForeignKey(gnss, on_delete=models.PROTECT, null=True,blank=True,)
    employee = models.ForeignKey(employee, on_delete=models.PROTECT, null=True,blank=True,)

class news(models.Model):
    def __str__(self):
        return self.text
    text = models.TextField()
    date = models.DateField()
    department = models.ForeignKey(department, on_delete=models.PROTECT, null=True, blank=True)

class absenceType(models.Model):
    def __str__(self):
        return self.type
   #  choices = (
   #      ('vacation', 'Ferien'),
   #      ('army', 'Militär'),
   #      ('school', 'Schule'),
   #      ('sick', 'Krank')  
   #  )
    type = models.CharField(max_length=20)
    confirm = models.BooleanField()
    active = models.BooleanField(default=True)
    color = ColorField(default='#FF0000')


class absence(models.Model):
    def __str__(self):
        return self.type.type + ' for ' + self.employee.username

    statusChoices = (
    ('denied', 'abgelehnt'),
    ('accepted', 'genehmigt'),
    ('open', 'offen')
    )
    start = models.DateField()
    end = models.DateField()
    type = models.ForeignKey(absenceType, on_delete=models.PROTECT)
    employee = models.ForeignKey(employee, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=statusChoices, default='open')
    recursiveID = models.IntegerField(null=True, blank=True)
    recursive = models.BooleanField(default=False, null=True)

class zuständigkeiten(models.Model):
	def __str__(self):
		return self.gemeinde + ', ' + self.thema
	gemeinde = models.CharField(max_length=50)
	thema = models.CharField(max_length=50)
	person = models.CharField(max_length=100)
	stellvertretung = models.CharField(max_length=100, null=True, blank=True)
