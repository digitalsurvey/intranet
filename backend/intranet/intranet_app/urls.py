"""intranet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

app_name = 'intranet_app'
urlpatterns= [   
    path('', views.home.as_view(), name="home"),
    path('api/employee/<int:pk>', views.employeeByID),
    path('api/employee', views.allEmployee),
    path('api/task', views.allTask),
    path('api/task/<int:pk>', views.taskByID),
    path('api/task/employee/<int:pk>', views.taskByEmployee),
    path('api/department', views.allDepartments),
    path('api/location', views.allLocations),
    path('api/company', views.allCompanies),
    path('api/tachy', views.allTachys),
    path('api/gnss', views.allGNSS),
    path('api/car', views.allCars),
    path('api/fieldTask', views.allFieldTasks),
    path('api/fieldTask/<int:pk>', views.fieldTaskByID),
    path('api/fieldTask/<start>&<end>', views.weeklyFieldTasks),
    path('api/fieldTask/<start>&<end>/released', views.getReleasedWeeklyFieldTasks),
    # path('api/fieldTask/<int:id>/released', views.patchReleased),
    path('api/fieldTask/<int:taskID>/selected/employee', views.getSelectedEmployee),
    path('api/fieldTask/<int:taskID>/selected/car', views.getSelectedCar),
    path('api/fieldTask/<int:taskID>/selected/tachy', views.getSelectedTachy),
    path('api/fieldTask/<int:taskID>/selected/gnss', views.getSelectedGnss),
    path('api/deviceForTask', views.allDeviceForTasks),
    path('api/deviceForTask/<int:deviceForTaskID>', views.deviceForTaskByID),
    path('api/deviceForTask/task/<int:fieldTaskID>', views.getDeviceForTaskByTaskID),
    path('api/available/employee/<date>&<time>&<locationID>&<int:ignoreNumber>', views.getAvailableEmployees),
    path('api/available/tachy/<date>&<time>&<locationID>&<int:ignoreNumber>', views.getAvailableTachys),
    path('api/available/gnss/<date>&<time>&<locationID>&<int:ignoreNumber>', views.getAvailableGNSS),
    path('api/available/car/<date>&<time>&<locationID>&<int:ignoreNumber>', views.getAvailableCars),
    path('api/news/<start>&<end>', views.getNews),
    path('api/news', views.createNews),
    path('api/news/<int:id>', views.deleteNews),
	path('api/absence/<int:empId>&<startDate>&<endDate>', views.absenceByEmployeeByDate),
	path('api/absence/type', views.getAbsenceType),
	path('api/absence', views.allAbsences),
    path('api/absence/recursive/<int:recursiveID>', views.recursiveAbsencesById),
    path('api/absence/<int:absenceId>', views.absenceById),
	path('api/mail', views.mailView),
	path('api/responsibilities', views.allZuständigkeiten),
]
