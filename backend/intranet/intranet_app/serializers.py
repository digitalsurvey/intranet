from dataclasses import field, fields
from rest_framework import serializers
from intranet_app.models import *
from drf_writable_nested.serializers import WritableNestedModelSerializer
class locationSerializer(serializers.ModelSerializer):
    class Meta: 
        model = location
        fields = '__all__'

class departmentSerializer(serializers.ModelSerializer):
    class Meta:
        model= department
        fields = '__all__'

class roleSerializer(serializers.ModelSerializer):
    class Meta:
        model = role
        fields= '__all__'

# class employeeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = employee
#         fields = '__all__'
class employeeSerializer(WritableNestedModelSerializer):
    role = roleSerializer(many=False, required=False)
    department = departmentSerializer(many=False, required=True)
    class Meta:
        model = employee
        fields = '__all__'

class cantonSerialiizer(serializers.ModelSerializer):
    class Meta:
        model = canton
        fields = '__all__'

class companySerializer(serializers.ModelSerializer):
    class Meta:
        model = company
        fields = '__all__'

class taskSerializer(serializers.ModelSerializer):
    class Meta:
        model = task
        fields = '__all__'


class carSerializer(serializers.ModelSerializer):
    class Meta:
        model = car
        fields = '__all__'


class tachySerializer(serializers.ModelSerializer):
    class Meta:
        model = tachy
        fields = '__all__'


class gnssSerializer(serializers.ModelSerializer):
    class Meta:
        model = gnss
        fields = '__all__'


class fieldTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = fieldTask
        fields = '__all__'

class deviceForTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = deviceForTask
        fields = '__all__'

class newsSerializer(serializers.ModelSerializer):
    class Meta:
        model = news
        fields = '__all__'

class absenceTypeSerializer(serializers.ModelSerializer):
    class Meta: 
        model = absenceType
        fields= '__all__'

class absenceSerializer(WritableNestedModelSerializer):
    employee = employeeSerializer(many=False, required=True)
    type = absenceTypeSerializer(many=False, required=True)
    class Meta: 
        model = absence
        fields = '__all__'


class zuständigkeitenSerializer(serializers.ModelSerializer):
	class Meta:
		model = zuständigkeiten
		fields = '__all__'