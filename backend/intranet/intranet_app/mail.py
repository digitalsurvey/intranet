from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib, ssl
from time import strftime
from intranet_app.mailSettings import *
import locale
import logging
locale.setlocale(locale.LC_TIME, "de_CH")
logger=logging.getLogger(__name__)


def sendManagerMail(employee, managerMail, type, start, end):
    server = smtplib.SMTP(smtpServer, port=port)
    server.starttls()
    server.login(userName, password)
    message = MIMEMultipart("alternative")
    message["From"] = userName
    subject = employee.username + " beantragt " + type
    text,html = createManagerText(employee.username, type, start, end)
    textPart = MIMEText(text, "plain")
    htmlPart = MIMEText(html, "html")
    message["To"]=managerMail
    message['Subject']=subject
    message.attach(textPart)
    message.attach(htmlPart) 
    logger.warning("sending {} to {}".format(textPart,managerMail))
    server.sendmail(userName,managerMail,message.as_string())

def sendEmployeeMail(employeeMail, type, status, start, end ):
    server = smtplib.SMTP(smtpServer, port=port)
    server.starttls()
    server.login(userName, password)
    message = MIMEMultipart("alternative")
    message["From"]=userName
    subject = "Statusänderung für " + type + ""	
    message["Subject"]=subject
    message["To"]=employeeMail
    if(status=="denied"):
        statusName="abgelehnt"
    else:
        statusName="angenommen"
    text,html = createEmployeeText(type, statusName, start, end)
    textPart = MIMEText(text, "plain")
    htmlPart = MIMEText(html, "html")
    message.attach(textPart)
    message.attach(htmlPart)
    server.sendmail(userName,employeeMail,message.as_string())

def sendRecursiveManagerMail(dates, employee, managerMail, type):
    server = smtplib.SMTP(smtpServer, port=port)
    server.starttls()
    server.login(userName, password)       
    message = MIMEMultipart("alternative")
    message["From"] = userName
    subject = employee.username + " beantragt " + type.type
    text,html = createRecursiveManagerText(employee.username, type, dates)
    textPart = MIMEText(text, "plain")
    htmlPart = MIMEText(html, "html")
    message["To"]=managerMail
    message['Subject']=subject
    message.attach(textPart)
    message.attach(htmlPart)
    try:
        server.sendmail(userName,managerMail,message.as_string())
        logger.warning("sucessfully sent mail")
    except Exception as e:
        logger.warning("no mail sent")
        logger.warning(e)

def sendRecursiveEmployeeMail(employeeMail, dates, type, newStatus):
    server=smtplib.SMTP(smtpServer, port=port)
    server.starttls()
    server.login(userName, password)
    message = MIMEMultipart("alternative")
    message["From"] = userName
    subject = "Statusänderung für " + type
    if(newStatus=="denied"):
        statusName="abgelehnt"
    else:
        statusName="angenommen"

    text,html = createRecursiveEmployeeText(type, dates, statusName)
    textPart = MIMEText(text, "plain")
    htmlPart = MIMEText(html, "html")
    message["To"]=employeeMail
    message['Subject']=subject
    message.attach(textPart)
    message.attach(htmlPart)
    try: 
        server.sendmail(userName, employeeMail, message.as_string())
    except Exception as e:
        logger.warning(e)
    server.sendmail(userName, employeeMail, message.as_string())

def createRecursiveEmployeeText(type, dates, newStatus):
	text="""\
		Die Abwesenheiten vom Typ {0} von
	""".format(type)
	html="""\
	<html>
		<body>
			<p> Die Abwesenheiten vom Typ <b>{0}</b> von </p>
		""".format(type)
	for d in dates:
		start= datetime.strftime(d["start"], "%a %d.%m.%Y")
		end= datetime.strftime(d["end"], "%a %d.%m.%Y")
		text+= """
			{0} bis {1}
		""".format(start,end)
		html+="""\
			{0} bis {1} <br>	
		""".format(start,end)
	text+="""\
		haben nun den Status: {0}
		""".format(newStatus)
	html+="""\
		haben nun den Status <b>{0}</b>
		</body>
	</html>
	""".format(newStatus)
	return text,html


def createRecursiveManagerText(username, absenceType, dates):
	text="""\
		Der Mitarbeiter {0} beantragt {1}
		""".format(username, absenceType)
	html="""
	<html>
		<body>
			<p> Der Mitarbeiter <b>{0}</b> beantragt <b>{1}</b> von </p>
	""".format(username,absenceType)
	for date in dates:
		logger.warning(date)
		start=datetime.strptime(date['start'], "%Y-%m-%d").strftime("%a %d.%m.%Y")
		end=datetime.strptime(date['end'], "%Y-%m-%d").strftime("%a %d.%m.%Y")
		text += """
		von {0} bis {1}
		""".format(start, end)
		html += """
		{0} bis {1} <br>
		""".format(start, end)
	if(absenceType.type=='Ferien'):
		text += """
			Diese Abwesenheit muss im Intranet noch bestätigt werden
			http://intranet/#/managerAbsences
			"""
		html += """
		Diese Abwesenheit muss im Intranet noch bestätigt werden<br>
		<a href="http://intranet/#/managerAbsences">Abwesenheiten</a>
		"""
	html+="""
		</body>
	</html>
	"""
	return text,html

def createEmployeeText(type, status, start, end):
	startDate=start.strftime("%a %d.%m.%Y")
	endDate=end.strftime("%a %d.%m.%Y")

	text = """\
	{0} von {1} bis {2} hat neu den status; {3}	
	""".format(type, startDate, endDate, status)
	html="""
	<html>
		<body>
			<b>{0}</b> von <b>{1}</b> bis <b>{2}</b> hat neu den Status: <b>{3}</b>
		</body>
	</html>
	""".format(type,startDate,endDate, status)
	return text,html


def createManagerText(username, type, start, end):
	startDate=datetime.strptime(start, "%Y-%m-%d").strftime("%a %d.%m.%Y")
	endDate=datetime.strptime(end, "%Y-%m-%d").strftime("%a %d.%m.%Y")
	if(type=="Ferien"):
		text = """\
		Der Mitarbeiter {0} beantragt {1} von 	
		{2} 
		bis
		{3}
		Diese Abwesenheit muss im Intranet noch bestätigt werden
		""".format(username, type, startDate, endDate)
		html="""
		<html>
			<body>
				<p> Der Mitarbeiter <b>{0}</b> beantragt <b>{1}</b> von </p>
				<p> {2} </p>
				bis
				<p> {3} </p>
				<p> Diese Abwesenheit muss im Intranet noch bestätigt werden </p>
				<a href="http://intranet/#/managerAbsences">Abwesenheiten</a>
			</body>
		</html>
		""".format(username,type,startDate,endDate)
	else:
		text = """
		Der Mitarbeiter {0} beantragt {1} von 	
		{2} 
		bis
		{3}""".format(username, type, startDate,endDate)
		html="""
		<html>
			<body>
				<p> Der Mitarbeiter <b>{0}</b> beantragt <b>{1}</b> von </p>
				<p> {2} </p>
				bis
				<p> {3} </p>
			</body>
		</html>
		""".format(username,type,startDate,endDate)
	return text,html
