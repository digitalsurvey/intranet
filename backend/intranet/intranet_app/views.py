from datetime import datetime
from email import message
from functools import partial
from time import time
from django.shortcuts import get_object_or_404, render, HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.db.models import Q

from intranet_app.models import *
from intranet_app.serializers import *

from django.views.decorators.csrf import csrf_exempt
from django.views import generic

from intranet_app.models import *

from django.shortcuts import render
from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from rest_framework.decorators import api_view

from intranet_app.mail import *
# Create your views here.
import logging
logger=logging.getLogger(__name__)


class home(generic.ListView):
	model = employee
	template_name = 'intranet_app/home.html' 

@api_view(['GET','POST'])
def allEmployee(request):
	if request.method == 'GET':
		emp = employee.objects.filter(active=True).order_by('department','username')
		serial = employeeSerializer(emp, many=True)
		return JsonResponse(serial.data, safe=False)
	if request.metohd == 'POST':
		data = JSONParser().parse(request)
		serial = employeeSerializer(data=data)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_201_CREATED)
		return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)

	return employee.objects.all()

@api_view(['GET','POST','DELETE'])
def employeeByID(request, pk):
	if request.method == 'GET':
		emp = employee.objects.get(pk=pk)
		serial = employeeSerializer(emp)
		return JsonResponse(serial.data, safe=False)

@api_view(['GET','POST'])
def allTask(request):
	if request.method == 'GET':
		tsk = task.objects.all()
		serial = taskSerializer(tsk, many=True)
		return JsonResponse(serial.data, safe = False)
	if request.method == 'POST':
		data = JSONParser().parse(request)
		serial = taskSerializer(data=data)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_201_CREATED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET','DELETE','PATCH'])
def taskByID(request,pk):
	if request.method == 'GET':
		tsk = task.objects.get(pk=pk)
		serial = taskSerializer(tsk)
		return JsonResponse(serial.data, safe = False)
	if request.method == 'DELETE':
		tsk = task.objects.get(pk=pk)
		tsk.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)
	if request.method == 'PATCH':
		data=JSONParser().parse(request)
		tsk = task.objects.get(pk=pk)
		serial = taskSerializer(tsk, data=data, partial=True)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)

		

@api_view(['GET','POST','DELETE'])
def taskByEmployee(request, pk):
	if request.method == 'GET':
		tasks = employee.objects.get(pk=pk).task_set.all()
		serial = taskSerializer(tasks, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['GET'])
def allDepartments(request):
	deps = department.objects.filter(active=True).order_by('name')
	serial = departmentSerializer(deps, many=True)
	return JsonResponse(serial.data, safe=False)
@api_view(['GET'])
def allLocations(request):
	locs = location.objects.filter(active=True).order_by('name')
	serial = locationSerializer(locs, many=True)
	return JsonResponse(serial.data, safe=False)
@api_view(['GET'])
def allCompanies(request):
	comps = company.objects.filter(active=True).order_by('name')
	serial = companySerializer(comps, many=True)
	return JsonResponse(serial.data, safe=False)

@api_view(['GET'])
def allGNSS(request):
    g = gnss.objects.filter(active=True)
    serial = gnssSerializer(g, many=True)
    return JsonResponse(serial.data, safe=False)

@api_view(['GET'])
def allTachys(request):
    t = tachy.objects.filter(active=True)
    serial = tachySerializer(t, many=True)
    return JsonResponse(serial.data, safe=False)

@api_view(['GET'])
def allCars(request):
    c = car.objects.filter(active=True)
    serial = carSerializer(c, many=True)
    return JsonResponse(serial.data, safe=False)


@api_view(['GET','PATCH','DELETE'])
def fieldTaskByID(request,pk):
	task = fieldTask.objects.get(pk=pk)
	if request.method == "GET":
		serial = fieldTaskSerializer(task)
		return JsonResponse(serial.data, safe=False)
	if request.method == 'PATCH':
		data=JSONParser().parse(request)
		serial = fieldTaskSerializer(task, data=data, partial=True)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.data, status=status.HTTP_400_BAD_REQUEST)
	if request.method == 'DELETE':
		task.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def getSelectedEmployee(request,taskID):
	if request.method == "GET":
		ids=[]
		employees = deviceForTask.objects.filter(fieldTask=taskID).values('employee')
		for e in employees: 
			ids.append(e.get('employee'))
		selectedEmployees = employee.objects.filter(id__in=ids)
		serial = employeeSerializer(selectedEmployees, many=True,)
		return JsonResponse(serial.data,safe=False)

def getSelectedCar(request,taskID):
	if request.method == "GET":
		ids=[]
		cars = deviceForTask.objects.filter(fieldTask=taskID).values('car')
		for c in cars: 
			ids.append(c.get('car'))
		selectedCars = car.objects.filter(id__in=ids)
		serial = carSerializer(selectedCars, many=True,)
		return JsonResponse(serial.data,safe=False)

def getSelectedGnss(request,taskID):
	if request.method == "GET":
		ids=[]
		gnssArr = deviceForTask.objects.filter(fieldTask=taskID).values('gnss')
		for g in gnssArr: 
			ids.append(g.get('gnss'))
		selectedGnss = gnss.objects.filter(id__in=ids)
		serial = gnssSerializer(selectedGnss, many=True,)
		return JsonResponse(serial.data,safe=False)

def getSelectedTachy(request,taskID):
	if request.method == "GET":
		ids=[]
		tachys = deviceForTask.objects.filter(fieldTask=taskID).values('tachy')
		for t in tachys: 
			ids.append(t.get('tachy'))
		selectedTachys = tachy.objects.filter(id__in=ids)
		serial = tachySerializer(selectedTachys, many=True,)
		return JsonResponse(serial.data,safe=False)


@api_view(['GET','POST'])
def allFieldTasks(request):
    if request.method == "GET":
        tasks = fieldTask.objects.all().order_by('date','time')
        serial = fieldTaskSerializer(tasks, many=True)
        return JsonResponse(serial.data, safe=False)
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serial = fieldTaskSerializer(data=data)
        if serial.is_valid():
            serial.save()
            return JsonResponse(serial.data, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def weeklyFieldTasks(request,start,end):
	if request.method == 'GET':
		tasks = fieldTask.objects.filter(date__range=(start,end)).order_by('date','time')
		serial = fieldTaskSerializer(tasks, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['GET'])
def getReleasedWeeklyFieldTasks(request,start,end):
	if request.method == 'GET':
		tasks = fieldTask.objects.filter(date__range=(start,end),released=True)
		serial = fieldTaskSerializer(tasks, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['POST'])
def allDeviceForTasks(request):
	if request.method == 'POST':
		data = JSONParser().parse(request)
		serial = deviceForTaskSerializer(data=data)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_201_CREATED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)
			
@api_view(['GET','PATCH','DELETE'])
def deviceForTaskByID(request,deviceForTaskID):
	entry = deviceForTask.objects.get(pk=deviceForTaskID)
	if request.method == 'GET':
		serial = deviceForTaskSerializer(entry)
		return JsonResponse(serial.data, safe=False)
	if request.method == 'PATCH':
		data=JSONParser().parse(request)
		serial = deviceForTaskSerializer(entry, data=data, partial=True)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.data, status=status.HTTP_400_BAD_REQUEST)
	if request.method == 'DELETE':
		entry = deviceForTask.objects.get(pk=deviceForTaskID)
		entry.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)
		
@api_view(['GET'])
def getDeviceForTaskByTaskID(request,fieldTaskID):
	if request.method == 'GET':
		entries = deviceForTask.objects.filter(fieldTask=fieldTaskID)
		serial = deviceForTaskSerializer(entries, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['PATCH'])
def patchReleased(request,id):
	if request.method == 'PATCH':
		task = fieldTask.objects.get(pk=id)
		data = JSONParser().parse(request)
		serial = fieldTaskSerializer(task, data=data, partial=True)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getAvailableTachys(request,date,time,locationID,ignoreNumber):
	ids=[]
	if request.method == 'GET':
		if time=="day":
			tasks = fieldTask.objects.filter(date=date).exclude(time='night')
		elif time=="night":
			tasks = fieldTask.objects.filter(date=date, time=time)
		else:
			tasks= fieldTask.objects.filter(Q(date=date, time=time)| Q(date=date, time='day')).values('id')
		tasks = tasks.exclude(id=ignoreNumber) #ignore Planning of task with this ID
		usedTachys = deviceForTask.objects.filter(fieldTask__in=tasks).exclude(tachy__isnull=True).values('tachy')
		for t in usedTachys: 
			ids.append(t.get('tachy'))
		availableTachys = tachy.objects.filter(location=locationID).exclude(id__in=ids)
		serial = tachySerializer(availableTachys, many=True,)
		return JsonResponse(serial.data,safe=False)
		

def getAvailableGNSS(request,date,time,locationID,ignoreNumber):
	if request.method == 'GET':
		ids=[]
		if time=="day":
			tasks = fieldTask.objects.filter(date=date).exclude(time='night')
		elif time=="night":
			tasks = fieldTask.objects.filter(date=date, time=time)
		else:
			tasks= fieldTask.objects.filter(Q(date=date, time=time)| Q(date=date, time='day')).values('id')
		tasks = tasks.exclude(id=ignoreNumber) #ignore Planning of task with this ID
		usedGnss = deviceForTask.objects.filter(fieldTask__in=tasks).exclude(gnss__isnull=True).values('gnss')
		for g in usedGnss: 
			ids.append(g.get('gnss'))
		availableGnss = gnss.objects.filter(location=locationID,active=True).exclude(id__in=ids)
		serial = gnssSerializer(availableGnss, many=True)
		return JsonResponse(serial.data,safe=False)

def getAvailableCars(request,date,time,locationID,ignoreNumber):
	if request.method == 'GET':
		ids=[]
		if time=="day":
			tasks = fieldTask.objects.filter(date=date).exclude(time='night')
		elif time=="night":
			tasks = fieldTask.objects.filter(date=date, time=time)
		else:
			tasks= fieldTask.objects.filter(Q(date=date, time=time)| Q(date=date, time='day')).values('id')
		tasks = tasks.exclude(id=ignoreNumber) #ignore Planning of task with this ID

		usedCars = deviceForTask.objects.filter(fieldTask__in=tasks).exclude(car__isnull=True).values('car')
		for c in usedCars: 
			ids.append(c.get('car'))
		availableCars = car.objects.filter(location=locationID,active=True).exclude(id__in=ids)
		serial = carSerializer(availableCars, many=True)
		return JsonResponse(serial.data,safe=False)
		

def getAvailableEmployees(request,date,time,locationID,ignoreNumber):
	if request.method == 'GET':
		ids=[]
		if time=="day":
			tasks = fieldTask.objects.filter(date=date).exclude(time='night')
		elif time=="night":
			tasks = fieldTask.objects.filter(date=date, time=time)
		else:
			tasks= fieldTask.objects.filter(Q(date=date, time=time)| Q(date=date, time='day'))
		tasks = tasks.exclude(id=ignoreNumber) #ignore Planning of task with this ID
		for task in tasks:
			ids.append(task.operator.id)
		usedEmployees = deviceForTask.objects.filter(fieldTask__in=tasks.values('id')).exclude(employee__isnull=True).values('employee')
		for e in usedEmployees:
			ids.append(e.get('employee'))
		absentEmployees = absence.objects.exclude(status="denied").exclude(employee__in=ids).filter(start__lte=date, end__gte=date)
		for absenceEntry in absentEmployees:
			ids.append(absenceEntry.employee.id)
		availableEmployees = employee.objects.filter(active=True).exclude(id__in=ids).order_by('username')
		serial = employeeSerializer(availableEmployees, many=True)
		return JsonResponse(serial.data,safe=False)

@api_view(['GET'])
def getNews(request,start,end):
	if request.method == "GET":
		weeklyNews = news.objects.filter(date__range=(start,end)).order_by('date')
		serial= newsSerializer(weeklyNews,many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['POST'])
def createNews(request):
	if request.method == "POST":
		data = JSONParser().parse(request)
		serial = newsSerializer(data=data)
		if serial.is_valid():
			serial.save()
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def deleteNews(request,id):
	if request.method== 'DELETE':
		object = news.objects.get(pk=id)
		object.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def absenceByEmployeeByDate(request, empId, startDate, endDate):
	if request.method =="GET":
		# absences = absence.objects.filter(employee=empId).filter(Q(start__gte=startDate, start__lte=endDate) | Q(end__lte=endDate, end__gte=startDate))
		absences = absence.objects.filter(employee=empId).exclude(status='denied').filter(Q(start__range=[startDate, endDate]) | Q(end__range=[startDate, endDate]) | Q(Q(start__lte=startDate, end__gte=startDate) & Q(start__lte=endDate, end__gte=endDate)))
		serial = absenceSerializer(absences, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['POST', 'GET'])
def allAbsences(request):
	today=datetime.now()
	if request.method=='POST':
		data = JSONParser().parse(request)
		serial = absenceSerializer(data=data)
		if serial.is_valid():
			serial.save()
			requester = employee.objects.get(pk=data.get("employee").get("id"))
			man = manager.objects.filter(department=(requester.department)).filter(active=True).filter(date_to__gte=today).get()
			managerMail = man.employee.email
			type = absenceType.objects.get(pk=data.get("type").get("id"))
			if(not(data['recursive'])): # when Absence is not recusrive
				try:
					sendManagerMail(requester, managerMail, type.type, data['start'], data['end'])
				except:
					logger.warning("Mail konnte nicht versandt werden")
					# return  JsonResponse({'message':"Error: Mail konnte nicht versandt werden"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
					return  JsonResponse({'message': 'Mail konnte nicht versandt werden bitte überprüfen'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else :
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)
	if request.method=='GET':
		absences = absence.objects.filter(end__gte=today).order_by('start', 'end')
		serial = absenceSerializer(absences, many=True)
		return JsonResponse(serial.data, safe=False)

@api_view(['PATCH','DELETE','GET'])
def absenceById(request,absenceId):
	if request.method == 'PATCH':
		data = JSONParser().parse(request)
		absenceEntry = absence.objects.get(pk=absenceId)
		serial = absenceSerializer(absenceEntry, data=data, partial=True)
		if serial.is_valid():
			serial.save()
			employeeMail =absenceEntry.employee.email
			type =  absenceEntry.type.type
			try:
				sendEmployeeMail(employeeMail, type, data["status"], absenceEntry.start, absenceEntry.end)
			except Exception as e:
				logger.warning("Mail konnte nicht versandt werden")
				logger.warning(e)
				return  JsonResponse({'message':"Error: Mail konnte nicht versandt werden"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
			return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)
		else:
			return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)
	if request.method =="DELETE":
		absenceObject = absence.objects.get(pk=absenceId)
		absenceObject.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)
	if request.method =="GET":
		absenceObject = absence.objects.get(pk=absenceId)
		serial = absenceSerializer(absenceObject)
		return JsonResponse(serial.data, safe=False)

@api_view(['GET','DELETE','PATCH'])
def recursiveAbsencesById(request, recursiveID):
	if request.method == 'GET':
		absenceObjects = absence.objects.filter(Q(pk=recursiveID)|Q(recursiveID=recursiveID))
		serial = absenceSerializer(absenceObjects, many=True)
		return JsonResponse(serial.data, safe=False)
	if request.method == 'DELETE':
		absenceObjects = absence.objects.filter(Q(pk=recursiveID)|Q(recursiveID=recursiveID))
		logger.warning(absenceObjects)
		logger.warning(absenceObjects.values())
		absenceObjects.delete()
		return JsonResponse({'message': 'Succesfully deleted'}, status=status.HTTP_204_NO_CONTENT)
	if request.method == 'PATCH':
		logger.warning("inside patch")
		data= JSONParser().parse(request)
		logger.warning("got data")
		logger.warning(data)
		absenceObjects = absence.objects.filter(Q(pk=recursiveID)|Q(recursiveID=recursiveID))
		logger.warning(absenceObjects.values())
		dates = []
		for a in absenceObjects:
			serial= absenceSerializer(a, data=data, partial=True)
			if serial.is_valid():
				# logger.warning("valid for")
				# logger.warning(a.id)
				dates.append({'start': a.start, 'end':a.end})
				serial.save()
				# logger.warning(dates)
			else: 
				return JsonResponse(serial.errors, status=status.HTTP_400_BAD_REQUEST)
		employeeMail = absenceObjects[0].employee.email
		# logger.warning("Got emp mail: " + employeeMail)
		type = absenceObjects[0].type.type
		# logger.warning("Got Type:" + type)
		try :
			sendRecursiveEmployeeMail(employeeMail, dates, type, data["status"])
		except Exception as e:
			logger.error(e)
			return  JsonResponse({'message':"Error: Mail konnte nicht versandt werden"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		return JsonResponse(serial.data, status=status.HTTP_202_ACCEPTED)	




@api_view(['GET'])
def getAbsenceType(request):
	if request.method=='GET':
		types = absenceType.objects.filter(active=True)
		serial = absenceTypeSerializer(types,many=True)
		return JsonResponse(serial.data, safe=False)


@api_view(['PUT', 'GET'])
def mailView(request):
	if request.method == 'PUT' or request.method == 'GET':
		data = JSONParser().parse(request)
		# logger.warning(data)
		requester = employee.objects.get(pk=data['employeeID'])
		today=datetime.now()
		man = manager.objects.filter(department=(requester.department)).filter(active=True).filter(date_to__gte=today).get()
		managerMail = man.employee.email
		type = absenceType.objects.get(pk=data['typeID'])
		try:
			sendRecursiveManagerMail(data['dates'], requester, managerMail, type)
			logger.warning("after mailsend")
			return JsonResponse({'message': "Mail succesfully sent"}, status=status.HTTP_204_NO_CONTENT)
		except Exception as e:
			print(e)
			return  JsonResponse({'message':"Error: Mail konnte nicht versandt werden"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def allZuständigkeiten(request):
	if request.method == 'GET':
		objects = zuständigkeiten.objects.all().order_by('gemeinde')
		serial = zuständigkeitenSerializer(objects, many=True)
		return JsonResponse(serial.data, safe=False)