from django.contrib import admin
from .models import *
# Register your models here.

class employeeAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class locationAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class departmentAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class roleAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class cantonAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class companyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class taskAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    
class managerAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

class carAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

class tachyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

class gnssAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

class fieldTaskAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

class deviceForTaskAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(employee, employeeAdmin)
admin.site.register(location,locationAdmin)
admin.site.register(department,departmentAdmin)
admin.site.register(role,roleAdmin)
admin.site.register(canton,cantonAdmin)
admin.site.register(company,companyAdmin)
admin.site.register(task,taskAdmin)
admin.site.register(manager,managerAdmin)
admin.site.register(car,carAdmin)
admin.site.register(tachy, tachyAdmin)
admin.site.register(gnss, gnssAdmin)
admin.site.register(fieldTask, fieldTaskAdmin)
admin.site.register(deviceForTask, deviceForTaskAdmin)
admin.site.register(absence)
admin.site.register(absenceType)
admin.site.register(zuständigkeiten)