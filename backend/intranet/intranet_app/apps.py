from django.apps import AppConfig


class IntranetAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'intranet_app'
