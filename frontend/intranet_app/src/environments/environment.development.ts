// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: "localhost",
  port: 8000,
  SSL: false,
};


//Ip HomeOffice
//  const server = '192.168.1.102'

// Hotspot Config
// const server = "192.168.190.55" 


// IP offline
// const server = 'localhost'

//IP Office
// const server = '192.168.32.105'
// const server ='intranet.intra.bsb-partner.ch'

// const SSL = true
// const SSL = false
// const port = '8000'
