export const environment = {
	production: true,
	server: "intranet.bsb-partner.ch",
	port: 8000,
	SSL: true,
};
