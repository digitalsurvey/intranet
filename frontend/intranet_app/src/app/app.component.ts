import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'intranet_app';

  toggleNav(ev: any) {
	ev.preventDefault();
	let el = document.getElementById('ds-intranet');
	el.classList.toggle('sidebar-collapsed');
 }
}

