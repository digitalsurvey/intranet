import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Car } from '../models/car/car.model';
import { DeviceForTask } from '../models/deviceForTask/device-for-task.model';
import { FieldTask } from '../models/fieldTask/field-task.model';
import { Gnss } from '../models/gnss/gnss.model';
import { Tachy } from '../models/tachy/tachy.model';
import { DateService } from '../services/date.service';
import { FieldTaskService } from '../services/field-task.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { WochenplanService } from '../services/wochenplan.service';
import { Location } from '../models/location/location.model';
import { ShowFeldeinsatzComponent } from '../modals/feldeinsatz/show-feldeinsatz/show-feldeinsatz.component';
import { EditFeldeinsatzComponent } from '../modals/feldeinsatz/edit-feldeinsatz/edit-feldeinsatz.component';
import { formatDate } from '@angular/common';

@Component({
	selector: 'app-deviceplan',
	templateUrl: './deviceplan.component.html',
	styleUrls: ['./deviceplan.component.scss']
})
export class DeviceplanComponent implements OnInit {
	@ViewChild('showFeldeinsatz') private showModalComponent: ShowFeldeinsatzComponent
	@ViewChild('editFeldeinsatz') private editModalComponent: EditFeldeinsatzComponent

	loading: boolean = true
	allTachys: Tachy[]
	allCars: Car[]
	allGnss: Gnss[]
	selectedTachys: Tachy[]
	selectedCars: Car[]
	selectedGnss: Gnss[]
	// dates: Date[]
	weeklyFieldTasks: FieldTask[] = []
	deviceForTasks: Map<FieldTask,DeviceForTask[]> = new Map()
	tachyByDay: Map<string, Map<number, FieldTask[]>>=new Map()
	carByDay: Map<string, Map<number, FieldTask[]>>=new Map()
	gnssByDay: Map<string, Map<number, FieldTask[]>>=new Map()
	locFilter: number = 0
	locations: Location[]

	
	
	constructor(public dateService: DateService, private fieldTaskService: FieldTaskService, private wochenplanService : WochenplanService) { }
	
	async ngOnInit(): Promise<void> {
		this.wochenplanService.getAllLocations().subscribe({
			next: (res) => this.locations=res
		})
		this.dateService.getDates()
		await this.loadData()
		this.getDeviceMap()
	}

	async loadData() {
		this.fieldTaskService.getAllData().subscribe(responseList => {
				this.allCars = responseList[0]
				this.allTachys = responseList[1]
				this.allGnss= responseList[2]
				console.log(this.allCars, this.allTachys, this.allGnss)

		})
		this.weeklyFieldTasks = await lastValueFrom(this.fieldTaskService.getWeeklyFieldTasks(formatDate(this.dateService.dates[0],'yyyy-MM-dd','de-CH'), formatDate(this.dateService.dates[this.dateService.dates.length-1],'yyyy-MM-dd','de-CH')))
		console.log("got these FieldTasks:", this.weeklyFieldTasks)
		this.getDeviceForTask()
	}
	async getDeviceForTask() {
		for (let task of this.weeklyFieldTasks) {
			let deviceForTaskEntries = await lastValueFrom(this.fieldTaskService.getDeviceForTaskEntryByTaskID(task.id))
			this.deviceForTasks.set(task, deviceForTaskEntries)
		}
	}
	async getSelectedDevices(id:number) {
		this.fieldTaskService.getSelectedTachys(id).subscribe({
			next: (response) => {this.selectedTachys = response}
		})
		this.fieldTaskService.getSelectedCars(id).subscribe({
			next: (response) => {this.selectedCars= response}
		})
		this.fieldTaskService.getSelectedGnss(id).subscribe({
			next: (response) => {
				this.selectedGnss= response}
		})
	}
	async getDeviceMap() {
		let tachys: Tachy[]
		let cars: Car[]
		let gnss: Gnss[]
		let arr= []
		for (let date of this.dateService.dates) {
			let fieldTaskByTachy: Map<number, FieldTask[]> = new Map
			let fieldTaskByCar: Map<number, FieldTask[]> = new Map
			let fieldTaskByGnss: Map<number, FieldTask[]> = new Map
			let dateString = formatDate(date,'yyyy-MM-dd','de-CH')
			for (let task of this.weeklyFieldTasks) {
				// console.log("checking Task", task.orderDescription)
				this.selectedTachys=[]
				this.selectedCars=[]
				this.selectedGnss=[]
				if(this.dateService.compareDates(date, new Date(task.date))){
					// await this.getSelectedDevices(task.id)
					let responseList = await lastValueFrom(this.fieldTaskService.getAllSelected(task.id))
					tachys = responseList[0]
					cars= responseList[1]
					gnss = responseList[2]
					for (let t of tachys){
						arr = fieldTaskByTachy.get(t.id) ? fieldTaskByTachy.get(t.id) : []
						arr.push(task)
						fieldTaskByTachy.set(t.id,arr)
					}
					for(let c of cars){
						arr= fieldTaskByCar.get(c.id) ? fieldTaskByCar.get(c.id): []
						arr.push(task)
						fieldTaskByCar.set(c.id,arr)
					}
					for (let g of gnss) {
						arr = fieldTaskByGnss.get(g.id) ? fieldTaskByGnss.get(g.id) : []
						arr.push(task)
						fieldTaskByGnss.set(g.id,arr)
					}
				}
			}
			this.tachyByDay.set(dateString,fieldTaskByTachy)
			this.carByDay.set(dateString,fieldTaskByCar)
			this.gnssByDay.set(dateString,fieldTaskByGnss)
		}
		this.loading=false
	}
	async changeWeek(direction:string){
		this.loading=true
		this.dateService.changeWeek(direction)
		await this.loadData()
		this.getDeviceMap()
	}
	async dateChange(e: any) {
		this.loading=true
		this.dateService.getDates(e)
		await this.loadData()
		this.getDeviceMap()
	}
	async openShowModal(taskID:number,event?:Event) {
		event?.stopPropagation()
		return this.showModalComponent.open(taskID)
	}
	createCopiedFieldTask() {
		this.editModalComponent.openCopied()
	}
	async openCreateModal() {
		return await this.editModalComponent.open()
	 }
}
	