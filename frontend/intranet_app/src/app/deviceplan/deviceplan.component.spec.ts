import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceplanComponent } from './deviceplan.component';

describe('DeviceplanComponent', () => {
  let component: DeviceplanComponent;
  let fixture: ComponentFixture<DeviceplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeviceplanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
