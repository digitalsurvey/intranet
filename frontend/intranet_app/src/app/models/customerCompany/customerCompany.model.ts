export class CustomerCompany {
    id: number
    name: string
    shortname?: string
    address?: number
}