import { CustomerCategory } from "../customerCategory/customer-category.model";

export class Customer {
    id: number
	 title: string
	 category: CustomerCategory
	 firstname: string
	 lastname: string
	 email:string
	 phone: string
	 companyName: string
	 stree: string
	 zip: number
	 city: string
}

export enum CustomerType {
    person = 'Person',
    company = 'Firma / Werk'
}