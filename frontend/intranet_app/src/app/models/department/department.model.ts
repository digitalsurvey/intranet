export class Department {
   id: number;
   name: string;
   company: number;
   shortname: string;
   description: string;
   active: boolean;
   color: string;

}
