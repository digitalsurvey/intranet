export class Company {
    id: number;
    name: string;
    shortname: string;
    active: boolean
}
