import { CostCenter } from "../costCenter/cost-center.model"
import { Department } from "../department/department.model"
import { Employee } from "../employee/employee.model"
import { Gemeinde } from "../gemeinde/gemeinde.model"
import { Invoice } from "../invoice/invoice.model"
import { ProjectCustomer } from "../projectCustomer/project-customer.model"
import { ProjectTask } from "../projectTask/projectTask.model"
import { Tag } from "../tag/tag.model"
export class Project {
    id: number
    currState: string /* enum ProjectState */
    acquisitionId: string
    projectId?: string
    title: string
    department: Department /* department */
    township: Gemeinde
   //  client: number[] /* customer */
   //  company?: number[] /* customer */
	client: ProjectCustomer[]
	company: ProjectCustomer[]
    offerCreator: Employee
    offerState: string /* enum OfferState */
    planningGroup?: number
    SIAphase: string
    estimatedCosts: number /* float */
    acceptedCosts: number /* float */
    effectiveCosts?: number /* float */
    plannedManager: Employee /* employee */
    plannedManagerDep: Employee /* employee */
    plannedForeman: Employee /* employee */
    plannedForemanDep: Employee /* employee */
    isReferenceWeb: boolean
    isReferenceTrainee: boolean
    manager?: Employee /* employee */
    managerDep?: Employee /* employee */
    foreman?: Employee /* employee */
    foremanDep?: Employee /* employee */
    coReferent?: Employee /* employee */
	 projectTasks?: ProjectTask[]
	 comment?: string
    creationDate: Date
    planningStartDate?: Date
    realisationStartDate?: Date
    doneDate?: Date
    estimatedDoneDate?: Date
    invoices: Invoice[]
    geolocation?: string /* geolocation */
	 referenceProject?: number[]
	 tags: Tag[]
	 costCenter: CostCenter
	
}


export enum ProjectState {
    acquisition = 'Akquisition',
    planning = 'Projektierung',
    realisation = 'Realisierung',
    done = 'Abschluss'
}

export enum OfferState {
    open = 'in Bearbeitung',
    accepted = 'erhalten',
    denied = 'Nicht erhalten'
}
