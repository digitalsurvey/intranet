import { Employee } from "../employee/employee.model"

export class Invoice {
    id:number
    number?: string
    amount?:number
    date?: Date
    employee: Employee
}
