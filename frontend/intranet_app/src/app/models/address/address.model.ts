export class Address {
    id: number
    street: string
    zip: number
    city: string
}