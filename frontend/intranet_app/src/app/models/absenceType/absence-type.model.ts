export class AbsenceType {
	id:number
	type:string
	confirm: boolean
	active:boolean
	color:string
}
