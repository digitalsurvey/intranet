export class Zuständigkeit {
	gemeinde: string;
	thema: string;
	person: string;
	stellvertretung?: string;
}
