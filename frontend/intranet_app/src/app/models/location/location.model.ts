export class Location {
    id: number;
    name: string;
    street: string;
    zip: string;
    city: string
    canton: number;
    active: boolean
}
