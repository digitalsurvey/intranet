export class ProjectTask {
    id: number
    title: string
    description: string
}