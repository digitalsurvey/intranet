export class Tachy {
    id: number
    abbreviation: string
    name: string
    serial: string
    function: string
    location: number
    active:boolean
    color:string

}
