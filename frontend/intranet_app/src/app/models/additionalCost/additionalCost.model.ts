export class AdditionalCost {
    id: number
    project: number
    description?: string
    costs: number
    creationDate: string
}