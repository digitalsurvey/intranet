import { Customer } from "../customer/customer.model"

export class ProjectCustomer {
	id: number
	customer: Customer
	contact: string
}

