export class Gemeinde {

	objectid:number
	wkb_geometry:string
	uuid:string
	datum_aenderung :Date
	datum_erstellung:Date 
	erstellung_jahr:number 
	erstellung_monat:number 
	revision_jahr:number
	revision_monat:number
	grund_aenderung:number
	herkunft:number 
	herkunft_jahr:number
	herkunft_monat:number 
	objektart:number 
	bfs_nummer:number 
	bezirksnummer:number
	kantonsnummer:number
	name:string 
	gem_teil:number 
	gem_flaeche:number 
	see_flaeche:number 
	icc:string 
	shn:string
	revision_qualitaet:string 
	einwohnerzahl:number 
	shape_length:number
	shape_area:number
}
