export class Person {
    id: number
    firstname: string
    lastname: string
    address?: number
    email: string
    phone: string
    title: string
}