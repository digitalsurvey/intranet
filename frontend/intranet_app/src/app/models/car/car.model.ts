export class Car {
    id: number;
    abbreviation: string;
    name: string;
    company: number;
    location: number;
    licensePlate: string;
    active: boolean;
    color:string;
}
