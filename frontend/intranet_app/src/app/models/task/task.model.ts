export class Task {
    id?: number;
    employee?: number;
    name?: string;
    description?: string;
    date: Date;
    homeOffice: boolean;
}
