export class PlanningGroup {
    id: number
    name: string
    description: string
}