import { Employee } from "../employee/employee.model";

export class User extends Employee {
    password: string;
    token: string;
}
