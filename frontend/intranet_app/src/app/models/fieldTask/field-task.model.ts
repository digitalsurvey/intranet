export class FieldTask {
    id: number
    orderNO: string
    orderDescription: string
    date: Date
    time: string
    location: number
    operator: number
    carAmount: number
    tachyAmount: number
    gnssAmount: number
    employeeAmount: number
    comment: string
    released: boolean
}
