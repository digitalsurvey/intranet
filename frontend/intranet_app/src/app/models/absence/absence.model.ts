import { AbsenceType } from "../absenceType/absence-type.model"
import { Employee } from "../employee/employee.model"

export class Absence {
	id:number
	start:string
	end:string
	type: AbsenceType
	employee:Employee
	recursiveID:number
	status:string
	recursive:boolean
}
