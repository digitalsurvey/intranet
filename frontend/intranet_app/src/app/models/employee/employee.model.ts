import { Department } from "../department/department.model";
import { Role } from "../role/role.model";

export class Employee {
    id: number;
    firstname?: string;
    lastname?: string;
    username: string;
    department?: Department;
    workplace?: number;
    role?: Role;
    birthday?: string;
    phoneInt?: string;
    phoneMob?: string;
    email?: string;
}


