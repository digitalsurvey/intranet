import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { News } from '../models/news/news.model';

import { NEWS_URL } from './server.constants';
@Injectable({
	providedIn: 'root'
})
export class NewsService {
	
	constructor(private http: HttpClient) { }
	
	getNews(start: string, end: string): Observable<News[]> {
		return this.http.get<News[]>(`${NEWS_URL}/${start}&${end}`)
	}
	createNews(data: any): Observable<any>{
		return this.http.post(NEWS_URL,data)
	}
	deleteNews(id:number){
		return this.http.delete(`${NEWS_URL}/${id}`)
	}
}
