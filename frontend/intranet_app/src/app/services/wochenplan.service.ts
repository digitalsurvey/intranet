import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { Employee } from '../models/employee/employee.model';
import { Task } from '../models/task/task.model';
import { Department } from '../models/department/department.model';
import { Location } from '../models/location/location.model';
import { Company } from '../models/company/company.model';
import { EMPLOYEE_URL, TASK_URL, DEPARTMENT_URL, LOCATION_URL, COMPANY_URL, ROLE_URL, ABSENCE_URL} from  './server.constants';
import { Role } from '../models/role/role.model';
import { AbsenceType } from '../models/absenceType/absence-type.model';

@Injectable({
  providedIn: 'root'
})

export class WochenplanService {
  copiedTask: Task

  constructor(private http: HttpClient) { }


  getAllData(): Observable<any> {
    let employees = this.getAllEmployees()
    let tasks = this.getAllTasks()
    let departments = this.getAllDepartments()
    let companies = this.getAllCompanies()
    let locations = this.getAllLocations()
    return forkJoin([employees, tasks, departments, companies, locations])
  }
  getAllEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(EMPLOYEE_URL)
  }
  getAllDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(DEPARTMENT_URL)
  }  
  getAllTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(TASK_URL)
  }
  getAllCompanies(): Observable<Company[]>{
    return this.http.get<Company[]>(COMPANY_URL)
  }
  getAllLocations(): Observable<Location[]>{
    return this.http.get<Location[]>(LOCATION_URL)
  }
  getEmployeeByID(id: number): Observable<Employee> {
    return this.http.get<Employee>(`${EMPLOYEE_URL}/${id}`)
  }
  getTaksbyEmployee(id: any): Observable<Task[]> {
    return this.http.get<Task[]>(`${TASK_URL}/employee/${id}`)
  }
  createTask(data: any): Observable<any> {
    return this.http.post(TASK_URL, data)
  }
  deleteTask(id: number): Observable<any> {
    return this.http.delete(`${TASK_URL}/${id}`)
  }
  updateTask(id:number, data:any): Observable<any> {
    return this.http.patch(`${TASK_URL}/${id}`,data)
  }
  setCopiedTask(task:Task){
    this.copiedTask = task
  }
  getCopiedTask(): Task{
    return this.copiedTask
  }
  getAllRoles(): Observable<Role[]> {
	return this.http.get<Role[]>(ROLE_URL)
 }
 getAllAbsenceTypes(): Observable<AbsenceType[]> {
  return this.http.get<AbsenceType[]>(`${ABSENCE_URL}/type`)
 }
  

}
