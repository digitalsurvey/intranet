import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Feature } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import { Geometry } from 'ol/geom';
import VectorSource from 'ol/source/Vector';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }


  async searchAdress(searchText: String): Promise<Feature<Geometry>[]>{
    let text = searchText
    let url = `https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=${text}&type=locations&sr=2056&geometryFormat=geojson`
    let result =  await lastValueFrom(this.http.get(url))
    let features = new GeoJSON().readFeatures(JSON.stringify(result))
    return features
  }
}
