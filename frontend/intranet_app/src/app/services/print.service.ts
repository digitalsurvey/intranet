import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PrintService {


  constructor(private router: Router) { }

  isPrinting = false
  printDocument(depFilter: number, locFilter: number, date: string) {
	this.isPrinting = true;
	this.router.navigate(['/',
	  { outlets: {
		 'print': ['print', 'wochenplan', depFilter, locFilter, date]
	  }}]);
 }

 onDataReady() {
	setTimeout(() => {
	  window.print();
	  this.isPrinting = false;
	  this.router.navigate([{ outlets: { print: null }}]);
	});
 }
}
