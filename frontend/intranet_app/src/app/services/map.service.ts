import { Injectable, OnInit } from '@angular/core';
import { last, lastValueFrom } from 'rxjs';
import { Project } from '../models/project/project.model';
import { ProjectService } from './project.service';
import {AccountService} from "./account.service";
import {User} from "../models/user/user.model";

@Injectable({
	providedIn: 'root'
})
export class MapService implements OnInit {

  loggedInUser: User
  allProjects: Project[]
  visibleProjects: Map<number,Project> = new Map
  constructor(
    private projectService: ProjectService,
    private accountService: AccountService
  ) {  }

  async ngOnInit(userSpecific?: boolean) {
    if(userSpecific) {
      if(!this.loggedInUser){
        this.accountService.user.subscribe({
          next: (res) => this.loggedInUser=res
        })    
      }
      this.allProjects = await lastValueFrom(this.projectService.getAllProjectByUser(this.loggedInUser.id))
    }else {
      this.allProjects = await lastValueFrom(this.projectService.getAllProjects());
    }
    this.visibleProjects.clear()
    for(let project of this.allProjects) {
        this.visibleProjects.set(project.id, project);
    }
  }

  removeProject(projectID:number){
    this.visibleProjects.delete(projectID);
  }

  addProject(projectID:number){
    this.visibleProjects.set(projectID,this.allProjects.find(e=>e.id==projectID))
  }

  async getVisibleProjects(): Promise<Project[]> {
    return Array.from( this.visibleProjects.values() );
  }

  async getVisibleProjectsMap(): Promise<Map<number, Project>> {
    return this.visibleProjects;
  }

  getVisibleProject(projectId: number): Project {
    return this.visibleProjects.get(projectId);
  }
  reset(){
    this.visibleProjects.clear()
    for(let project of this.allProjects) {
        this.visibleProjects.set(project.id, project);
    }  }

}
