import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, lastValueFrom, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { USERS_AUTH_URL, 
	USERS_PASSWORD_URL, 
	USERS_REGISTER_URL, 
	USERS_URL,
	MANAGER_URL } from './bluMAP API';
	import { User } from '../models/user/user.model';
	import { Manager } from '../models/manager/manager.model';
import { WochenplanService } from './wochenplan.service';
import { Role } from '../models/role/role.model';
	
	@Injectable({ providedIn: 'root' })
	export class  AccountService{
		private userSubject: BehaviorSubject<User>;
		public user: Observable<User>;
		private roles: Role[];
		
		constructor(private router: Router,private http: HttpClient, private wochenplanService: WochenplanService) {
				this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
				this.user = this.userSubject.asObservable();
			}

			public get userValue(): User {
				let user = new User
				let role = new Role()
				role.id = 2
				user.role = role;
				// return this.userSubject.value;
				return user
			}
			
			login(username: String, password: String) {
				return this.http.post<User>(USERS_AUTH_URL, { username, password })
				.pipe(map(user => {
					// store user details and jwt token in local storage to keep user logged in between page refreshes
					localStorage.setItem('user', JSON.stringify(user));
					this.userSubject.next(user);
					return user;
				}));
			}
			
			logout() {
				// remove user from local storage and set current user to null
				localStorage.removeItem('user');
				this.userSubject.next(null);
				this.router.navigate(['/account/login']);
			}
			
			forgot(username: string, newPassword: string) {
				let body = {username: username, password: newPassword}
				return this.http.post(USERS_PASSWORD_URL, body);
			}
			
			register(user: User) {
				return this.http.post(USERS_REGISTER_URL, user);
			}
			
			getAll() {
				return this.http.get<User[]>(USERS_URL);
			}
			
			getById(id: number) {
				return this.http.get<User>(`${USERS_URL}/${id}`);
			}
			
			// TODO: Luca Archidiacono - params needs to be updated
			update(id: number, params: any) {
				return this.http.put(`${USERS_URL}/${id}`, params)
				.pipe(map(x => {
					// update stored user if the logged in user updated their own record
					if (id == this.userValue.id) {
						// update local storage
						const user = { ...this.userValue, ...params };
						localStorage.setItem('user', JSON.stringify(user));
						
						// publish updated user to subscribers
						this.userSubject.next(user);
					}
					return x;
				}));
			}
			
			delete(id: number) {
				return this.http.delete(`${USERS_URL}/${id}`)
				.pipe(map(x => {
					// auto logout if the logged in user deleted their own record
					if (id == this.userValue.id) {
						this.logout();
					}
					return x;
				}));
			}
			
			async isManager(): Promise<boolean> {
				let allManagers = await lastValueFrom(this.http.get<Manager[]>(MANAGER_URL))
				if(allManagers.find(manager => manager.employee == this.userValue.id)){
					return true
				} else {
					return false
				}
			}
		}