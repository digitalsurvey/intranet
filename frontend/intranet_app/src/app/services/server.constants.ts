import { environment } from "src/environments/environment";

export let BASE_URL: string
if(environment.SSL) {
	BASE_URL = `https://${environment.server}:${environment.port}/api`
} else {
	BASE_URL = `http://${environment.server}:${environment.port}/api`
}
// export const BASE_URL = `http://${server}:${port}/api`
export const EMPLOYEE_URL = `${BASE_URL}/employee`;
export const ROLE_URL = `${BASE_URL}/roles`;
export const TASK_URL = `${BASE_URL}/task`;
export const DEPARTMENT_URL = `${BASE_URL}/department`;
export const LOCATION_URL = `${BASE_URL}/location`;
export const COMPANY_URL = `${BASE_URL}/company`;
export const TACHY_URL = `${BASE_URL}/tachy`;
export const CAR_URL = `${BASE_URL}/car`;
export const GNSS_URL = `${BASE_URL}/gnss`;
export const FIELD_TASK_URL = `${BASE_URL}/fieldTask`;
export const DEVICE_FOR_TASK_URL = `${BASE_URL}/deviceForTask`
export const AVAILABLE_URL = `${BASE_URL}/available`
export const NEWS_URL = `${BASE_URL}/news`
export const ABSENCE_URL = `${BASE_URL}/absence`
export const MAIL_URL = `${BASE_URL}/mail`
export const RESPONSIBILITY_URL = `${BASE_URL}/responsibilities`



// export enum ABSENCE_TYPES {
// 	'school' = "Schule",
// 	'army' = "Militär",
// 	'sick' = "Krankheit",
// 	'vacation' = "Ferien",
// }
export enum ABSENCE_SATUS{
	'open' = 'offen',
	'accepted' = 'angenommen',
	'denied' = 'abgelehnt'
}
export enum ABSENCE_TYPE_NUMBER{
	'N' = 0,
	'F' = 1,
	'M' = 2,
	'S' = 3,
	'K' = 4,

}