import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Zuständigkeit } from '../models/zuständigkeit/zuständigkeit.model';
import { RESPONSIBILITY_URL } from './server.constants';

@Injectable({
  providedIn: 'root'
})
export class ResponsibilityService {

  constructor(private http: HttpClient) { }

  getAllResponsibilities(): Observable<Zuständigkeit[]> {
	return this.http.get<Zuständigkeit[]>(RESPONSIBILITY_URL)
  }

}
