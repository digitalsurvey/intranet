import { Injectable, OnInit } from '@angular/core';
import { DateTime } from 'luxon';
import Holidays from 'date-holidays';

@Injectable({
	providedIn: 'root'
})
export class DateService implements OnInit {
	holidays=new Holidays('CH',{types:["public"]}) //get all public holidays for Bern, Switzerland
	dates: Date[] = []
	today: Date = new Date()
	thisWeek: number = DateTime.fromJSDate(this.today).weekNumber
	selectedDate: Date
	weekNumber: number
	
	constructor() { }
	ngOnInit(): void {
	}
	
	changeWeek(direction:string) {
		let newDate = this.selectedDate
		switch(direction) {
			case 'forward':
			newDate = new Date(newDate.setDate(newDate.getDate()+7))
			break;
			case 'backward':
			newDate = new Date(newDate.setDate(newDate.getDate()-7))
			break;
		}
		this.getDates(newDate)
	} 
	changeMonth(direction:string) {
		let newDate = this.selectedDate
		switch(direction) {
			case 'forward':
			newDate = new Date(newDate.setMonth(newDate.getMonth()+1))
			break;
			case 'backward':
			newDate = new Date(newDate.setMonth(newDate.getMonth()-1))
			break;
		}
		this.getDates(newDate)
	} 
	getDates(newDate?: Date): void {
		if (newDate === undefined) {newDate = new Date()}
		this.selectedDate = new Date(newDate)
		let selDate = newDate
		if(selDate.getDay() == 0) { //when a sunday is chosen get the monday 6 days before
			selDate.setDate(selDate.getDate()-6)
		}
		for (let i=0; i<=6;i++){
			this.dates[i] = new Date(selDate.setDate(selDate.getDate() - selDate.getDay()+1+i));
			this.dates[i].setHours(0,0,0,0)
		}
		this.weekNumber = DateTime.fromJSDate(this.dates[0]).weekNumber
	}
	
	compareDates(d1: Date, d2: Date): boolean {
		if(d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear() ){
			return true
		}
		return false
	}
	getLastDay(date: Date): Date {
		let month = date.getMonth()
		let year = date.getFullYear()
		let end = DateTime.local(year,month).daysInMonth
		return new Date(year,month,end)
	}
	getDaysPerMonth(date: Date): number {
		let month = date.getMonth()+1 //since TypeScript start with January = 0, a +1 has to be added
		let year = date.getFullYear()
		return DateTime.local(year,month).daysInMonth
	}
	checkHolidays(date: Date): any {
		let hol = this.holidays.isHoliday(date)
		return hol!=false?hol[0]:false
	}
	
	

}
