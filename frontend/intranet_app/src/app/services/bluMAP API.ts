//Ip HomeOffice
//  const server = '192.168.1.102'

// Hotspot Config
// const server = "192.168.190.55" 


// IP offline
// const server = 'localhost'

//IP Office
// const server = '192.168.32.105'
const server ='intranet.intra.bsb-partner.ch'

// const SSL = true
const SSL = false
const port = '9998'
export let BASE_URL: string
if(SSL) {
	BASE_URL = `https://${server}:${port}/api`
} else {
	BASE_URL = `http://${server}:${port}/api`
}
export const EMPLOYEE_URL = `${BASE_URL}/employee`;
export const PROJECT_URL = `${BASE_URL}/project`;
export const PROJECT_ADDITIONAL_COSTS_URL = `${BASE_URL}/project/additionalCosts`;
export const PROJECT_REFERRED_URL = `${BASE_URL}/project/referred`
export const PROJECT_TAG_URL = `${BASE_URL}/projectTags`
export const TAGS_URL = `${BASE_URL}/tag`;
export const CUSTOMER_URL = `${BASE_URL}/customer`;
export const USERS_AUTH_URL = `${BASE_URL}/users/authenticate`
export const USERS_REGISTER_URL = `${BASE_URL}/users/register`
export const USERS_PASSWORD_URL = `${BASE_URL}/users/forgotPassword`
export const USERS_URL = `${BASE_URL}/users`
export const ADDITIONAL_COSTS_URL = `${BASE_URL}/additionalCosts`;
export const PROJECTTASKS_URL = `${BASE_URL}/project/tasks`;
export const PROJECTTASK_URL = `${BASE_URL}/project/task`;
export const PROJECTUSER_URL = `${BASE_URL}/project/user`;
export const MANAGER_URL = `${BASE_URL}/manager`;
export const PROJECT_COMPANY_URL	= `${BASE_URL}/project/company`
export const PROJECT_INVOICE_URL = `${BASE_URL}/project/invoice`
export const PROJECT_REFERENCE_URL	= `${BASE_URL}/project/referenceProject`
export const TOP5_PROJECT_BY_DEPARTMENT_URL = `${BASE_URL}/project/department/top5`
export const TOP5_PROJECT_URL = `${BASE_URL}/project/top5`
export const PROJECT_BY_DEPARTMENT_URL = `${BASE_URL}/project/department`
export const OLDERST_PROJECT_IN_ACQUISITION = `${BASE_URL}/project/acquisition/oldest`
export const INVOICE_URL = `${BASE_URL}/invoice`
export const GEMEINDE_URL = `${BASE_URL}/gemeinde`
export const COSTCENTER_URL = `${BASE_URL}/costcenter`
export const CUSTOMERCATEGORY_URL = `${BASE_URL}/customerCategory`
export const PROJECT_CUSTOMER_URL = `${BASE_URL}/projectCustomer`
export const CREATE_SHEET_URL = `${BASE_URL}/createSheet`