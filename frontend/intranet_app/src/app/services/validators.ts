import {AbstractControl, ValidatorFn} from '@angular/forms';

export function objectValidator(nullAllowed?:boolean): ValidatorFn {
	return (control: AbstractControl): {[key:string]:any} | null => {
		let valid
		if(nullAllowed){
			valid = typeof control.value == 'object'
		} else {
			valid = typeof control.value == 'object' && control.value != null
		}
		return valid?null:{wrongValue: 'please select Employee'}
	};
}