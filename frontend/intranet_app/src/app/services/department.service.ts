import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import {DEPARTMENT_URL} from './server.constants';
import {Department} from "../models/department/department.model";

@Injectable({
    providedIn: 'root'
})

export class DepartmentService {

    constructor(private http: HttpClient) { }

    getAllData(): Observable<any> {
        let departments = this.getAllDepartments()
        return forkJoin([departments])
    }
    getAllDepartments(): Observable<Department[]> {
        return this.http.get<Department[]>(DEPARTMENT_URL)
    }
    getDepartmentByID(id: number): Observable<Department> {
        return this.http.get<Department>(`${DEPARTMENT_URL}/${id}`)
    }
}