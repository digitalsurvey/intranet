import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { Car } from '../models/car/car.model'
import { DeviceForTask } from '../models/deviceForTask/device-for-task.model';
import { Employee } from '../models/employee/employee.model';
import { FieldTask } from '../models/fieldTask/field-task.model';
import { Gnss } from '../models/gnss/gnss.model';
import { Tachy } from '../models/tachy/tachy.model';

import { TACHY_URL, GNSS_URL, FIELD_TASK_URL, CAR_URL, DEVICE_FOR_TASK_URL, AVAILABLE_URL } from  './server.constants';

@Injectable({
	providedIn: 'root'
})
export class FieldTaskService {
	copiedFieldTask: FieldTask
	
	
	constructor( private http: HttpClient){ }
	
	getAllData(): Observable<any> {
		let cars = this.getAllCars()
		let tachys = this.getAllTachys()
		let gnss = this.getAllGNSS()
		let fieldTasks = this.getAllFieldTasks()
		return forkJoin([cars,tachys,gnss,fieldTasks])
	} 
	getAllCars(): Observable<Car[]> {
		return this.http.get<Car[]>(CAR_URL)
	}
	getAllTachys(): Observable<Tachy[]> {
		return this.http.get<Tachy[]>(TACHY_URL)
	}
	getAllGNSS(): Observable<Gnss[]>{
		return this.http.get<Gnss[]>(GNSS_URL)
	}
	getAllFieldTasks(): Observable<FieldTask[]> {
		return this.http.get<FieldTask[]>(FIELD_TASK_URL)
	}
	getReleasedWeeklyFieldTasks(start:string,end:string): Observable<FieldTask[]>{
		return this.http.get<FieldTask[]>(`${FIELD_TASK_URL}/${start}&${end}/released`)
	}
	getFieldTaskByID(id:number): Observable<FieldTask> {
		return this.http.get<FieldTask>(`${FIELD_TASK_URL}/${id}`)
	}
	deleteFieldTask(taskID:number): Observable<any> {
		return this.http.delete(`${FIELD_TASK_URL}/${taskID}`)
	}
	createFieldTask(data: any): Observable<any> {
		return this.http.post(FIELD_TASK_URL, data)
	}
	getWeeklyFieldTasks(start: string,end:string): Observable<FieldTask[]> {
		return this.http.get<FieldTask[]>(`${FIELD_TASK_URL}/${start}&${end}`)
	}
	createDeviceForTask(data: any): Observable<any>{
		return this.http.post(DEVICE_FOR_TASK_URL,data)
	}
	// patchFieldTaskReleased(taskID:number,released:boolean): Observable<any> {
	//   let data = {'released':released}
	//   return this.http.patch(`${FIELD_TASK_URL}/${taskID}/released`,data)
	// }
	patchFieldTask(taskID:number,data:any): Observable<any> {
		return this.http.patch(`${FIELD_TASK_URL}/${taskID}`,data)
	}
	
	//ignoreExisting:number
	// id of FieldTask with existing entries which sould be Ignored when checking for available Devices so that
	// devices already selected on the existing FieldTaks can be selected again. 
	getAllAvailable(date:string,time:string,locationID:number,ignoreTaskId:number) { 

		console.log("sending with ignore ID", ignoreTaskId)

		let tachys = this.getAvailableTachys(date,time,locationID,ignoreTaskId)
		let cars = this.getAvailableCars(date,time,locationID,ignoreTaskId)
		let gnss = this.getAvailableGnss(date,time,locationID,ignoreTaskId)
		let employees = this.getAvailableEmployees(date,time,locationID,ignoreTaskId)
		return forkJoin([tachys,cars,gnss,employees])
	}
	getAllSelected(taskID:number) {
		let tachys = this.getSelectedTachys(taskID)
		let cars = this.getSelectedCars(taskID)
		let gnss = this.getSelectedGnss(taskID)
		let employees = this.getSelectedEmployees(taskID)
		let deviceForTask = this.getDeviceForTaskEntryByTaskID(taskID)
		return forkJoin([tachys, cars, gnss, employees, deviceForTask])
	}
	getDeviceForTaskEntryByTaskID(taskID:number): Observable<DeviceForTask[]> {
		return this.http.get<DeviceForTask[]>(`${DEVICE_FOR_TASK_URL}/task/${taskID}`)
	}
	patchDeviceForTask(entryID:number, data:any): Observable<any> {
		return this.http.patch(`${DEVICE_FOR_TASK_URL}/${entryID}`,data)
	}
	deleteDeviceForTaskEntry(entryID:number): Observable<any> {
		return this.http.delete(`${DEVICE_FOR_TASK_URL}/${entryID}`)
	}
	getSelectedTachys(taskID:number): Observable<Tachy[]> {
		return this.http.get<Tachy[]>(`${FIELD_TASK_URL}/${taskID}/selected/tachy`)
	}
	getSelectedCars(taskID:number): Observable<Car[]> {
		return this.http.get<Car[]>(`${FIELD_TASK_URL}/${taskID}/selected/car`)
	}
	getSelectedGnss(taskID:number): Observable<Gnss[]> {
		return this.http.get<Gnss[]>(`${FIELD_TASK_URL}/${taskID}/selected/gnss`)
	}
	getSelectedEmployees(taskID:number): Observable<Employee[]> {
		return this.http.get<Employee[]>(`${FIELD_TASK_URL}/${taskID}/selected/employee`)
	}
	
	
	//ignoreNumber:number
	// id of FieldTask whose selection shall be ignored, set to 0 to disable  
	getAvailableTachys(date:string,time: string,locationID:number,ignoreNumber:number): Observable<Tachy[]> {
		return this.http.get<Tachy[]>(`${AVAILABLE_URL}/tachy/${date}&${time}&${locationID}&${ignoreNumber}`)
	}
	getAvailableCars(date:string,time: string,locationID:number,ignoreNumber:number): Observable<Car[]> {
		return this.http.get<Car[]>(`${AVAILABLE_URL}/car/${date}&${time}&${locationID}&${ignoreNumber}`)
	}
	getAvailableGnss(date:string,time: string,locationID:number,ignoreNumber:number): Observable<Gnss[]> {
		return this.http.get<Gnss[]>(`${AVAILABLE_URL}/gnss/${date}&${time}&${locationID}&${ignoreNumber}`)
	}
	getAvailableEmployees(date:string,time: string,locationID:number,ignoreNumber:number): Observable<Employee[]> {
		return this.http.get<Employee[]>(`${AVAILABLE_URL}/employee/${date}&${time}&${locationID}&${ignoreNumber}`)
	}
	
	setCopiedFieldTask(task:FieldTask) { 
		this.copiedFieldTask = task
	}
	getCopiedFieldTask() {
		return this.copiedFieldTask
	}
}
