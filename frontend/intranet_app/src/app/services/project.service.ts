import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';

import {
	PROJECT_ADDITIONAL_COSTS_URL,
	ADDITIONAL_COSTS_URL,
	PROJECT_TAG_URL,
	PROJECT_URL, TAGS_URL,
	PROJECTTASKS_URL, PROJECTTASK_URL, PROJECT_REFERRED_URL, PROJECTUSER_URL, PROJECT_CUSTOMER_URL, PROJECT_COMPANY_URL, PROJECT_REFERENCE_URL, PROJECT_BY_DEPARTMENT_URL, TOP5_PROJECT_BY_DEPARTMENT_URL, OLDERST_PROJECT_IN_ACQUISITION, TOP5_PROJECT_URL, PROJECT_INVOICE_URL, INVOICE_URL, CREATE_SHEET_URL, EMPLOYEE_URL
} from './bluMAP API';
import {Project} from "../models/project/project.model";
import { Tag } from '../models/tag/tag.model';
import { AdditionalCost } from '../models/additionalCost/additionalCost.model';
import { ProjectTask } from '../models/projectTask/projectTask.model';
import { ProjectTag } from '../models/projectTag/project-tag.model';
import { Invoice } from '../models/invoice/invoice.model';
import { ProjectCustomer } from '../models/projectCustomer/project-customer.model';
import { Employee } from '../models/employee/employee.model';

@Injectable({
	providedIn: 'root'
})
export class ProjectService {
	
	constructor( private http: HttpClient ) { }
	
	getAllEmployees(): Observable<any> {
		return this.http.get<Employee[]>(EMPLOYEE_URL)
	}
	getAllData(): Observable<any> {
		let projects = this.getAllProjects()
		return forkJoin([projects])
	}
	getAllProjects(): Observable<Project[]> {
		return this.http.get<Project[]>(PROJECT_URL)
	}
	getAllProjectByUser(userId: number): Observable<Project[]> {
		return this.http.get<Project[]>(`${PROJECTUSER_URL}/${userId}`)
	}
	getProjectByID(id:number): Observable<Project> {
		return this.http.get<Project>(`${PROJECT_URL}/${id}`)
	}
	deleteProject(projectId:number): Observable<any> {
		return this.http.delete(`${PROJECT_URL}/${projectId}`)
	}
	createProject(data: any): Observable<any> {
		return this.http.post(PROJECT_URL, data)
	}
	patchProject(projectId:number,data:any): Observable<any> {
		return this.http.patch(`${PROJECT_URL}/${projectId}`,data)
	}
	getAllSelected(projectId:number) {
		return forkJoin([])
	}
	
	getAllTags(): Observable<Tag[]> {
		return this.http.get<Tag[]>(TAGS_URL);
	}
	getAllAdditionalCosts(projectId:number): Observable<AdditionalCost[]> {
		return this.http.get<AdditionalCost[]>(`${PROJECT_ADDITIONAL_COSTS_URL}/${projectId}`)
	}
	createAdditionalCosts(data:any): Observable<any> {
		return this.http.post(ADDITIONAL_COSTS_URL, data)
	}
	deleteAdditionalCosts(additionalCostId:number): Observable<any> {
		return this.http.delete(`${ADDITIONAL_COSTS_URL}/${additionalCostId}`)
	}
	getAllProjectTasks(): Observable<ProjectTask[]> {
		return this.http.get<ProjectTask[]>(PROJECTTASK_URL);
	}
	getProjectTask(projectTaskId:number): Observable<ProjectTask[]> {
		return this.http.get<ProjectTask[]>(`${PROJECTTASK_URL}/${projectTaskId}`);
	}
	
	getAllProjectTasksById(projectId:number): Observable<ProjectTask[]> {
		return this.http.get<ProjectTask[]>(`${PROJECT_URL}/${projectId}/tasks`);
	}
	
	createProjectTask(data:any): Observable<any> {
		return this.http.post(PROJECTTASK_URL, data)
	}
	
	deleteProjectTasks(projectId:number, projectTaskId:number): Observable<any> {
		return this.http.delete(`${PROJECT_URL}/${projectId}/${projectTaskId}`)
	}
	deleteProjectTask(taskId: number): Observable<any> {
		return this.http.delete(`${PROJECTTASK_URL}/${taskId}`)
	}
	
	addProjectTaskToProject(data:any): Observable<any> {
		return this.http.post(PROJECTTASKS_URL, data)
	}
	getReferenceProjects(projectId:number): Observable<Project[]> {
		return this.http.get<Project[]>(`${PROJECT_REFERRED_URL}/${projectId}`);
	}
	
	removeReferenceProject(projectId:number, rProjectId:number): Observable<any> {
		return this.http.delete(`${PROJECT_REFERRED_URL}/${projectId}/${rProjectId}`)
	}
	
	addReferenceProjects(data:any): Observable<any> {
		return this.http.post(PROJECT_REFERRED_URL, data)
	}
	addProjectCustomer(data:any):Observable<ProjectCustomer> {
		return this.http.post<ProjectCustomer>(PROJECT_CUSTOMER_URL,data)
	}
	addProjectCustomerToProject(projectId:number, projectCustomerId:number): Observable<any> {
		return this.http.post(`${PROJECT_CUSTOMER_URL}/${projectId}/${projectCustomerId}`,null)
	}
	deleteProjectCustomer(projectCustomerId:number){
		return this.http.delete(`${PROJECT_CUSTOMER_URL}/${projectCustomerId}`)
	}
	updateProjectCustomer(projectCustomerId:number, data:any): Observable<ProjectCustomer>{
		return this.http.patch<ProjectCustomer>(`${PROJECT_CUSTOMER_URL}/${projectCustomerId}`,data)
	}
	addProjectReferenceToProject(projectId:number, referenceId:number): Observable<any> {
		let data=""//data is not needed since it only adds a reference but POST requires data to be sent
		return this.http.post(`${PROJECT_REFERENCE_URL}/${projectId}/${referenceId}`, data)
	}
	removeProjectReferenceFromProject(projectId:number, referenceId:number): Observable<any> {
		return this.http.delete(`${PROJECT_REFERENCE_URL}/${projectId}/${referenceId}`)
	}
	getTop5ProjectsByDepartment(departmentId: number): Observable<Project[]> {
		return this.http.get<Project[]>(`${TOP5_PROJECT_BY_DEPARTMENT_URL}/${departmentId}`)
	}
	getTop5Projects(): Observable<Project[]> {
		return this.http.get<Project[]>(TOP5_PROJECT_URL)
	}
	getProjectsByDepartment(departmentId:number): Observable<Project[]> {
		return this.http.get<Project[]>(`${PROJECT_BY_DEPARTMENT_URL}/${departmentId}`)
	}
	getOldest5ProjectsInAcquisition(): Observable<Project[]>{
		return this.http.get<Project[]>(`${OLDERST_PROJECT_IN_ACQUISITION}`)
	}
	addInvoice(data:any): Observable<Invoice>{
		return this.http.post<Invoice>(`${INVOICE_URL}`,data)
	}
	addInvoiceToProject(projectId:number,invoiceId:number): Observable<any> {
		let data=""//data is not needed since it only adds a reference but POST requires data to be sent
		return this.http.post(`${PROJECT_INVOICE_URL}/${projectId}/${invoiceId}`,data)
	}
	deleteInvoice(projectId:number, invoiceId:number): Observable<any> {
		return this.http.delete(`${PROJECT_INVOICE_URL}/${projectId}/${invoiceId}`)
	}
	addProjectTag(projectId:number, tagId:number): Observable<any> {
		let data=""//data is not needed since it only adds a reference but POST requires data to be sent
		return this.http.post(`${PROJECT_TAG_URL}/${projectId}/${tagId}`,data)
	}
	deleteProjectTag(projectId:number, tagId:number): Observable<any> {
		return this.http.delete(`${PROJECT_TAG_URL}/${projectId}/${tagId}`)
	}
	createProjectSheet(projectId:number):Observable<any> {
		return this.http.get(`${CREATE_SHEET_URL}/${projectId}`)
	}
	
}
