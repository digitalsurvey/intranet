import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CostCenter } from '../models/costCenter/cost-center.model';
import { COSTCENTER_URL} from './bluMAP API'
@Injectable({
	providedIn: 'root'
})
export class CostCenterService {
	
	constructor(private http:HttpClient) { }
	
	getAllCostCenters(): Observable<CostCenter[]> {
		return this.http.get<CostCenter[]>(COSTCENTER_URL)
	}
}
