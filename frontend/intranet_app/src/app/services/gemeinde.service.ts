import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, firstValueFrom, lastValueFrom, Observable, Subject } from 'rxjs';
import { Gemeinde } from '../models/gemeinde/gemeinde.model';
import { GEMEINDE_URL } from './bluMAP API';

@Injectable({
	providedIn: 'root'
})
export class GemeindeService {
	public townshipSubject: BehaviorSubject<Gemeinde[]> = new BehaviorSubject([])
	public townships: Gemeinde[]
	finished: boolean = false
	constructor(private http: HttpClient) { 
		this.callApi()
	}
	getTownships(): Observable<Gemeinde[]> {
		return this.townshipSubject.asObservable()
	}
	callApi(){
		console.log("getting townships")

		this.http.get<Gemeinde[]>(GEMEINDE_URL).subscribe({
			next: (response) => {
				this.townships=response
				this.townshipSubject.next(response)
				this.finished=true
				console.log("finished getting TS"	)
			}
		})
		// this.townships = await firstValueFrom(this.http.get<Gemeinde[]>(GEMEINDE_URL))
		// console.log("finished gettings ts")

	}
	
}
