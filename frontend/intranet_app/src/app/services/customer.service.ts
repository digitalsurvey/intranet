import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';

import {CUSTOMERCATEGORY_URL, CUSTOMER_URL} from './bluMAP API';
import {Customer} from "../models/customer/customer.model";
import { CustomerCategory } from '../models/customerCategory/customer-category.model';

@Injectable({
	providedIn: 'root'
})
export class CustomerService {
	
	constructor( private http: HttpClient ) { }
	
	getAllData(): Observable<any> {
		let customers = this.getAllCustomers()
		return forkJoin([customers])
	}
	getAllCustomers(): Observable<Customer[]> {
		return this.http.get<Customer[]>(CUSTOMER_URL)
	}
	getCustomerByID(id:number): Observable<Customer> {
		return this.http.get<Customer>(`${CUSTOMER_URL}/${id}`)
	}
	deleteCustomer(customerId:number): Observable<any> {
		return this.http.delete(`${CUSTOMER_URL}/${customerId}`)
	}
	createCustomer(data: any): Observable<any> {
		return this.http.post(CUSTOMER_URL, data)
	}
	patchCustomer(customerId:number,data:any): Observable<any> {
		return this.http.patch(`${CUSTOMER_URL}/${customerId}`,data)
	}
	getAllSelected(customerId:number) {
		return forkJoin([])
	}
	getAllCategories():Observable<CustomerCategory[]> {
		return this.http.get<CustomerCategory[]>(CUSTOMERCATEGORY_URL)
	}
}
