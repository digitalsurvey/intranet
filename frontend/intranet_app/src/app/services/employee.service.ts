import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { Employee } from '../models/employee/employee.model';
import { EMPLOYEE_URL} from  './server.constants';

@Injectable({
    providedIn: 'root'
})

export class EmployeeService {

    constructor(private http: HttpClient) { }

    getAllData(): Observable<any> {
        let employees = this.getAllEmployees()
        return forkJoin([employees])
    }
    getAllEmployees(): Observable<Employee[]> {
        return this.http.get<Employee[]>(EMPLOYEE_URL)
    }
    getEmployeeByID(id: number): Observable<Employee> {
        return this.http.get<Employee>(`${EMPLOYEE_URL}/${id}`)
    }
	 getEmployeesByDepartment(departmentId: number): Observable<Employee[]> {
		return this.http.get<Employee[]>(`${EMPLOYEE_URL}/department/${departmentId}`)
	 }
}