import {Observable, ReplaySubject, Subject} from "rxjs";
import {Project} from "../models/project/project.model";


export class RepollProjectsNotificationService {
    subject: Subject<any> = new Subject();
    obs: Observable<any> = this.subject.asObservable();

    notify = (data: any) => {
        this.subject.next(data);
    }
}

export class NewProjectCreatedNotificationService {
    subject: Subject<Project> = new Subject();
    obs: Observable<Project> = this.subject.asObservable();

    notify = (project: Project) => {
        this.subject.next(project);
    }
}