import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbsencesComponent } from '../absences/absences.component';
import { Absence } from '../models/absence/absence.model';
import { AbsenceType } from '../models/absenceType/absence-type.model';
import { ABSENCE_URL, MAIL_URL } from './server.constants';
@Injectable({
	providedIn: 'root'
})
export class AbsenceService {
	
	constructor(private http: HttpClient) { }
	
	
	getAbsenceByEmployee(empId: number, startDate: Date, endDate: Date): Observable<Absence[]>{
		let start = typeof(startDate)=='string' ? startDate : formatDate(startDate,'yyyy-MM-dd',"de-CH")
		let end = typeof(endDate)=='string' ? endDate : formatDate(endDate,'yyyy-MM-dd',"de-CH")
		return this.http.get<Absence[]>(`${ABSENCE_URL}/${empId}&${start}&${end}`)
	}
	getAbsenceTypes(): Observable<AbsenceType[]> {
		return this.http.get<AbsenceType[]>(`${ABSENCE_URL}/type`)
	}
	saveAbsence(data: any): Observable<any> {
		console.log(data)
		return this.http.post(ABSENCE_URL,data)
	}
	getAllAbsences(): Observable<Absence[]> {
		return this.http.get<Absence[]>(ABSENCE_URL)
	}
	updateAbsenceStatus(absenceID: number, newStatus: string): Observable<any> {
		let data = {'status': newStatus}
		return this.http.patch(`${ABSENCE_URL}/${absenceID}`,data)
	}
	deleteAbsence(absenceId: number): Observable<any> {
		return this.http.delete(`${ABSENCE_URL}/${absenceId}`)
	}
	getAbsenceById(absenceId: number): Observable<Absence> {
		return this.http.get<Absence>(`${ABSENCE_URL}/${absenceId}`)
	}
	sendRecursiveMail(absenceDates: Array<{}>, employeeID: number, typeID: number) {
		let data= {'dates':absenceDates,'employeeID':employeeID,'typeID':typeID}
		return this.http.put(MAIL_URL,data)
	}
	getRecursiveAbsences(recursiveID: number): Observable<Absence[]> {
		return this.http.get<Absence[]>(`${ABSENCE_URL}/recursive/${recursiveID}`)
	}
	deleteRecursiveAbsences(id: number): Observable<any> {
		return this.http.delete(`${ABSENCE_URL}/recursive/${id}`)
	}
	updateRecursiveAbsenceStatus(id: number, newStatus: string): Observable<any> {
		let data = {'status': newStatus}
		return this.http.patch(`${ABSENCE_URL}/recursive/${id}`,data)
	}
}
