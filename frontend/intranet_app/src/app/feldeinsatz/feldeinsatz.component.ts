import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CreateFeldeinsatzComponent } from '../modals/feldeinsatz/create-feldeinsatz/create-feldeinsatz.component';
import { Employee } from '../models/employee/employee.model';
import { WochenplanService } from '../services/wochenplan.service';
import { Location } from '../models/location/location.model';
import { FieldTaskService } from '../services/field-task.service';
import { Gnss } from '../models/gnss/gnss.model';
import { FieldTask } from '../models/fieldTask/field-task.model';
import { Car } from '../models/car/car.model';
import { Tachy } from '../models/tachy/tachy.model';
import { DateTime } from 'luxon';
import { EditFeldeinsatzComponent } from '../modals/feldeinsatz/edit-feldeinsatz/edit-feldeinsatz.component';
import { lastValueFrom } from 'rxjs';
import { DeleteComponent } from '../modals/delete/delete.component';

@Component({
	selector: 'app-feldeinsatz',
	templateUrl: './feldeinsatz.component.html',
	styleUrls: ['./feldeinsatz.component.scss']
})
export class FeldeinsatzComponent implements OnInit {
	@ViewChild('createFieldTask') addFieldTaskModal : CreateFeldeinsatzComponent
	@ViewChild('editFieldTask') editFieldTaskModal : EditFeldeinsatzComponent
	@ViewChild('deleteModal') deleteModal: DeleteComponent

	
	dates: Date[] = []
	selectedDate: Date
	weekNumber: number
	today: Date = new Date()
	
	employeeMap: Map<number, Employee>= new Map()
	locationMap: Map<number, Location>= new Map()

	
	cars: Car[]
	tachys: Tachy[]
	gnss: Gnss[]
	fieldTasks: FieldTask[] = []
	
	weeklyFieldTasks: FieldTask[] = []
	// weeklyMorningTasks: FieldTask[][]
	// weeklyAfternoonTasks: FieldTask[][]
	
	
	
	constructor(private fieldTaskService: FieldTaskService, private wochenPlanService: WochenplanService ) {}
	
	ngOnInit(): void {
		let employees: Employee[] = []
		let locations: Location[]

		this.getDates(new Date())
		this.wochenPlanService.getAllData()        
		.subscribe(responseList => {
			employees = responseList[0]
			locations = responseList[4]
			for (let emp of employees) {
				this.employeeMap.set(emp.id,emp)
			}
			for (let loc of locations){
				this.locationMap.set(loc.id,loc)
			}
		})
		this.fieldTaskService.getAllData()
		.subscribe(responseList => {
			this.cars = responseList[0]
			this.tachys = responseList[1]
			this.gnss = responseList[2]
			this.fieldTasks = responseList[3]
			this.getWeeklyFieldTask()
		})		
	}
	changeWeek(direction:string) {
		let newDate = this.selectedDate
		switch(direction) {
			case 'forward':
				newDate = new Date(newDate.setDate(newDate.getDate()+7))
				break;
			case 'backward':
				newDate = new Date(newDate.setDate(newDate.getDate()-7))
				break;
		}
		this.getDates(newDate)
	}
	getDates(newDate?: Date): void {
		if (newDate === undefined) {newDate = new Date()}
		let selDate = newDate
		this.selectedDate = new Date(newDate)
		if(selDate.getDay() == 0) { //when a sunday is chosen get the monday 6 days before
			selDate.setDate(selDate.getDate()-6)
		}
		for (let i=0; i<=6;i++){
			this.dates[i] = new Date(selDate.setDate(selDate.getDate() - selDate.getDay()+1+i))
			this.dates[i].setHours(0,0,0,0)
		}
		this.weekNumber = DateTime.fromJSDate(this.dates[0]).weekNumber
		this.getWeeklyFieldTask()
	}
	compareDates(d1: Date, d2: Date): boolean {
		if(d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear() ){
			return true
		}
		return false
	}
	// getDailyFieldTask(day: Date): [FieldTask[], FieldTask[]] {
	// 	// console.log("getting Daily Tasks for " + day)
	// 	let morningTasks=[]
	// 	let afternoonTasks=[]
	// 	// console.log(this.fieldTasks)
	// 	for (let t of this.fieldTasks){
	// 		if(this.compareDates(new Date(t.date),day)) {
	// 			t.morning ? morningTasks.push(t) : afternoonTasks.push(t)
	// 		}
	// 	}
	// 	return [morningTasks, afternoonTasks]
	// }
	getWeeklyFieldTask(): void {
		// this.fieldTaskService.getWeeklyFieldTasks(this.dates[0].toISOString().split('T')[0], this.dates[this.dates.length-1].toISOString().split('T')[0])
		this.fieldTaskService.getWeeklyFieldTasks(this.dates[0].toISOString().substring(0,10), this.dates[this.dates.length-1].toISOString().substring(0,10))
		.subscribe({
			next: (data) => {
				this.weeklyFieldTasks = data
			}
		})
	}
	async openModalDelete(taskID:number) {
		return await this.deleteModal.open(taskID,'fieldTask')
	}
	
	async openModalCreate(){
		return await this.addFieldTaskModal.open()
	}
	async openModalEdit(id?:number){
		return await this.editFieldTaskModal.open(id)
	}
	copyFieldTask(task:FieldTask) { 
		this.fieldTaskService.setCopiedFieldTask(task)
		this.editFieldTaskModal.openCopied()
	}
}
