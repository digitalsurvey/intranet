import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeldeinsatzComponent } from './feldeinsatz.component';

describe('FeldeinsatzComponent', () => {
  let component: FeldeinsatzComponent;
  let fixture: ComponentFixture<FeldeinsatzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeldeinsatzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeldeinsatzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
