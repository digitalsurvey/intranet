import { CREATE_SHEET_URL } from "../services/bluMAP API";

export function downloadExcel(projectID:number){
    const link = document.createElement('a');
    link.setAttribute('target', '_self');
    link.setAttribute('href', `${CREATE_SHEET_URL}/${projectID}`)
    link.setAttribute('download', `products.csv`);
    document.body.appendChild(link);
    link.click();
    link.remove();
}