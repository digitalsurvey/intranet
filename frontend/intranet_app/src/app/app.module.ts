import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeDECH from '@angular/common/locales/de-CH'
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ShowDataComponent } from './show-data/show-data.component';
import { WochenplanComponent } from './wochenplan/wochenplan.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FeldeinsatzComponent } from './feldeinsatz/feldeinsatz.component';
import { CreateFeldeinsatzComponent } from './modals/feldeinsatz/create-feldeinsatz/create-feldeinsatz.component';
import { EditFeldeinsatzComponent } from './modals/feldeinsatz/edit-feldeinsatz/edit-feldeinsatz.component';
import { ShowFeldeinsatzComponent } from './modals/feldeinsatz/show-feldeinsatz/show-feldeinsatz.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { PrintWochenplanComponent } from './print/print-wochenplan/print-wochenplan.component';
import { DeleteComponent } from './modals/delete/delete.component';
import { DeviceplanComponent } from './deviceplan/deviceplan.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateComponent } from './modals/news/create/create.component';
import { AbsencesComponent } from './absences/absences.component';
import { RequestAbsenceComponent } from './modals/absence/request-absence/request-absence.component';
import { PersonalAbsencesComponent } from './absences/personal-absences/personal-absences.component';
import { ManagerAbsencesComponent } from './absences/manager-absences/manager-absences.component';
import { CustomDateAdapter } from './services/custom-date-adapter';
import {AgGridModule} from "ag-grid-angular";
import { AddressSearchComponent } from './address-search/address-search.component';
import { DetailviewComponent } from './modals/project/detailview/detailview.component';
import { DoneComponent } from './modals/project/done/done.component';
import { PlanningComponent } from './modals/project/planning/planning.component';
import { RealisationComponent } from './modals/project/realisation/realisation.component';
import { SelectStateChangeComponent } from './modals/select-state-change/select-state-change.component';
import { ForgotPasswordComponent } from './project/components/account/forgotPassword.component';
import { LoginComponent } from './project/components/account/login.component';
import { RegisterComponent } from './project/components/account/register.component';
import { MapViewComponent } from './project/components/map-view/map-view.component';
import { ProjectOverviewComponent } from './project/components/projectOverview/projectOverview.component';
import { ProjectStateFilter } from './project/components/projectOverview/state-filter.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CreateProjectComponent } from './modals/project/create-project/create-project.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete'
import { NewProjectCreatedNotificationService, RepollProjectsNotificationService } from './services/repollProjectsNotificationService';


registerLocaleData(localeDECH)

@NgModule({
	declarations: [
		AppComponent,
		PagenotfoundComponent,
		ShowDataComponent,
		WochenplanComponent,
		FeldeinsatzComponent,
		CreateFeldeinsatzComponent,
		EditFeldeinsatzComponent,
		ShowFeldeinsatzComponent,
		PrintLayoutComponent,
		PrintWochenplanComponent,
		DeleteComponent,
		DeviceplanComponent,
		DashboardComponent,
		CreateComponent,
		AbsencesComponent,
		RequestAbsenceComponent,
		PersonalAbsencesComponent,
		ManagerAbsencesComponent,
		ProjectOverviewComponent,
		LoginComponent,
		RegisterComponent,
		ForgotPasswordComponent,
		MapViewComponent,
		ProjectStateFilter,
		DetailviewComponent,
		PlanningComponent,
		RealisationComponent,
		AddressSearchComponent,
		SelectStateChangeComponent,
		DoneComponent,
		CreateProjectComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatProgressSpinnerModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatFormFieldModule,
		MatInputModule,
		ReactiveFormsModule,
		NgbModule,
		AgGridModule,
		FontAwesomeModule,
		MatAutocompleteModule
	],
	providers: [
		{provide: LOCALE_ID, useValue: 'de-CH'},
		{provide: DateAdapter, useClass: CustomDateAdapter},
		RepollProjectsNotificationService,
		NewProjectCreatedNotificationService,
	],
	bootstrap: [AppComponent]
})
export class AppModule { 
	// constructor(private dateAdapter: DateAdapter<Date>) {
	//   this.dateAdapter.setLocale('de-CH')
	// }
}
