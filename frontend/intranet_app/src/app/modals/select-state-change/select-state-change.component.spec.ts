import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectStateChangeComponent } from './select-state-change.component';

describe('SelectStateChangeComponent', () => {
  let component: SelectStateChangeComponent;
  let fixture: ComponentFixture<SelectStateChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectStateChangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectStateChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
