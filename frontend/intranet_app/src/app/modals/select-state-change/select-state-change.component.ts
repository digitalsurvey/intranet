import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
	selector: 'app-select-state-change',
	templateUrl: './select-state-change.component.html',
	styleUrls: ['./select-state-change.component.css']
})
export class SelectStateChangeComponent implements OnInit {
	
	constructor(private modalService: NgbModal) { }
	@ViewChild('selectStateChange') private modalContent: TemplateRef<SelectStateChangeComponent>
	@Output() newStateEmitter = new EventEmitter()
	id: number
	state:string
	modalRef: NgbModalRef
	ngOnInit(): void {
	}
	async open(id:number, state:string) {
		this.id=id
		this.state=state
		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() =>{}, //on close
			() => {this.close()} //on dismiss
		)
	}

	openChangeModal(newState: string){
		let projectId =this.id
		this.newStateEmitter.emit({newState,projectId})
		this.modalRef.close()
	}
	close() {
		this.modalRef.close()
	}
	
}
