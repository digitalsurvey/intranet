import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { lastValueFrom } from 'rxjs';
import { Car } from 'src/app/models/car/car.model';
import { DeviceForTask } from 'src/app/models/deviceForTask/device-for-task.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { FieldTask } from 'src/app/models/fieldTask/field-task.model';
import { Gnss } from 'src/app/models/gnss/gnss.model';
import { Location } from 'src/app/models/location/location.model';
import { Tachy } from 'src/app/models/tachy/tachy.model';
import { Task } from 'src/app/models/task/task.model';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { CreateFeldeinsatzComponent } from '../create-feldeinsatz/create-feldeinsatz.component';

@Component({
  selector: 'app-edit-feldeinsatz',
  templateUrl: './edit-feldeinsatz.component.html',
  styleUrls: ['./edit-feldeinsatz.component.scss']
})
export class EditFeldeinsatzComponent{

  constructor(private wochenplanService: WochenplanService, private modalService: NgbModal, private fieldTaskService: FieldTaskService) { }
  	fieldTask: FieldTask  	
	employees: Employee[]
	locations: Location[]
	cars: Car[]
	tachy: Tachy[]
	availableTachys: Tachy[]
	selectedTachys: Tachy[] = []
	availableCars: Car[]
	selectedCars: Car[] = []
	availableEmployees: Employee[]
	selectedEmployees: Employee[] = []
	availableGnss: Gnss[]
	selectedGnssArray: Gnss[] = []
	deviceForTaskEntries: DeviceForTask[] = []
	employeeChoice: Employee[]
	dateBefore: Date
	timeBefore: string
	
	editFieldTaskForm: UntypedFormGroup =  new UntypedFormGroup({
		orderNO: new UntypedFormControl('', []),
		orderDescription: new UntypedFormControl('', [Validators.required]),
		operator: new UntypedFormControl('',[Validators.required]),
		date: new UntypedFormControl('',[Validators.required]),
		location: new UntypedFormControl('', [Validators.required]),
		time: new UntypedFormControl('',[Validators.required]),
		carAmount: new UntypedFormControl('0', [Validators.required]),
		tachyAmount: new UntypedFormControl('0',[Validators.required]),
		gnssAmount: new UntypedFormControl('0',[Validators.required]),
		employeeAmount: new UntypedFormControl('0',[Validators.required]),
		comment: new UntypedFormControl(''),
		released: new UntypedFormControl(''),
	})
	initialValues = this.editFieldTaskForm.value
	
	modalRef: NgbModalRef
	@ViewChild('editFieldTask') private modalContent: TemplateRef<EditFeldeinsatzComponent>
	@Output() closeEmitter = new EventEmitter();
	
	async planTask(){
		if(!this.fieldTask){
			this.editFieldTaskForm.patchValue({'released':false})
			let data=this.editFieldTaskForm.value
			this.fieldTask = await lastValueFrom(this.fieldTaskService.createFieldTask(data))
		}
		this.deviceForTaskEntries = await lastValueFrom(this.fieldTaskService.getDeviceForTaskEntryByTaskID(this.fieldTask.id))
		let carID, tachyID, gnssID, employeeID
		let entryLength = this.deviceForTaskEntries.length // Count the already existing Entrys in Table to update if exisitng or create if not existing
		let maxAmount = this.selectedCars.length
		if (this.selectedEmployees.length > maxAmount) maxAmount = this.selectedEmployees.length
		if(this.selectedGnssArray.length > maxAmount) maxAmount = this.selectedGnssArray.length
		if (this.selectedTachys.length > maxAmount) maxAmount = this.selectedTachys.length
		if(maxAmount == 0 && entryLength >= 0) {
			for (let i = 0; i<entryLength; i++) {
				this.fieldTaskService.deleteDeviceForTaskEntry(this.deviceForTaskEntries[i].id).subscribe({
					error: e => console.log(e)
				})
			}
		}
		for (let i = 0; i<maxAmount;i++) {
			carID = this.selectedCars[i] ? this.selectedCars[i].id : ''
			employeeID = this.selectedEmployees[i] ? this.selectedEmployees[i].id : ''
			gnssID = this.selectedGnssArray[i] ? this.selectedGnssArray[i].id : ''
			tachyID = this.selectedTachys[i] ? this.selectedTachys[i].id : ''
			let data={
				'fieldTask': this.fieldTask.id,
				'car': carID,
				'tachy': tachyID,
				'gnss': gnssID,
				'employee': employeeID,
			}
			if(entryLength > 0 && entryLength > i)  {//when already entry exists, do a PATCH
				this.fieldTaskService.patchDeviceForTask(this.deviceForTaskEntries[i].id,data).subscribe({
					error: e => console.log(e)
				})
			} else { // if there is not yet an Entry, create one with POST
				this.fieldTaskService.createDeviceForTask(data).subscribe({
					error: e => console.log(e)
				})
			}
		}
		let data
		this.editFieldTaskForm.patchValue({'released':this.checkAssignedDevices()})
		data= this.editFieldTaskForm.value
		this.fieldTaskService.patchFieldTask(this.fieldTask.id,data).subscribe({
			error: e => console.log(e),
			complete: () => {
				this.fieldTask = null
				this.close()
				this.resetForm()
				this.removeSelections()
			}
		})
	}
	checkAssignedDevices(): boolean {
		if (this.selectedEmployees.length == this.editFieldTaskForm.value.employeeAmount && 
			this.selectedCars.length == this.editFieldTaskForm.value.carAmount &&
			this.selectedGnssArray.length == this.editFieldTaskForm.value.gnssAmount &&
			this.selectedTachys.length == this.editFieldTaskForm.value.tachyAmount) 
			{
				return true
			} else {
				return false
			}

	}
	// open modal
	async openCopied() {
		let responseList = await lastValueFrom(this.wochenplanService.getAllData())
		this.employees = responseList[0]
		this.employees.sort((a,b) => {
			return a.username.localeCompare(b.username)
		})

		this.locations = responseList[4]
		this.fieldTask = this.fieldTaskService.getCopiedFieldTask()
		this.setFormData()
		await this.getAvailable(this.fieldTask.date.toString(),this.fieldTask.time,this.fieldTask.location,0)
		this.fieldTask = null //unset FieldTask so that on Save a new one is created
		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() => {this.closeEmitter.emit("close")}, //on close,
			() => { //on dismiss
				this.removeSelections()
				this.resetForm()
			} 		
		)
		
	}
	async open(id?:number) {
		let responseList = await lastValueFrom(this.wochenplanService.getAllData())
		this.employees = responseList[0]
		console.log("emps ", this.employees)
		this.employees.sort((a,b) => {
			return a.username.localeCompare(b.username)
		})
		console.log("sorted", this.employees)
		this.locations = responseList[4]
		

		if (id) { this.openExisting(id)}
		else { this.openNew()}
	}
	openNew(){
		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() => {this.closeEmitter.emit("close")}, //on close,
			() => {
				this.removeSelections()
				this.resetForm()
			} //on dismiss		
		)
	}
	async openExisting(id:number) {
		this.fieldTask = await lastValueFrom(this.fieldTaskService.getFieldTaskByID(id))
		this.timeBefore = this.fieldTask.time
		this.dateBefore = this.fieldTask.date
		this.setFormData()
		this.fieldTaskService.getFieldTaskByID(id)
			.subscribe({
				next: (res) =>{
					this.fieldTask = res
					this.setFormData()
					this.getAvailable(this.fieldTask.date.toString(),this.fieldTask.time,this.fieldTask.location,0)
					this.getSelected()
				},
				error: (e) => {console.log(e)}
				
			})
		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() => {this.removeSelections()}, //on close
			() => {
				this.removeSelections()
				this.resetForm()
			} //on dismiss
		)
	}
	// close modal
	close(): void {
		this.resetForm()
		this.modalRef.close()
		console.log("closing Edit")
		this.closeEmitter.emit("close")
	}
	getSelected(){
		this.fieldTaskService.getAllSelected(this.fieldTask.id).subscribe(responseList => {
			this.selectedTachys = responseList[0]
			this.selectedCars = responseList[1]
			this.selectedGnssArray = responseList[2]
			this.selectedEmployees = responseList[3]
			this.deviceForTaskEntries = responseList[4]
		})
	}
	async getAvailable(date:string,time:string,locationID:number,ignoreTaskId:number){
		let responseList = await lastValueFrom(this.fieldTaskService.getAllAvailable(date,time,locationID,ignoreTaskId))
		this.availableTachys = responseList[0]
		this.availableCars = responseList[1]
		this.availableGnss = responseList[2]
		this.availableEmployees = responseList[3]
	}
	setFormData(){
		console.log(this.fieldTask.operator)
		this.editFieldTaskForm.setValue({
			orderNO: this.fieldTask.orderNO,
			orderDescription: this.fieldTask.orderDescription,
			operator: this.fieldTask.operator,
			// operator: '',
			date: this.fieldTask.date,
			location: this.fieldTask.location,
			time: this.fieldTask.time,
			carAmount: this.fieldTask.carAmount,
			tachyAmount: this.fieldTask.tachyAmount,
			gnssAmount: this.fieldTask.gnssAmount,
			employeeAmount: this.fieldTask.employeeAmount,
			comment: this.fieldTask.comment,
			released: this.fieldTask.released,
		})

	}
	importantValueChange(){
		let newDate=this.editFieldTaskForm.get('date')?.value?.toString()
		let newLoc = this.editFieldTaskForm.get('location')?.value
		let newTime = this.editFieldTaskForm.get('time')?.value
		if(newDate && newLoc && newTime) {
			let existingTaskId = this.fieldTask?.id?this.fieldTask.id:0;
			this.getAvailable(newDate,newTime,newLoc,existingTaskId)
			if((this.fieldTask.time == 'day' && newDate == this.fieldTask.date) 
			|| (this.fieldTask.date == newDate && this.fieldTask.time == newTime )){
			
			} else {

				this.fieldTask.operator = 0
			}
			this.removeSelections()
		}
	}


	selectedEmployee(e:any){
		let emp = this.availableEmployees.find(element => element.id == e.target.value)
		this.selectedEmployees.push(emp)
		this.removeFromArray(this.availableEmployees,e.target.value)
	}
	selectedCar(e:any){
		let car = this.availableCars.find(element => element.id == e.target.value)
		this.selectedCars.push(car)
		this.removeFromArray(this.availableCars,e.target.value)
	}	
	selectedGnss(e:any){
		let gnss = this.availableGnss.find(element => element.id == e.target.value)
		this.selectedGnssArray.push(gnss)
		this.removeFromArray(this.availableGnss,e.target.value)
	}
	selectedTachy(e:any){
		let tachy = this.availableTachys.find(element => element.id == e.target.value)
		this.selectedTachys.push(tachy)
		this.removeFromArray(this.availableTachys,e.target.value)
	}	
	removeSelectedEmployee(index:number) {
		this.availableEmployees.push(this.selectedEmployees[index])
		this.selectedEmployees.splice(index,1)
	}
	removeSelectedCar(index:number) {
		this.availableCars.push(this.selectedCars[index])
		this.selectedCars.splice(index,1)

	}
	removeSelectedGnss(index:number) {
		this.availableGnss.push(this.selectedGnssArray[index])
		this.selectedGnssArray.splice(index,1)
	}
	removeSelectedTachy(index:number) {
		this.availableTachys.push(this.selectedTachys[index])
		this.selectedTachys.splice(index,1)
		console.log(this.selectedTachys.length)
	}
	createNgForCounter(i: number) {// Helper Function to create a "ngFor" in the HTML withouth having an element
		return new Array(i);
	}
	removeFromArray(array:any[],id:number) {
		array.forEach((el, index)=> {
			if(el.id==id){array.splice(index,1)}
		})
	}
	removeSelections() {
		console.log("reomiving selected")
		this.selectedCars = []
		this.selectedEmployees = []
		this.selectedGnssArray= []
		this.selectedTachys=[]
		this.editFieldTaskForm.patchValue({
			operator: ''
		})
	}
	resetForm() {
		this.fieldTask= null
		this.editFieldTaskForm.reset(this.initialValues)
		this.availableCars = []
		this.availableEmployees = []
		this.availableGnss = []
		this.availableTachys = []
	}
	getEmployee(employeeId: number): Employee {
		return this.employees.find(emp => emp.id == employeeId)
	}


}
