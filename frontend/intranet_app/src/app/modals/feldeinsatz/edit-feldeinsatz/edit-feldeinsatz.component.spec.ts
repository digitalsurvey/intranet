import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFeldeinsatzComponent } from './edit-feldeinsatz.component';

describe('EditFeldeinsatzComponent', () => {
  let component: EditFeldeinsatzComponent;
  let fixture: ComponentFixture<EditFeldeinsatzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFeldeinsatzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFeldeinsatzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
