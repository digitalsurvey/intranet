import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Car } from 'src/app/models/car/car.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { Location } from 'src/app/models/location/location.model';
import { Tachy } from 'src/app/models/tachy/tachy.model';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';

@Component({
	selector: 'app-create-feldeinsatz',
	templateUrl: './create-feldeinsatz.component.html',
	styleUrls: ['./create-feldeinsatz.component.scss']
})
export class CreateFeldeinsatzComponent implements OnInit {
	
	employees: Employee[]
	locations: Location[]
	cars: Car[] = []
	tachy: Tachy[]
	
	createFieldTaskForm: UntypedFormGroup =  new UntypedFormGroup({
		orderNO: new UntypedFormControl('', []),
		orderDescription: new UntypedFormControl('', [Validators.required]),
		operator: new UntypedFormControl('',[Validators.required]),
		date: new UntypedFormControl('',[Validators.required]),
		location: new UntypedFormControl('', [Validators.required]),
		time: new UntypedFormControl('',[Validators.required]),
		carAmount: new UntypedFormControl('0', [Validators.required, Validators.max(4)]),
		tachyAmount: new UntypedFormControl('0',[Validators.required, Validators.max(4)]),
		gnssAmount: new UntypedFormControl('0',[Validators.required, Validators.max(4)]),
		employeeAmount: new UntypedFormControl('1',[Validators.required]),
		comment: new UntypedFormControl(''),
		released: new UntypedFormControl(''),
	})
	
	modalRef: NgbModalRef
	@ViewChild('addFieldTask') private modalContent: TemplateRef<CreateFeldeinsatzComponent>
	@Output() closeEmitter = new EventEmitter();
	
	constructor(private modalService: NgbModal, private wochenplanService: WochenplanService, private fieldTaskService: FieldTaskService) { 
	}
	ngOnInit(): void {
	}
	
	// open modal
	open(): void {
		this.wochenplanService.getAllData().subscribe(responseList => {
			this.employees = responseList[0]
			this.locations = responseList[4]
		})
		this.modalRef = this.modalService.open(this.modalContent)
	}
	
	// close modal
	close(): void {
		this.modalRef.close()
		console.log("closing")
		this.closeEmitter.emit("close")
	}
	createTask(): void {
		this.createFieldTaskForm.patchValue({
			released: false,
		})
		let data = this.createFieldTaskForm.value
		this.fieldTaskService.createFieldTask(data)
		.subscribe({
			next: (data) => {
				console.log(data)
			},
			error: (e) => console.log(e),
			complete: () => this.close()
		})
	}
	
}
