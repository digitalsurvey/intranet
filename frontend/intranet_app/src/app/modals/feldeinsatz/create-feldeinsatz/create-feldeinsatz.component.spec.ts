import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFeldeinsatzComponent } from './create-feldeinsatz.component';

describe('CreateFeldeinsatzComponent', () => {
  let component: CreateFeldeinsatzComponent;
  let fixture: ComponentFixture<CreateFeldeinsatzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFeldeinsatzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFeldeinsatzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
