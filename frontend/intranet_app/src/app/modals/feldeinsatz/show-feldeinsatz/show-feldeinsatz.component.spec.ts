import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFeldeinsatzComponent } from './show-feldeinsatz.component';

describe('ShowFeldeinsatzComponent', () => {
  let component: ShowFeldeinsatzComponent;
  let fixture: ComponentFixture<ShowFeldeinsatzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowFeldeinsatzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFeldeinsatzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
