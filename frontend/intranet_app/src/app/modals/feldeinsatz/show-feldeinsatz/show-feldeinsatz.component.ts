import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { lastValueFrom } from 'rxjs';
import { Car } from 'src/app/models/car/car.model';
import { DeviceForTask } from 'src/app/models/deviceForTask/device-for-task.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { FieldTask } from 'src/app/models/fieldTask/field-task.model';
import { Gnss } from 'src/app/models/gnss/gnss.model';
import { Location } from 'src/app/models/location/location.model';
import { Tachy } from 'src/app/models/tachy/tachy.model';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { EditFeldeinsatzComponent } from '../edit-feldeinsatz/edit-feldeinsatz.component';

@Component({
	selector: 'app-show-feldeinsatz',
	templateUrl: './show-feldeinsatz.component.html',
	styleUrls: ['./show-feldeinsatz.component.scss']
})
export class ShowFeldeinsatzComponent implements OnInit {
	
	constructor(private wochenplanService: WochenplanService, private modalService: NgbModal, private fieldTaskService: FieldTaskService) { }
	
	modalRef: NgbModalRef
	@ViewChild('showFieldTask') private modalContent: TemplateRef<ShowFeldeinsatzComponent>
	@ViewChild('editFieldTask') editFieldTaskModal : EditFeldeinsatzComponent

	@Output() closeEmitter = new EventEmitter();
	@Output() copyEventEmitter = new EventEmitter();

	operator: Employee
	// employees: Employee[]
	locations: Location[]
	fieldTask: FieldTask
	selectedCars: Car[]
	selectedEmployees: Employee[]
	selectedTachys: Tachy[]
	selectedGnss: Gnss[]
	deviceForTaskEntries: DeviceForTask[]

	ngOnInit(): void {
		
	}
	
	// open modal
	async open(id:number,){
		// this.employees = await lastValueFrom(this.wochenplanService.getAllEmployees())
		this.locations = await lastValueFrom(this.wochenplanService.getAllLocations())

		this.fieldTask = await lastValueFrom(this.fieldTaskService.getFieldTaskByID(id))
		this.operator = await lastValueFrom(this.wochenplanService.getEmployeeByID(this.fieldTask.operator))
		this.getSelected()

		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() => {}, //on close
			() => {} //on dismiss
			)
		}
		// close modal
		close(): void {
			this.modalRef.close()
			this.closeEmitter.emit("close")
		}
		getSelected(){
			this.fieldTaskService.getAllSelected(this.fieldTask.id).subscribe(responseList => {
				this.selectedTachys = responseList[0]
				this.selectedCars = responseList[1]
				this.selectedGnss = responseList[2]
				this.selectedEmployees = responseList[3]
				this.deviceForTaskEntries = responseList[4]
			})
		}
		copyFieldTask(task:FieldTask) { 
			this.fieldTaskService.setCopiedFieldTask(task)
			this.copyEventEmitter.emit(task)
			this.close()
		}
		async openModalEdit(id?:number){
			this.close();

			return await this.editFieldTaskModal.open(id)
		}
	}
	