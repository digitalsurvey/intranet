import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { last, lastValueFrom } from 'rxjs';
import { FieldTask } from 'src/app/models/fieldTask/field-task.model';
import { AbsenceService } from 'src/app/services/absence.service';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';

@Component({
	selector: 'app-delete',
	templateUrl: './delete.component.html',
	styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
	
	text: String
	type : String
	id:number
	recursive = false
	absenceAmount = 0;
	constructor(private modalService: NgbModal,private wochenplanService: WochenplanService, private fieldTaskService: FieldTaskService, private absenceService: AbsenceService) { }
	
	modalRef: NgbModalRef
	@ViewChild('deleteModal') private modalContent: TemplateRef<DeleteComponent>
	@Output() closeEmitter = new EventEmitter();
	
	ngOnInit(): void {}
	async open(id:number, type: string, recursiveDelete?: boolean) {
		this.id = id
		this.type=type
		this.recursive = recursiveDelete
		switch(this.type){
			case 'fieldTask': {
				let fieldTask =  await lastValueFrom(this.fieldTaskService.getFieldTaskByID(this.id))
				this.text= fieldTask.orderDescription
				break;
			}
			case 'absence': {
				let absence = await lastValueFrom(this.absenceService.getAbsenceById(this.id))
				if (recursiveDelete) {
					let absences
					if(absence.recursiveID){
						absences = await lastValueFrom(this.absenceService.getRecursiveAbsences(absence.recursiveID))
						this.id = absence.recursiveID
					} else {
						absences = await lastValueFrom(this.absenceService.getRecursiveAbsences(absence.id))
						this.id=absence.id
					}
					this.absenceAmount = absences.length			
				}
				let employee = await lastValueFrom(this.wochenplanService.getEmployeeByID(absence.employee.id))
				let types = lastValueFrom(this.absenceService.getAbsenceTypes())
				this.text = (await types).find(el => el.id==absence.type.id).type + " für " + employee.username
				break;
			}
			
		}
		//  this.fieldTask= await lastValueFrom(this.fieldTaskService.getFieldTaskByID(id))
		this.modalRef = this.modalService.open(this.modalContent)
		this.modalRef.result.then(
			() => {}, //on close
			() => {this.close()} //on dismiss
			)  
		}
		
		async deleteObject() {
			//  console.log("deleting id", this.fieldTask.id)
			switch(this.type){
				case 'fieldTask': {
					await lastValueFrom(this.fieldTaskService.deleteFieldTask(this.id))
					break;
				}
				case 'absence': {
					if(this.recursive) {
						await lastValueFrom(this.absenceService.deleteRecursiveAbsences(this.id))
					} else {
						await lastValueFrom(this.absenceService.deleteAbsence(this.id))
					}
					break;
				}
				
			}
			//  await lastValueFrom(this.fieldTaskService.deleteFieldTask(this.fieldTask.id))
			this.close()
		}
		
		close() {
			this.modalRef.close()
			console.log("closing Delete")
			this.closeEmitter.emit("close")
		}
		
	}
	
	
	