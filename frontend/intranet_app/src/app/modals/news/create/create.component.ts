import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, FormControlName } from '@angular/forms';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { lastValueFrom } from 'rxjs';
import { Car } from 'src/app/models/car/car.model';
import { Department } from 'src/app/models/department/department.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { Tachy } from 'src/app/models/tachy/tachy.model';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { NewsService } from 'src/app/services/news.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { CreateFeldeinsatzComponent } from '../../feldeinsatz/create-feldeinsatz/create-feldeinsatz.component';

@Component({
	selector: 'app-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
	
	departments: Department[]
	
	createNewsForm: UntypedFormGroup =  new UntypedFormGroup({
		text: new UntypedFormControl('', [Validators.required]),
		department: new UntypedFormControl('',[]),
		date: new UntypedFormControl('',[Validators.required])
	})
	
	modalRef: NgbModalRef
	@ViewChild('createNews') private modalContent: TemplateRef<CreateComponent>
	@Output() closeEmitter = new EventEmitter();
	
	constructor(private modalService: NgbModal, private newsService: NewsService, private wochenPlanService: WochenplanService) { 
	}
	async ngOnInit() {
		this.departments = await lastValueFrom(this.wochenPlanService.getAllDepartments())
	}
	
	// open modal
	open(): void {
		this.modalRef = this.modalService.open(this.modalContent)
	}
	
	// close modal
	close(): void {
		this.modalRef.close()
		this.closeEmitter.emit("close")
	}
	createNewsEntry(): void {
		let data = this.createNewsForm.value
		this.newsService.createNews(data)
		.subscribe({
			next: (data) => {
				console.log(data)
			},
			error: (e) => console.log(e),
			complete: () => this.close()
		})
	}
	
}
