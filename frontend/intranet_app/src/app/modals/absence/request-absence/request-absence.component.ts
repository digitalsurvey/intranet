import { Component, OnInit, Output, TemplateRef, ViewChild,EventEmitter } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AbsenceType } from 'src/app/models/absenceType/absence-type.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { AbsenceService } from 'src/app/services/absence.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { lastValueFrom, map, Observable, startWith, timer } from 'rxjs';
import { formatDate } from '@angular/common';

@Component({
	selector: 'app-request-absence',
	templateUrl: './request-absence.component.html',
	styleUrls: ['./request-absence.component.scss']
})
export class RequestAbsenceComponent implements OnInit {
	
	
	
	modalRef: NgbModalRef
	@ViewChild('requestAbsence') private modalContent: TemplateRef<RequestAbsenceComponent>
	@Output() closeEmitter= new EventEmitter()
	datesOkay: boolean = false
	formValid: boolean = false
	employees: Employee[]
	filteredEmployees: Observable<Employee[]>
	absenceTypes: AbsenceType[]
	requestAbsenceForm: UntypedFormGroup = new UntypedFormGroup({
		employee: new UntypedFormControl('',[Validators.required]),
		start: new UntypedFormControl('', [Validators.required]),
		end: new UntypedFormControl('', [Validators.required]),
		type: new UntypedFormControl('', [Validators.required]),
		status: new UntypedFormControl('open', [])
		
	})
	initialDAta = this.requestAbsenceForm.value
	recursiveAbsence: boolean = false
	recursiveType: number=0 //default Value weekly
	recursiveStart: Date
	endingType: string
	recursiveEnd: Date
	numberOfOccurences: number
	recursiveID: number
	recursiveAbsenceDates: Array<{}> = []
	weekDays = [
		{name: "Mo", selected:false,value:1},
		{name: "Di", selected:false,value:2},
		{name: "Mi",selected:false,value:3},
		{name: "Do", selected:false,value:4},
		{name: "Fr", selected:false, value:5},
		{name: "Sa", selected:false, value:6},
		{name: "So",selected:false, value:0},
	]
	checkedDays=0
	
	constructor(private modalService: NgbModal, private wochenplanService: WochenplanService, private absenceService: AbsenceService) { }
	
	ngOnInit(): void {

	}
	createFilteredList() {
		this.filteredEmployees = this.requestAbsenceForm.get('employee').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)
	}
	filterEmployee(text: string): Employee[] {
		let filterValue=text.toLowerCase()
		return this.employees.filter(emp => 
			(emp.username.toLowerCase().includes(filterValue) || 
			emp.firstname.toLowerCase().includes(filterValue) || 
			emp.lastname.toLowerCase().includes(filterValue))
		)
	}
	displayEmployee(e: Employee): string {
		return e ? e.username + ' - ' + e.firstname + ' ' + e.lastname: ''
	}
	open(){
		this.wochenplanService.getAllEmployees().subscribe({
			next: (response) => {
			this.employees = response
			this.createFilteredList()
			}
		})
		this.absenceService.getAbsenceTypes().subscribe({
			next: (response) => this.absenceTypes=response
		})
		this.absenceService.getAbsenceTypes()
		this.modalRef = this.modalService.open(this.modalContent)
	}
	close(){
		this.modalRef.close()
		this.closeEmitter.emit("close")
	}
	
	async submitForm(){
		if(!this.recursiveAbsence){
			let data = this.requestAbsenceForm.value
			data.recursive = false
			data.start = formatDate(this.requestAbsenceForm.get('start').value, 'yyyy-MM-dd', 'de-CH')
			data.end = formatDate(this.requestAbsenceForm.get('end').value, 'yyyy-MM-dd', 'de-CH')
			// data.employee = data.employee.id
			if (this.absenceTypes.find(el => el.id == data.type.id).confirm) {
				data.status='open'
			} else {
				data.status='accepted'
			}
			this.absenceService.saveAbsence(data).subscribe({
				error: (e) => {
					console.log(e)
					alert(e.error.message)
					this.modalRef.close()
					this.closeEmitter.emit('close')
					this.requestAbsenceForm.reset(this.initialDAta)
					
				},
				complete: () => {
					this.modalRef.close()
					this.closeEmitter.emit('close')
					this.requestAbsenceForm.reset(this.initialDAta)
					
				}
			})
		} else {
			if (this.recursiveType == 1) { //bi-weekly recurring Absence
				let recursiveID //for identifying the first entry of the absences
				let selectedDays = this.getRecurringDays()
				if(this.endingType=="endDate"){
					let formStartDate = null
					let formEndDate = null 
					let finish = false // variable to wheter following Date is also in recurring absence
					let loopDate = new Date(this.recursiveStart)
					let dayCounter = 1 //for counting up to 7 so that only every 2nd week is considered
					// if(selectedDays.includes(loopDate.getDay())){formStartDate=loopDate}
					while(loopDate <= this.recursiveEnd){
						if(selectedDays.includes(loopDate.getDay())){//if the next day is also included in week
							if(formStartDate){
								formEndDate=new Date(loopDate)
							} else {
								formStartDate=new Date(loopDate)
							}
						} else { //next day not inlcuded
							if(formStartDate){
								formEndDate=(new Date(loopDate))
								formEndDate.setDate(formEndDate.getDate()-1)
								finish=true
							} else {
							}
						}
						if(finish){ //when no followings day is found send absence Block
							await this.saveRecursiveAbsence(formStartDate, formEndDate)
							formEndDate = null
							formStartDate = null
							finish=false
						}
						dayCounter++
						if(dayCounter >7){ //when sunday is finished
							loopDate.setDate(loopDate.getDate()+8) //set date to monday 1 week later
							dayCounter=1
						}
						else{
							loopDate.setDate(loopDate.getDate()+1)//get the next day 
						}
						
					}
					if(formStartDate) {
						formEndDate = this.recursiveEnd
						await this.saveRecursiveAbsence(formStartDate, formEndDate)
						formEndDate = null
						formStartDate = null
						recursiveID= null
						finish=false
					}
				}
				if(this.endingType=="occurences"){
					let formStartDate = null
					let formEndDate = null
					let finish = false // variable to wheter following Date is also in recurring absence
					let loopDate = new Date(this.recursiveStart)
					let absenceDayAmount = 0
					// if(selectedDays.includes(loopDate.getDay())){
					// 	formStartDate=loopDate
					// 	absenceDayAmount++
					// } //add first Day if it is included in weekdays
					let dayCounter = 1 //for counting up to 7 so that only every 2nd week is considered
					while(absenceDayAmount < this.numberOfOccurences){
						if(selectedDays.includes(loopDate.getDay())){//if the next day is also included in week
							if(formStartDate){
								formEndDate=new Date(loopDate)
								absenceDayAmount++
							} else {
								formStartDate=new Date(loopDate)
								absenceDayAmount++
							}
							//formStartDate?formEndDate=loopDate:formStartDate=loopDate //if no startDate has been set set it, otherwise set the end date to the loop date since it is on a following day
						} else { //next day not inlcuded
							if(formStartDate){
								formEndDate=(new Date(loopDate))
								formEndDate.setDate(formEndDate.getDate()-1)
								finish=true
							} else {
							}
						}
						if(finish){
							await this.saveRecursiveAbsence(formStartDate, formEndDate)
							formEndDate = null
							formStartDate = null
							finish=false
						}
						dayCounter++
						if(dayCounter >7){ //when sunday is finished
							loopDate.setDate(loopDate.getDate()+8) //set date to monday 1 week later
							dayCounter=1
						}
						else{
							loopDate.setDate(loopDate.getDate()+1)//get the next day 
						}
					}
					if(formStartDate) { //if the last day was an absence
						if(!formEndDate){formEndDate = new Date(loopDate.setDate(loopDate.getDate()-1))} // when amount of Occurences has been reached, set the last day
						await this.saveRecursiveAbsence(formStartDate, formEndDate)
						formEndDate = null
						formStartDate = null
						recursiveID= null
						finish=false
					}
				}
				
			}
			if (this.recursiveType == 0) { //weekly recurring Absence
				let first = true //var to create define the first absence for the mail 
				let recursiveID //for identifying the first entry of the absences
				let selectedDays = this.getRecurringDays()
				if(this.endingType=="endDate"){
					let formStartDate = null
					let formEndDate = null 
					let finish = false // variable to wheter following Date is also in recurring absence
					let loopDate = new Date(this.recursiveStart)
					// if(selectedDays.includes(loopDate.getDay())){formStartDate=loopDate}
					while(loopDate <= this.recursiveEnd){
						if(selectedDays.includes(loopDate.getDay())){//if the next day is also included in week
							if(formStartDate){
								formEndDate=new Date(loopDate)
							} else {
								formStartDate=new Date(loopDate)
							}
							//formStartDate?formEndDate=loopDate:formStartDate=loopDate //if no startDate has been set set it, otherwise set the end date to the loop date since it is on a following day
						} else { //next day not inlcuded
							if(formStartDate){
								formEndDate=(new Date(loopDate))
								formEndDate.setDate(formEndDate.getDate()-1)
								finish=true
							} else {
							}
						}
						if(finish){ //when no followings day is found send absence Block
							await this.saveRecursiveAbsence(formStartDate, formEndDate)
							formEndDate = null
							formStartDate = null
							finish=false
						}
						loopDate.setDate(loopDate.getDate()+1) //get the next day 
					}
					if(formStartDate) {
						formEndDate = this.recursiveEnd
						await this.saveRecursiveAbsence(formStartDate, formEndDate)
						formEndDate = null
						formStartDate = null
						recursiveID= null
						finish=false
					}
				}
				if(this.endingType=="occurences"){
					let formStartDate = null
					let formEndDate = null
					let finish = false // variable to wheter following Date is also in recurring absence
					let loopDate = new Date(this.recursiveStart)
					let absenceDayAmount = 0
					// if(selectedDays.includes(loopDate.getDay())){
					// 	formStartDate=loopDate
					// 	absenceDayAmount++
					// } //add first Day if it is included in weekdays
					while(absenceDayAmount < this.numberOfOccurences){
						if(selectedDays.includes(loopDate.getDay())){//if the next day is also included in week
							if(formStartDate){
								formEndDate=new Date(loopDate)
								absenceDayAmount++
							} else {
								formStartDate=new Date(loopDate)
								absenceDayAmount++
							}
							//formStartDate?formEndDate=loopDate:formStartDate=loopDate //if no startDate has been set set it, otherwise set the end date to the loop date since it is on a following day
						} else { //next day not inlcuded
							if(formStartDate){
								formEndDate=(new Date(loopDate))
								formEndDate.setDate(formEndDate.getDate()-1)
								finish=true
							} else {
							}
						}
						if(finish){
							await this.saveRecursiveAbsence(formStartDate, formEndDate)
							formEndDate = null
							formStartDate = null
							finish=false
						}
						loopDate.setDate(loopDate.getDate()+1) //get the next Day
					}
					if(formStartDate) { //if the last day was an absence
						if(!formEndDate){formEndDate = new Date(loopDate.setDate(loopDate.getDate()-1))} // when amount of Occurences has been reached, set the last day
						await this.saveRecursiveAbsence(formStartDate, formEndDate)
						formEndDate = null
						formStartDate = null
						recursiveID= null
						finish=false
					}
				}
				
			}		
			
			this.absenceService.sendRecursiveMail(this.recursiveAbsenceDates, this.requestAbsenceForm.get('employee').value.id, this.requestAbsenceForm.get('type').value).subscribe({
				error: (e) => alert("Mail konnte nicht gesandt werden"),
			})
		}
		this.modalRef.close()
		this.closeEmitter.emit('close')
		this.resetForm()
	}
	addRecursiveAbsenceDate(start: string, end: string){ //for genereating an Array with start and End Dates for a recursive absence which then is used for sending an Email
		this.recursiveAbsenceDates.push({start: start, end: end})
	} 
	getRecurringDays(): number[]{	
		let selectedDays= []			
		for (let day of this.weekDays) {
			day.selected ? selectedDays.push(day.value):'' //put the day of the week in the Array, where Sunday =0 , Saturday =6, can be used with date.getDay()
		}
		return selectedDays
	}
	
	async saveRecursiveAbsence(start: Date,end:Date){
		let startDate = formatDate(start,'yyyy-MM-dd',"de-CH")
		let endDate = formatDate(end,'yyyy-MM-dd',"de-CH")
		
		this.requestAbsenceForm.patchValue({
			start:startDate,
			end: endDate
		})
		
		let data = this.requestAbsenceForm.value
		data.recursive = true
		if(this.recursiveID!=null){
			data.recursiveID = this.recursiveID
		}
		if (this.absenceTypes.find(el => el.id == data.type.id).confirm) {
			data.status='open'
		} else {
			data.status='accepted'
		}
		//check if employee already has absences on those dates 
		let absences = await lastValueFrom(this.absenceService.getAbsenceByEmployee(data.employee.id, data.start, data.end))
		if(absences.length===0){
			this.addRecursiveAbsenceDate(startDate,endDate)
			// let createdAbsence = await lastValueFrom(this.absenceService.saveAbsence(data))
			let createdAbsence
			try {
				createdAbsence = await lastValueFrom(this.absenceService.saveAbsence(data))
			} catch (e) {
				alert("Mail konnte nicht gesandt werden")
			}
			if(!this.recursiveID){
				this.recursiveID=createdAbsence.id
			} else {
			}
		} else {
			alert("An den Daten " + data.start + " bis " + data.end + " besteht bereits eine Abwesenheit. Es wird nichts verändert",)
		}
	}
	resetForm() {
		this.requestAbsenceForm.reset(this.initialDAta)
		this.recursiveAbsenceDates=[] // delete dates 
		this.recursiveAbsence = false
		this.datesOkay  = false
		this.formValid = false
		this.numberOfOccurences = null
	}
	async checkDate(){
		let absences
		let data = this.requestAbsenceForm.value
		// if(data.end && data.start > data.end){
		// 	this.requestAbsenceForm.get('end').reset()
		// }
		if(this.requestAbsenceForm.valid) {
			// console.log("start", data.start, "end", data.end)
			absences = await lastValueFrom(this.absenceService.getAbsenceByEmployee(data.employee.id, data.start, data.end))
			// console.log("absences", absences)
			// absences.length==0? this.datesOkay=true:this.datesOkay=false
			this.datesOkay = absences.length==0? true:false
			
		}
	}
	checkForm(){
		let res = false
		if(!this.recursiveAbsence) {
			if(this.requestAbsenceForm.valid && this.checkDate()){
				res= true
			}
		} else{
			if(this.checkedDays > 0 && this.requestAbsenceForm.get('employee').value.id && this.requestAbsenceForm.get('type').value){
				if(this.endingType == 'endDate'){
					if(this.recursiveStart <= this.recursiveEnd){
						res= true
					}
				}
				if(this.endingType == 'occurences'){
					if(this.recursiveStart && this.numberOfOccurences > 0){
						res= true
					}
				}
			}
		}
		this.formValid = res
	}
	changeType(e:any){
		this.recursiveType=e.target.value
		this.checkForm()
	}
	changeWeekDay(day:string){
		let checked=0
		for(let d of this.weekDays){
			
			if(d.name == day){d.selected=!d.selected}
			d.selected? checked++:''
		}
		this.checkedDays=checked
		this.checkForm()
	}

}
