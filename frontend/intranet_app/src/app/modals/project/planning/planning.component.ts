import {
	Component,
	EventEmitter,
	OnDestroy,
	OnInit,
	Output,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from "../../../services/project.service";
import {Employee} from "../../../models/employee/employee.model";
import {WochenplanService} from "../../../services/wochenplan.service";
import {lastValueFrom, startWith, map, Observable} from "rxjs";
import {DatePipe} from "@angular/common";
import {CustomerService} from "../../../services/customer.service";
import {Customer} from "../../../models/customer/customer.model";
import {RepollProjectsNotificationService} from "../../../services/repollProjectsNotificationService";
import {Project, ProjectState} from "../../../models/project/project.model";
import {ProjectTask} from "../../../models/projectTask/projectTask.model";
import { AdditionalCost } from '../../../models/additionalCost/additionalCost.model';
import { Department } from 'src/app/models/department/department.model';
import { DepartmentService } from 'src/app/services/department.service';
import { AccountService } from 'src/app/services/account.service';
import { User } from 'src/app/models/user/user.model';
import { downloadExcel } from 'src/app/functions/downloadExcel';



@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {
	downloadExcel  = downloadExcel
  	employees: Employee[]
	filteredCoReferent: Observable<Employee[]>
	filteredManager: Observable<Employee[]>
	filteredManagerDep: Observable<Employee[]>
  	projectId: number
	project: Project
	edit: boolean = false
	departments: Department[]
	customers: Customer[]
	plannedManager: Employee
	plannedManagerDep: Employee
	additionalCostCosts: number
	additionalCostDesc: string
	allAdditionalCosts: AdditionalCost[] = []
	projectTaskTitle: string
	projectTaskDesc: string
	allProjectTasks: ProjectTask[] = []
	selectedProjectTasks: ProjectTask[] = []
	user: User


  createPlanningForm: UntypedFormGroup =  new UntypedFormGroup({
		projectId: new UntypedFormControl('', []), //	
		currState: new UntypedFormControl(ProjectState.planning, [Validators.maxLength(20)]),
		
		title: new UntypedFormControl('', [Validators.required, Validators.maxLength(50)]),
		manager: new UntypedFormControl('', [Validators.required]),
		managerDep: new UntypedFormControl(null, []),
		coReferent: new UntypedFormControl(null, [Validators.required]),
		
		//creationDate: new FormControl('', [Validators.required]), //TODO Move to create
		planningStartDate: new UntypedFormControl('',[]),
		estimatedDoneDate: new UntypedFormControl('',[]),
		effectiveCosts: new UntypedFormControl(0, [Validators.min(0)]), // Honorarsumme
		comment: new UntypedFormControl('', []),
		offerState: new UntypedFormControl(''),

		// linked relations
		projectTags: new UntypedFormControl('', []) ,
		referredProjects: new UntypedFormControl('', []),
		projectTasks: new UntypedFormControl('' ,[])
	});
	initialValues=this.createPlanningForm.value

	additionalCostForm: UntypedFormGroup = new UntypedFormGroup({
		project: new UntypedFormControl('', []), 
		costs: new UntypedFormControl('', []), 
		description: new UntypedFormControl('', []), 
		creationDate: new UntypedFormControl('', []),
	});

	projectTaskForm: UntypedFormGroup = new UntypedFormGroup({
		title: new UntypedFormControl('', []), 
		description: new UntypedFormControl('', []), 
	});

	projectTasksForm: UntypedFormGroup = new UntypedFormGroup({
		project: new UntypedFormControl('', []), 
		task: new UntypedFormControl('', []), 
	});

  modalRef: NgbModalRef
	@ViewChild('planProject') private modalContent: TemplateRef<PlanningComponent>
	@Output() closeEmitter = new EventEmitter();

  constructor(
	private modalService: NgbModal,
	// private wochenplanService: WochenplanService,
	private projectService: ProjectService,
	private customerService: CustomerService,
	private repollProjectsNotificationService: RepollProjectsNotificationService,
	// private departmentService: DepartmentService,
	private accountService: AccountService
  ) {  }

  ngOnInit(): void {
  }
  touchRequiredFields(){
	this.createPlanningForm.get('coReferent').markAsTouched()
	this.createPlanningForm.get('manager').markAsTouched()

}

  // open modal
	async open(edit?: boolean) {
		this.touchRequiredFields()
		this.user = this.accountService.userValue
		// this.employees = await lastValueFrom(this.wochenplanService.getAllEmployees())
		this.employees = await lastValueFrom(this.projectService.getAllEmployees())
		this.createFilterLists()
		// this.departments = await lastValueFrom(this.departmentService.getAllDepartments())
		this.customers = await lastValueFrom(this.customerService.getAllCustomers())
		this.project = await lastValueFrom(this.projectService.getProjectByID(this.projectId));
		this.allProjectTasks = await lastValueFrom(this.projectService.getAllProjectTasks())
		this.project.projectTasks.forEach(task => {
			this.addProjectTaskToProject(task)
		})
		this.plannedManager=this.project.plannedManager
		this.plannedManagerDep=this.project.plannedManagerDep
		if(edit){
			this.edit = edit
			this.fillData(this.project)
		} else {
			this.fillData(this.project)
			this.createPlanningForm.patchValue({
				manager: this.plannedManager,
				managerDep: this.plannedManagerDep
			})
		}
		this.modalRef = this.modalService.open(this.modalContent, { size: 'xl', backdrop: 'static', scrollable: true, centered: true })

		this.resyncAdditionalCosts()

		this.modalRef.result.then(
			() => {
				this.closeEmitter.emit("close")
				this.createPlanningForm.reset(this.initialValues)
			}, //on close,
			() => {
				this.createPlanningForm.reset(this.initialValues)
				this.close()
			} 
		)
	}
  	close(): void {
		this.modalRef.close()
		this.closeEmitter.emit("close")
	}

	async resyncAdditionalCosts(){
		this.allAdditionalCosts = await lastValueFrom(this.projectService.getAllAdditionalCosts(this.projectId));
	}

	addProjectAdditionalCost() {
		let datePipe = new DatePipe('en-US');
		this.additionalCostForm.patchValue({
			project: this.projectId,
			costs: this.additionalCostCosts,
			description: this.additionalCostDesc,
			creationDate: datePipe.transform(Date.now(), 'YYYY-MM-dd'),
		})
		let data = this.additionalCostForm.value
		this.projectService.createAdditionalCosts(data).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncAdditionalCosts()
		})
	}
	
	removeProjectAdditionalCost( additionalCostId: number) {
		this.projectService.deleteAdditionalCosts(additionalCostId).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncAdditionalCosts()
		})
	}

	async resyncProjectTasks(){
		this.allProjectTasks = await lastValueFrom(this.projectService.getAllProjectTasks());
	}

	addProjectTask() {
		this.projectTaskForm.patchValue({
			title: this.projectTaskTitle,
			description: this.projectTaskDesc,
		})
		let data = this.projectTaskForm.value
		this.projectService.createProjectTask(data).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncProjectTasks()
		})
	}

	addProjectTaskToProject(task: ProjectTask) {
		this.selectedProjectTasks.push(task)
		this.allProjectTasks.splice(this.allProjectTasks.findIndex(projectTask => projectTask.id == task.id),1)
	}
	
	removeProjectTask( task: ProjectTask) {
		this.selectedProjectTasks.splice(this.selectedProjectTasks.findIndex(projectTask => projectTask = task),1)
		this.allProjectTasks.push(task)
	}

  createPlanningInstance(): void {
		let datePipe = new DatePipe('en-US');
		
		// if(!this.createPlanningForm.get('estimatedDoneDate').value) this.createPlanningForm.removeControl('estimatedDoneDate') // remove when emtpy
		this.createPlanningForm.patchValue({
			currState: 'planning', // ProjectState.acquisition cannot get enum key as string
			planningStartDate: datePipe.transform(Date.now(), 'YYYY-MM-dd'),
			estimatedDoneDate: datePipe.transform(this.createPlanningForm.get('estimatedDoneDate').value, 'YYYY-MM-dd'),
			offerState: 'accepted',

			// coReferent: this.createPlanningForm.get('coReferent').value.id,
			// manager: this.createPlanningForm.get('manager').value.id,
			// managerDep: this.createPlanningForm.get('managerDep').value.id,
			projectTasks: this.selectedProjectTasks,
		})

		let data = this.createPlanningForm.value
		console.log("data", data)
		this.projectService.patchProject(this.projectId,data)
			.subscribe({
				next: (data: any) => {
					this.repollProjectsNotificationService.notify(null);
				},
				error: (e: any) => console.log(e),
				complete: () => this.close()
			})
	}

	setProjectId(projectId: number): void{
		this.projectId = projectId;
	}
	getDepartmentName(depId: number): string {
		return this.departments.find(d => d.id == depId).name
	}
	// getCustomerName(custId: number): string {
	// 	let customer = this.customers?.find( e=> e.id == custId)
	// 	return customer.description == null ? (customer.person.firstname + ' ' + customer.person.lastname) : customer.description 
	// }
	filterEmployee(text: string): Employee[] {
		// let filterValue=text.toLowerCase()
		return this.employees.filter(emp => 
			emp.username.toLowerCase().includes(text) || 
			emp.firstname.toLowerCase().includes(text) || 
			emp.lastname.toLowerCase().includes(text))
	}
	displayEmployee(e: Employee): string {
		return e ? e.username + ' - ' + e.firstname + ' ' + e.lastname: ''
	}
	createFilterLists(){
		this.filteredCoReferent = this.createPlanningForm.get('coReferent').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)
		this.filteredManager = this.createPlanningForm.get('manager').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)		
		this.filteredManagerDep = this.createPlanningForm.get('managerDep').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)	
	}
	deleteProjectTask(task: ProjectTask){
		if(confirm(`Sind Sie sicher dass Sie:\n"${task.description}"\nlöschen möchten?`)){
			this.projectService.deleteProjectTask(task.id).subscribe({
				error: (error) => alert(error),
				complete: () => {
					this.allProjectTasks.splice(this.allProjectTasks.findIndex(projectTask => projectTask.id == task.id),1)
				}
			})
		}
	}
	fillData(project: Project) {
		this.createPlanningForm.patchValue({
			projectId: project.id,
			currState: project.currState,
			title: project.title,
			manager: project.manager,
			managerDep: project.managerDep,
			coReferent: project.coReferent,
			estimatedDoneDate: project.estimatedDoneDate,
			effectiveCosts: project.effectiveCosts, // Honorarsumme
			comment: project.comment
		})
	}
}
