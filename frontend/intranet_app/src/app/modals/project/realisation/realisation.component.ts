import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	Pipe,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from "../../../services/project.service";
import {Employee} from "../../../models/employee/employee.model";
import {WochenplanService} from "../../../services/wochenplan.service";
import {lastValueFrom, Observable, startWith, map} from "rxjs";
import {DatePipe} from "@angular/common";
import {OfferState, Project, ProjectState} from "../../../models/project/project.model";
import {RepollProjectsNotificationService} from "../../../services/repollProjectsNotificationService";
import { Customer } from 'src/app/models/customer/customer.model';
import { Department } from 'src/app/models/department/department.model';
import { CustomerService } from 'src/app/services/customer.service';
import { DepartmentService } from 'src/app/services/department.service';
import { downloadExcel } from 'src/app/functions/downloadExcel';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-realisation',
  templateUrl: './realisation.component.html',
  styleUrls: ['./realisation.component.css']
})
export class RealisationComponent implements OnInit {
	downloadExcel = downloadExcel
	employees: Employee[]
	filteredForeman: Observable<Employee[]>
	filteredForemanDep: Observable<Employee[]>
	edit: boolean
	projectId: number;
	project: Project;
	allReferenceProjects: Project[] = []
	allProjects: Project[] = []
	plannedForeman: Employee
	plannedForemanDep: Employee
	customers: Customer[]
	departments: Department[]

  createRealisationForm: UntypedFormGroup =  new UntypedFormGroup({
		// offerState: new FormControl(OfferState.accepted, [Validators.required, Validators.maxLength(20)]),
		comment: new UntypedFormControl('', []),
		currState: new UntypedFormControl('', []),
		realisationStartDate: new UntypedFormControl('', []),
		foreman: new UntypedFormControl('', []),
		foremanDep: new UntypedFormControl('', []),

    	isReferenceWeb: new UntypedFormControl(false, []),
		isReferenceTrainee: new UntypedFormControl(false, []),
    
	});

	referenceProjectForm: UntypedFormGroup = new UntypedFormGroup({
		projectLocal: new UntypedFormControl('', []), 
		projectRef: new UntypedFormControl('', []), 
	});

  modalRef: NgbModalRef
	@ViewChild('realizeProject') private modalContent: TemplateRef<RealisationComponent>
	@Output() closeEmitter = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    // private wochenplanService: WochenplanService,
    private projectService: ProjectService,
    private repollProjectsNotificationService: RepollProjectsNotificationService,
	//  private departmentService: DepartmentService,
	 private customerService: CustomerService,


  ) { }

  ngOnInit(): void {
  }

  async open(edit?: boolean) {
		// this.employees = await lastValueFrom(this.wochenplanService.getAllEmployees())
		this.employees = await lastValueFrom(this.projectService.getAllEmployees())
		this.createFilterLists()
		this.customers = await lastValueFrom(this.customerService.getAllCustomers())
		// this.departments = await lastValueFrom(this.departmentService.getAllDepartments())
		this.project = await lastValueFrom(this.projectService.getProjectByID(this.projectId));
		this.modalRef = this.modalService.open(this.modalContent, { size: 'xl', backdrop: 'static', scrollable: true, centered: true })
		// this.plannedForeman = this.employees.find(e => e.id == this.project.plannedForeman)
		// this.plannedForemanDep = this.employees.find( e => e.id == this.project.plannedForemanDep)
		this.plannedForeman = this.project.plannedForeman
		this.plannedForemanDep = this.project.plannedForemanDep
		this.resyncProject()
		if(edit){
			this.edit=edit
			this.fillData(this.project)
		} else {
			this.createRealisationForm.patchValue({
				foreman: this.plannedForeman,
				foremanDep: this.plannedForemanDep
			})
		}
		this.modalRef.result.then(
			() => {this.closeEmitter.emit("close")}, //on close,
			() => {} 
		)
	}
  close(): void {
		this.modalRef.close()
		this.closeEmitter.emit("close")
	}

  createRealisationInstance(): void {
		let datePipe = new DatePipe('en-US');

		this.createRealisationForm.patchValue({
			currState: 'realisation', // ProjectState.acquisition cannot get enum key as string
			realisationStartDate: datePipe.transform(Date.now(), 'YYYY-MM-dd'),
			// foreman: this.createRealisationForm.get('foreman').value.id,
			// foremanDep: this.createRealisationForm.get('foremanDep').value.id,
		})

    let data = this.createRealisationForm.value
		this.projectService.patchProject(this.projectId, data)
			.subscribe({
				next: (data: any) => {
					this.repollProjectsNotificationService.notify(null);
				},
				error: (e: any) => console.log(e),
				complete: () => this.close()
			})
	}


	async resyncProject(){
		this.projectService.getReferenceProjects(this.projectId).subscribe({
			next: (data: any) => {
				this.allReferenceProjects = []
				data.forEach((element: any) => {
					this.projectService.getProjectByID(element.projectRef).subscribe({
						next: (data: any) => {
							this.allReferenceProjects.push(data);
						},
						error: (e: any) => console.log(e),
					});
				});
			},
			error: (e: any) => console.log(e)
		})
		this.allProjects = await lastValueFrom(this.projectService.getAllProjects());
	}

	addReferenceProject(rProjectId:number){
		this.referenceProjectForm.patchValue({
			projectLocal: this.projectId,
			projectRef: rProjectId,
		})
		let data = this.referenceProjectForm.value
		this.projectService.addReferenceProjects(data).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncProject()
		})
	}

	removeReferenceProject(rProjectId:number){
		this.projectService.removeReferenceProject(this.projectId, rProjectId).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncProject()
		})
	}

  setProjectId(projectId: number): void{
		this.projectId = projectId;
	}
	getDepartmentName(depId: number): string {
		return this.departments.find(d => d.id == depId).name
	}
	// getCustomerName(custId: number): string {
	// 	let customer = this.customers?.find( e=> e.id == custId)
	// 	return customer.description == null ? (customer.person.firstname + ' ' + customer.person.lastname) : customer.description 
	// }
	filterEmployee(text: string): Employee[] {
		let filterValue=text.toLowerCase()
		return this.employees.filter(emp => 
			emp.username.toLowerCase().includes(filterValue) || 
			emp.firstname.toLowerCase().includes(filterValue) || 
			emp.lastname.toLowerCase().includes(filterValue))
	}
	displayEmployee(e: Employee): string {
		return e ? e.username + ' - ' + e.firstname + ' ' + e.lastname: ''
	}
	createFilterLists(){
		this.filteredForeman = this.createRealisationForm.get('foreman').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)
		this.filteredForemanDep = this.createRealisationForm.get('foremanDep').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)		
	}
	fillData(project: Project) {
		this.createRealisationForm.patchValue({
			comment: project.comment,
			currState: project.currState,
			realisationStartDate: project.realisationStartDate,
			// foreman: this.employees.find(emp => emp.id == project.foreman),
			// foremanDep: this.employees.find(emp => emp.id == project.foremanDep),
			foreman: project.foreman,
			foremanDep: project.foremanDep,
			isReferenceWeb: project.isReferenceWeb,
			isReferenceTrainee: project.isReferenceTrainee
		})
	}

}

