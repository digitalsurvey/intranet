import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, lastValueFrom, startWith, map } from 'rxjs';
import { Customer } from 'src/app/models/customer/customer.model';
import { Department } from 'src/app/models/department/department.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { Project } from 'src/app/models/project/project.model';
import { CustomerService } from 'src/app/services/customer.service';
import { ProjectService } from 'src/app/services/project.service';
import { RepollProjectsNotificationService } from 'src/app/services/repollProjectsNotificationService';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { downloadExcel } from 'src/app/functions/downloadExcel';
@Component({
	selector: 'app-done',
	templateUrl: './done.component.html',
	styleUrls: ['./done.component.css']
})
export class DoneComponent implements OnInit {
	downloadExcel = downloadExcel
	employees: Employee[]
	filteredForeman: Observable<Employee[]>
	filteredForemanDep: Observable<Employee[]>
	edit: boolean
	denied: boolean // for chekcing if Offer was even accepted
	projectId: number;
	project: Project;
	allReferenceProjects: Project[] = []
	allProjects: Project[] = []
	plannedForeman: Employee
	plannedForemanDep: Employee
	customers: Customer[]
	departments: Department[]
	filteredInvoiceEmployee: Observable<Employee[]>

	finishProjectForm: UntypedFormGroup =  new UntypedFormGroup({
		isReferenceWeb: new UntypedFormControl(false, []),
		isReferenceTrainee: new UntypedFormControl(false, []),
		comment: new UntypedFormControl('', []),
		offerState: new UntypedFormControl('')
	});
	invoiceForm: UntypedFormGroup = new UntypedFormGroup({
		number: new UntypedFormControl('',[]),
		amount: new UntypedFormControl('',[]),
		date: new UntypedFormControl('',[]),
		employee: new UntypedFormControl()
	})
	
	referenceProjectForm: UntypedFormGroup = new UntypedFormGroup({
		projectLocal: new UntypedFormControl('', []), 
		projectRef: new UntypedFormControl('', []), 
	});
	
	modalRef: NgbModalRef
	@ViewChild('finishProjects') private modalContent: TemplateRef<DoneComponent>
	@Output() closeEmitter = new EventEmitter();
	
	constructor(
		private modalService: NgbModal,
		// private wochenplanService: WochenplanService,
		private projectService: ProjectService,
		private repollProjectsNotificationService: RepollProjectsNotificationService,
		private customerService: CustomerService,) { }
		
	ngOnInit(): void {}		
	async open(edit: boolean) {
		// this.employees = await lastValueFrom(this.wochenplanService.getAllEmployees())
		this.employees = await lastValueFrom(this.projectService.getAllEmployees())
		this.createFilteredInvoiceEmployee()
		this.project = await lastValueFrom(this.projectService.getProjectByID(this.projectId));
		this.modalRef = this.modalService.open(this.modalContent, { size: 'xl', backdrop: 'static', scrollable: true, centered: true })
		this.resyncProject()
		if(edit){
			this.edit=edit
			this.fillData(this.project)
		}
		this.modalRef.result.then(
			() => {this.closeEmitter.emit("close")}, //on close,
			() => {})
	}
	close(): void {
		this.modalRef.close()
		this.closeEmitter.emit("close")
	}
	finishProject(){
		let datePipe = new DatePipe('en-US');
		if(this.project.currState == 'acquisition'){
			this.finishProjectForm.patchValue({
				offerState: 'denied',
			})
		}
		let data = this.finishProjectForm.value
		data.doneDate = datePipe.transform(Date.now(), 'YYYY-MM-dd')
		data.currState = 'done'
		console.log(data)

		this.projectService.patchProject(this.projectId, data).subscribe({
			next: (resp) => {
				console.log(resp)
				this.repollProjectsNotificationService.notify(null);
			},
			error: (e: any) => console.log(e),
			complete: () => {
				this.close()
			}
		})
	}	
	async resyncProject(){
		this.projectService.getReferenceProjects(this.projectId).subscribe({
			next: (data: any) => {
				this.allReferenceProjects = []
				data.forEach((element: any) => {
					this.projectService.getProjectByID(element.projectRef).subscribe({
						next: (data: any) => {
							this.allReferenceProjects.push(data);
						},
						error: (e: any) => console.log(e),
					});
				});
			},
			error: (e: any) => console.log(e)
		})
		this.allProjects = await lastValueFrom(this.projectService.getAllProjects());
	}
	addReferenceProject(rProjectId:number){
		this.referenceProjectForm.patchValue({
			projectLocal: this.projectId,
			projectRef: rProjectId,
		})
		let data = this.referenceProjectForm.value
		this.projectService.addReferenceProjects(data).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncProject()
		})
	}
	removeReferenceProject(rProjectId:number){
		this.projectService.removeReferenceProject(this.projectId, rProjectId).subscribe({
			next: (data:any) => {
			},
			error: (e: any) => console.log(e),
			complete: () => this.resyncProject()
		})
	}	
	setProjectId(projectId: number): void{
		this.projectId = projectId;
	}
	getDepartmentName(depId: number): string {
		return this.departments.find(d => d.id == depId).name
	}
	// getCustomerName(custId: number): string {
	// 	return this.customers.find(c => c.id == custId).description
	// }
	filterEmployee(text: string): Employee[] {
		let filterValue=text.toLowerCase()
		return this.employees.filter(emp => 
			emp.username.toLowerCase().includes(filterValue) || 
			emp.firstname.toLowerCase().includes(filterValue) || 
			emp.lastname.toLowerCase().includes(filterValue)
		)
	}
	displayEmployee(e: Employee): string {
		return e ? e.username + ' - ' + e.firstname + ' ' + e.lastname: ''
	}
	createFilteredInvoiceEmployee() {
		this.filteredInvoiceEmployee = this.invoiceForm.get('employee').valueChanges.pipe(
			startWith(''),
			map(value => {
				let filter = typeof value === 'string' ? value: value?.username
				return filter ? this.filterEmployee(filter) : this.employees.slice()
			})
		)
	}
	async deleteInvoice(invoiceId: number){
		this.projectService.deleteInvoice(this.projectId,invoiceId).subscribe({
			next: async (resp) => {

				this.project = await lastValueFrom(this.projectService.getProjectByID(this.projectId))
			},
			error: (err) => console.log(err)
		})
	}
	async addInvoice(projectId:number){
		let datePipe = new DatePipe('en-US');
		this.invoiceForm.patchValue({
			date: datePipe.transform(this.invoiceForm.get('date').value,'YYYY-MM-dd')
		})
		if(this.invoiceForm.get('amount').value==''){this.invoiceForm.patchValue({amount:0})}
		let data = this.invoiceForm.value
		this.projectService.addInvoice(data).subscribe({
			next: (response) => {
				this.projectService.addInvoiceToProject(projectId, response.id).subscribe({
					next: async () => this.project = await lastValueFrom(this.projectService.getProjectByID(this.projectId))
				})
			},
			error: (e) => {
				console.log(e)
			},
			complete: async () => {
				this.invoiceForm.reset()
			}
		})
	}
	fillData(project: Project) {
		this.finishProjectForm.patchValue({
			comment: project.comment,
			isReferenceWeb: project.isReferenceWeb,
			isReferenceTrainee: project.isReferenceTrainee
		})
	}
		
}
			
			