import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import Draw from 'ol/interaction/Draw';
import { RepollProjectsNotificationService } from 'src/app/services/repollProjectsNotificationService';

import { CreateProjectComponent } from './create-project.component';

describe('CreateProjectComponent', () => {
  let component: CreateProjectComponent;
  let fixture: ComponentFixture<CreateProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateProjectComponent ],
      imports: [HttpClientTestingModule],
      providers: [RepollProjectsNotificationService]
    })
	 
    .compileComponents();    
    TestBed.inject(RepollProjectsNotificationService)
  });

  beforeEach(async() => {
    fixture = TestBed.createComponent(CreateProjectComponent);
    component = fixture.componentInstance;
    component.ngOnInit()
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
