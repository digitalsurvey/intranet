import {Component,EventEmitter,Output,TemplateRef,ViewChild} from '@angular/core';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from "../../../services/project.service";
import {Employee} from "../../../models/employee/employee.model";
import {Department} from "../../../models/department/department.model";
import {Location} from "../../../models/location/location.model";
// import {WochenplanService} from "../../../services/wochenplan.service";
import {Company} from "../../../models/company/company.model";
import {lastValueFrom, map, startWith, Observable, pipe} from "rxjs";
import {CurrencyPipe, DatePipe} from "@angular/common";
import {CustomerService} from "../../../services/customer.service";
import {Customer, CustomerType} from "../../../models/customer/customer.model";
import {NewProjectCreatedNotificationService,RepollProjectsNotificationService} from "../../../services/repollProjectsNotificationService";
import {OfferState, Project, ProjectState} from "../../../models/project/project.model";
import { Feature, Map as geoMap} from 'ol';
import { Tag } from 'src/app/models/tag/tag.model';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import proj4 from 'proj4';
import {register} from 'ol/proj/proj4';
import {get as getProjection} from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Draw, Snap, Modify, Select} from 'ol/interaction'
import { Geometry, GeometryCollection, Polygon} from 'ol/geom';
import WKT from 'ol/format/WKT'
import TileWMS from 'ol/source/TileWMS';
import { HttpClient } from '@angular/common/http';
import GeoJSON from 'ol/format/GeoJSON';
import {MapService} from "../../../services/map.service";
import WMTS, { optionsFromCapabilities } from 'ol/source/WMTS';
import WMTSCapabilities from 'ol/format/WMTSCapabilities';
import { fromExtent } from 'ol/geom/Polygon';
import { Gemeinde } from 'src/app/models/gemeinde/gemeinde.model';
import { GemeindeService } from 'src/app/services/gemeinde.service';
import { createStyle, hoverStyle, townshipStyle } from 'src/app/project/components/map-view/styles';
import { pointerMove, singleClick } from 'ol/events/condition';
import { CostCenter } from 'src/app/models/costCenter/cost-center.model';
import { CostCenterService } from 'src/app/services/cost-center.service';
import { CustomerCategory } from 'src/app/models/customerCategory/customer-category.model';
import { ProjectCustomer } from 'src/app/models/projectCustomer/project-customer.model';
import { objectValidator } from 'src/app/services/validators';
import { downloadExcel } from 'src/app/functions/downloadExcel';
export enum CustomerFormFor {
	client,
	company
}

@Component({
	selector: 'app-create-project',
	templateUrl: './create-project.component.html',
	styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent{
	downloadExcel = downloadExcel
	townshipHover: string|number
	employees: Employee[]
	filteredCreators: Observable<Employee[]>
	filteredForeman: Observable<Employee[]>
	filteredForemanDep: Observable<Employee[]>
	filteredManager: Observable<Employee[]>
	filteredManagerDep: Observable<Employee[]>
	filteredTownship:Observable<Gemeinde[]>
	filteredCostCenter: Observable<CostCenter[]>
	filteredCustomers: Observable<Customer[]>
	allProjects: Project[] = []
	filteredReferenceProjects: Project[]=[]
	referenceProjects: Project[]=[]
	costCenters: CostCenter[] = []
	departments: Department[]
	companies: Company[]
	locations: Location[]
	customers: Customer[]
	selectedCustomers: Customer[] = []
	selectedCompanies: Customer[] = []
	customerCategories: CustomerCategory[] = []
	projectCustomers: ProjectCustomer[] =[]
	selectedProjectCustomers: ProjectCustomer[]=[]
	projectCompanyCustomers: ProjectCustomer[] = []
	allTags: Tag[] = []
	selectedTags: Tag[] = []
	filteredTags: Tag[] = []
	projectTags: Map<number,Tag>
	openedProject : Project
	geometryType: String = 'Polygon'
	map: geoMap;
	searchText: string=''
	view: View;
	draw: Draw;
	visibleFeatures: number=0;
	vectorSource: VectorSource
	featureCounter: number
	vectorLayer: any
	townshipSource: VectorSource
	townshipLayer: any
	selectHover : Select
	selectClick : Select
	mapExtent: any
	wkt: WKT = new WKT()
	geometryCollection: GeometryCollection
	geometryArray: Geometry[] = []
	showCustomerForm: boolean = false
	personForm:boolean = true


	createProjectForm: UntypedFormGroup =  new UntypedFormGroup({

		acquisitionId: new UntypedFormControl('', [Validators.maxLength(50)]),
		title: new UntypedFormControl('', [Validators.required, Validators.maxLength(50)]),
		costCenter: new UntypedFormControl('', [Validators.required,objectValidator()]),
		township: new UntypedFormControl('', [Validators.required,objectValidator()]),
		estimatedDoneDate: new UntypedFormControl('',[]),	
		offerCreator: new UntypedFormControl(null),
		offerState: new UntypedFormControl('open', [Validators.maxLength(20)]),
		SIAphase: new UntypedFormControl('', [Validators.maxLength(254)]),
		estimatedCosts: new UntypedFormControl(0, [Validators.min(0)]),
		acceptedCosts: new UntypedFormControl(0, [Validators.min(0)]),
		// saveLocation: new FormControl('',[Validators.required]),
		comment: new UntypedFormControl('', []),

		customer: new UntypedFormControl('', []),
		clientContact: new UntypedFormControl('', [Validators.maxLength(1023)]),
		// company: new FormControl('', []),
		companyContact: new UntypedFormControl('', [Validators.maxLength(1023)]),

		planningGroup: new UntypedFormControl('', []),
		plannedManager: new UntypedFormControl('',[Validators.required,objectValidator()]),
		plannedManagerDep: new UntypedFormControl(null,[objectValidator(true)]),
		plannedForeman: new UntypedFormControl(null,[objectValidator(true)]),
		plannedForemanDep: new UntypedFormControl(null,[objectValidator(true)]),

		geolocation: new UntypedFormControl(),
		
		// linked relations
		projectTags: new UntypedFormControl('', []) /* estimated competences */,
		referredProjects: new UntypedFormControl('', []),
		
		// not displayed in creation form but needed for DB
		currState: new UntypedFormControl(ProjectState.acquisition, [Validators.maxLength(20)]),
		projectId: new UntypedFormControl('', [Validators.maxLength(50)]),
		manager: new UntypedFormControl(null, ),
		managerDep: new UntypedFormControl(null, ),
		foreman: new UntypedFormControl(null, ),
		foremanDep: new UntypedFormControl(null, ),
		coReferent: new UntypedFormControl(null, ),
		taskDescription: new UntypedFormControl('', ),
		creationDate: new UntypedFormControl('', ),
		planningStartDate: new UntypedFormControl(null, ),
		realisationStartDate: new UntypedFormControl(null, ),
		doneDate: new UntypedFormControl(null, ),
		isReferenceWeb: new UntypedFormControl(false, ),
		isReferenceTrainee: new UntypedFormControl(false, ),
		department: new UntypedFormControl(null, ),

	});
	initialValues = this.createProjectForm.value

	createCustomerForm: UntypedFormGroup = new UntypedFormGroup({
		category: new UntypedFormControl('Private', []),
		title: new UntypedFormControl('mister', []),
		firstname: new UntypedFormControl(null, []),
		lastname: new UntypedFormControl(null, []),
		email: new UntypedFormControl(null, [Validators.email]),
		phone: new UntypedFormControl(null, []),
		companyName: new UntypedFormControl(null, []),
		street: new UntypedFormControl(null, []),
		zip: new UntypedFormControl(null, []),
		city: new UntypedFormControl(null, []),
	})
	

	custTypeEnum = CustomerType;
	custFormForEnum = CustomerFormFor;
	offerStateEnum = OfferState;
	orig:any
	
	modalRef: NgbModalRef
	@ViewChild('createProject') private modalContent: TemplateRef<CreateProjectComponent>
	@Output() closeEmitter = new EventEmitter();
	
	constructor(
		private modalService: NgbModal,
		// private wochenplanService: WochenplanService,
		private projectService: ProjectService,
		private customerService: CustomerService,
		private repollProjectsNotificationService: RepollProjectsNotificationService,
		private newProjectCreatedNotificationService: NewProjectCreatedNotificationService,
		private http: HttpClient,
		private mapService: MapService,
		private gemeindeService: GemeindeService,
		private costCenterService: CostCenterService
		) { }

		filterEmployee(text: string): Employee[] {
			let filterValue=text.toLowerCase()
			return this.employees.filter(emp => 
				(emp.username.toLowerCase().includes(filterValue) || 
				emp.firstname.toLowerCase().includes(filterValue) || 
				emp.lastname.toLowerCase().includes(filterValue)) &&
				emp.role.role != 'Sachbearbeiter')
		}
		filterCustomer(text:string): Customer[] {
			let filterValue = text.toLowerCase()
			let filteredCustomers:Customer[] = []
			this.customers.forEach(cust =>{
				if(cust.category.name == 'Intern' || cust.category.name == 'Private'){
					if(cust.firstname.toLowerCase().includes(filterValue) || cust.lastname.toLowerCase().includes(filterValue)){
						filteredCustomers.push(cust)
					}
				} else if (cust.companyName.toLowerCase().includes(filterValue)){
					filteredCustomers.push(cust)
				}
			})
			return filteredCustomers
			// return this.customers.filter(cust =>

			// 	cust.firstname.toLowerCase().includes(filterValue) ||
			// 	cust.lastname.toLowerCase().includes(filterValue) ||
			// 	cust?.companyName.toLowerCase().includes(filterValue))
		}
		filterTownship(text:string): Gemeinde[] {
			let filterValue=text.toLowerCase()
			return this.gemeindeService.townships.filter(t => t.name.toLowerCase().includes(filterValue))
		}
		filterCostCenter(text:string): CostCenter[] {
			let filterValue = text.toLowerCase()
			return this.costCenters.filter(c => c.name.toLowerCase().includes(filterValue))
		}
		displayEmployee(e: Employee): string {
			return e ? e.username + ' - ' + e.firstname + ' ' + e.lastname: ''
		}
		displayCustomer(cust: Customer): string {
			let string
			if (cust.category.name == 'Intern' || cust.category.name == 'Private'){
				string = cust.firstname + ' ' + cust.lastname
			} else {
				string = cust.companyName
			}
			string += + ' (' + cust.category.name + ')'
			return string
		}
		displayTownship(township: Gemeinde): string {
			return township.name
		}
		displayCostCenter(costCenter: CostCenter): string {
			return costCenter.name
		}
		createFilteredCustomer(){
			this.filteredCustomers = this.createProjectForm.get('customer').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.firstname
					return filter ? this.filterCustomer(filter): this.customers.slice()
				})
			)
		}
		createFilterLists(){
			this.filteredCreators = this.createProjectForm.get('offerCreator').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.username
					return filter ? this.filterEmployee(filter) : this.employees.slice()
				})
			)
			this.createFilteredCustomer()
			this.filteredForeman = this.createProjectForm.get('plannedForeman').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.username
					return filter ? this.filterEmployee(filter) : this.employees.slice()
				})
			)		
			this.filteredForemanDep = this.createProjectForm.get('plannedForemanDep').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.username
					return filter ? this.filterEmployee(filter) : this.employees.slice()
				})
			)
			this.filteredManager = this.createProjectForm.get('plannedManager').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.username
					return filter ? this.filterEmployee(filter) : this.employees.slice()
				})
			)
			this.filteredManagerDep = this.createProjectForm.get('plannedManagerDep').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.username
					return filter ? this.filterEmployee(filter) : this.employees.slice()
				})
			)
			this.filteredTownship = this.createProjectForm.get('township').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string' ? value: value?.name
					return filter? this.filterTownship(filter): this.gemeindeService.townships?.slice()
				})
			)
			this.filteredCostCenter = this.createProjectForm.get('costCenter').valueChanges.pipe(
				startWith(''),
				map(value => {
					let filter = typeof value === 'string'? value: value.name
					return filter? this.filterCostCenter(filter): this.costCenters.slice()
				})
			)
		
		}
		touchRequiredFields(){
			this.createProjectForm.get('costCenter').markAsTouched()
			this.createProjectForm.get('township').markAsTouched()
			this.createProjectForm.get('plannedManager').markAsTouched()
		}
		// open modal
		async open(openedProject?: Project) {
			this.touchRequiredFields()
			this.selectedCustomers = []
			this.selectedCompanies = []
			this.referenceProjects = []
			this.geometryArray  =[]
			this.selectedTags = []
			this.filteredTags= []
			this.allTags=  []
			this.filteredReferenceProjects= []
			this.vectorSource=new VectorSource()
			this.featureCounter=0
			this.vectorSource.on('addfeature', event =>{
				let feature = event.feature
				this.geometryArray.push(feature.getGeometry())
				feature.set("name", "draw"+this.featureCounter)
				this.geometryCollection = new GeometryCollection(this.geometryArray)
				this.featureCounter++
			})
			this.customerService.getAllCategories().subscribe({
				next: (response) => this.customerCategories = response
			})
			// this.employees= await lastValueFrom(this.wochenplanService.getAllEmployees())
			this.employees= await lastValueFrom(this.projectService.getAllEmployees())

			let allProjects = await lastValueFrom(this.projectService.getAllProjects())
			allProjects.forEach(project =>{
				this.allProjects.push(project)
				this.filteredReferenceProjects.push(project)
			})
			this.customers= await lastValueFrom(this.customerService.getAllCustomers())
			this.createFilterLists()
			// this.wochenplanService.getAllData().subscribe({
			// 	next: (responseList) => {
			// 		// this.employees=responseList[0]
			// 		// this.departments=responseList[2]
			// 		// this.companies=responseList[3]
			// 		this.locations=responseList[4]
			// 	}
			// })
			this.costCenters = await lastValueFrom(this.costCenterService.getAllCostCenters())
			let allTags = await lastValueFrom(this.projectService.getAllTags())
			allTags.forEach(tag => {
				this.allTags.push(tag)
				this.filteredTags.push(tag)
			})
			if(openedProject)  {
				this.openedProject=openedProject
				this.fillData(openedProject)
				this.orig=openedProject.geolocation
				this.projectCustomers=openedProject.client
				for(let client of this.openedProject.client) {
					this.selectedCustomers.push(client.customer)
					this.removeFromArray(this.customers,client.customer.id)
					// let elem = <HTMLInputElement>document.getElementById(`contact-${client.customer.id}`)
					// elem.value = client.contact?client.contact:''
				}
				for(let company of this.openedProject.company) {
					this.selectedCompanies.push(company.customer)
					this.removeFromArray(this.customers,company.customer.id)
				}
				for(let refProject of this.openedProject.referenceProject) {
					this.referenceProjects.push(this.allProjects.find(cust => cust.id ==refProject))
				}
				for(let tag of this.openedProject.tags){
					this.addProjectTag(tag)
				}
				let wkt = new WKT()
				let geomCollection = wkt.readGeometry(openedProject.geolocation.split(';')[1]) as GeometryCollection
				geomCollection.getGeometries().forEach(pol => {
					let feature = new Feature(pol)
					feature.setStyle(createStyle)
					this.vectorSource.addFeature(feature)
				})

			}

			this.projectTags = new Map;
			
			this.modalRef = this.modalService.open(this.modalContent, { size: 'xl', backdrop: 'static', scrollable: true, centered: true })
			
			this.modalRef.result.then(
				() => {//on close,
					this.closeEmitter.emit("close")
					this.map = undefined //need to be done otherwise on second open of modal map is not shown
					this.selectedCustomers =[]
					this.selectedCompanies = []
					this.projectCompanyCustomers = []
					this.projectCustomers = []
					this.geometryArray = []
					this.createProjectForm.reset(this.initialValues)
				}, 
				() => {  //on dismiss
					this.map = undefined //need to be done otherwise on second open of modal map is not shown
					this.selectedCustomers =[]
					this.selectedCompanies = []
					this.projectCompanyCustomers = []
					this.projectCustomers = []
					this.geometryArray = []
					this.createProjectForm.reset(this.initialValues)

				}
			)
		}
		// close modal
		close(): void {	
			this.modalRef.close()
			this.closeEmitter.emit("close")
		}
		createProjectInstance(): void {
				let datePipe = new DatePipe('en-US');
				this.createProjectForm.patchValue({
					department: this.createProjectForm.get('costCenter').value.department,
					currState: 'acquisition', // ProjectState.acquisition --> can not get enum key as string
					creationDate: datePipe.transform(Date.now(), 'YYYY-MM-dd'),
					estimatedDoneDate: datePipe.transform(this.createProjectForm.get('estimatedDoneDate').value, 'YYYY-MM-dd'),
					geolocation: this.wkt.writeGeometry(this.geometryCollection),
				})
				let data = this.createProjectForm.value
				if(this.openedProject){
					data.client = this.projectCustomers
					data.company = this.projectCompanyCustomers
					for(let project of this.openedProject.referenceProject){ //remove all current Projects that are not selected
						if(!(this.referenceProjects.find(proj => proj.id == project))){
							this.projectService.removeProjectReferenceFromProject(this.openedProject.id, project).subscribe()
						}
					}
					for (let project of this.referenceProjects){
						if(!(this.openedProject.referenceProject.includes(project.id))){
							this.projectService.addProjectReferenceToProject(this.openedProject.id,project.id).subscribe()
						}
					}
					for (let tag of this.openedProject.tags){ // remove all current Tags that are not selected
						if(!(this.selectedTags.find(t => t.id == tag.id))){
							this.projectService.deleteProjectTag(this.openedProject.id, tag.id).subscribe()
						}
					}
					for (let tag of this.selectedTags){
						if(!(this.openedProject.tags.includes(tag))){
							this.projectService.addProjectTag(this.openedProject.id, tag.id).subscribe()
						}
					}

					this.projectService.patchProject(this.openedProject.id,data).subscribe({
						next: () => {
							this.mapService.ngOnInit()
							.then(() => this.repollProjectsNotificationService.notify(null))
							// .then(() => this.newProjectCreatedNotificationService.notify(data))

						},
						error:(e) => console.log(e),
						complete: () => {
							this.closeEmitter.emit()
							this.close()

						}
					})
				} else {
				this.projectService.createProject(data).subscribe({
					next: (data) => {
						let projectId = data['id']
						for(let customer of this.projectCustomers) {
							this.projectService.addProjectCustomerToProject(projectId, customer.id).subscribe({
								next: (r) => console.log(r),
								error: (e) => console.log(e)
							})
						}
						for(let company of this.projectCompanyCustomers){
							this.projectService.addProjectCustomerToProject(projectId, company.id).subscribe({
								error: (err) => console.log(err)
							})
						}
						for(let refProject of this.referenceProjects) {
							this.projectService.addProjectReferenceToProject(projectId, refProject.id).subscribe()
						}
						for(let tag of this.selectedTags){
							this.projectService.addProjectTag(projectId, tag.id).subscribe()
						}
						this.mapService.ngOnInit()
							.then(() => this.repollProjectsNotificationService.notify(null))
							.then(() => this.newProjectCreatedNotificationService.notify(data))
					},
					error: (e) => console.log(e),
					complete: () => this.close()
					})
				}
		}
		updateMapSize(){
			this.map.updateSize()
		}
		async createMap(){
			if(!this.map){
					proj4.defs("EPSG:2056","+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs");
					register(proj4)
					this.view = new View({
						projection: getProjection("EPSG:2056"),
						center: [2660013.54, 1185171.98],
						zoom: 1,
						extent: [2485071.58, 1075346.31,2828515.82, 1299941.79]
					})
					let swissTopo = new TileLayer({
						source: new TileWMS({
							url: "https://wms.geo.admin.ch/",
							params: {
								"LAYERS": "ch.swisstopo.pixelkarte-grau",
								"TILED": true,
							}
						}),
						zIndex:0,
						maxZoom:18 
						// additional parameter opacity could be set, like opacity= 0.3
					})
					this.townshipSource = new VectorSource()
					this.townshipLayer = new VectorLayer({
						source: this.townshipSource,
						opacity:0.7,
						zIndex: 9,
						maxZoom: 17
						// minZoom: 11
					})

					this.vectorLayer = new VectorLayer({
						source: this.vectorSource,
						style: createStyle,
						zIndex:10
					})
					let epsg2056 = getProjection("EPSG:2056")
					let resp = await fetch(`https://wmts.geo.admin.ch/EPSG/2056/1.0.0/WMTSCapabilities.xml`)
					let capablilities = new WMTSCapabilities().read(await resp.text())
					let options = optionsFromCapabilities(capablilities, {
						layer: "ch.kantone.cadastralwebmap-farbe",
						matrixSet: epsg2056
					})
					let parcels = new TileLayer({
						source: new WMTS(options),
						minZoom:17,
						opacity:1,
						zIndex:1
					})
					let modify = new Modify({
						source:this.vectorSource
					})
					this.map = new geoMap({
						layers: [swissTopo, this.vectorLayer, parcels, this.townshipLayer],
						target: 'map',
						view: this.view
					})
					this.selectHover = new Select({
						layers: [this.townshipLayer],
						condition: pointerMove,
						style: hoverStyle,
					})
					this.selectHover.on('select', e =>{
						if(e.selected.length > 0){
							this.townshipHover=e.selected[0].getId()
						} else {
							this.townshipHover=''
						}
					})
					this.selectClick = new Select({
						layers: [this.townshipLayer],
						condition: (e) => {return singleClick(e) && this.geometryType == 'Township'},
						style: townshipStyle
					})
					this.selectClick.on('select', event => {
						if(this.geometryType == "Township"){
							if(event.selected.length == 1 && !this.vectorSource.getFeatureById(event.selected[0].getId())) {
								let feature = new Feature(event.selected[0].getGeometry())
								feature.setStyle(createStyle)
								feature.setId(event.selected[0].getId())
								this.vectorSource.addFeature(feature)
							}
						}
					})
					// this.map.addInteraction(selectHover)
					// this.map.addInteraction(selectClick)
					let selectedTownship = this.createProjectForm.get('township').value
					if(selectedTownship){
						let wkt = new WKT()
						let feature = wkt.readFeature(selectedTownship.wkb_geometry.split(';')[1])// because DB stores as "SRID=2056;MULTIPOLYGON(...)" therefore need to split at ;
						feature.setStyle(townshipStyle)
						feature.setId(selectedTownship.name)
						this.townshipSource.addFeature(feature)
						let extent = fromExtent(feature.getGeometry().getExtent())
						extent.scale(2)
						this.map.getView().fit(extent)
					}
					if(this.vectorSource.getFeatures().length>0){
						let f = fromExtent(this.vectorSource.getExtent())
						f.scale(2)
						this.map.getView().fit(f)
					}
					this.map.addInteraction(modify)
					this.addInteraction()
			}
		}
		addTownships(){
			if(this.townshipSource.getFeatures().length <= 1000){
				let selectedTownship = this.createProjectForm.get('township').value
				this.gemeindeService.townships.forEach(township => {
					if(selectedTownship.name != township.name){
						let feature = this.wkt.readFeature(township.wkb_geometry.split(';')[1])
						feature.setId(township.name)
						this.townshipSource.addFeature(feature)
					}
				})
			}
		}
		geometryTypeChange(){
			this.map.removeInteraction(this.draw)
			
			if(this.geometryType != 'Township') {
				this.addInteraction()
				this.map.removeInteraction(this.selectHover)
				this.map.removeInteraction(this.selectClick)
			} else {
				this.map.addInteraction(this.selectHover)
				this.map.addInteraction(this.selectClick)
			}
		}
		handleSearchResultClicked(view: Polygon) {
			this.map.getView().fit(view)
		}
		addInteraction(){
			this.draw = new Draw({
				source: this.vectorSource,
				type: this.geometryType,
				condition: (event) => {
					if(event.originalEvent.buttons === 2){
						this.draw.removeLastPoint()
						return false
					} else {
						return true
					}
				},
				style: createStyle
			})
			let snap = new Snap({
				source: this.vectorSource
			})
			this.map.addInteraction(this.draw)
			this.map.addInteraction(snap)
		}
		
			
		setCustomerFormVisibility(show:boolean) {
			this.showCustomerForm = show
		}
		customerClientTypeChange(ev: any) {
			switch (ev.target.value) {
				case 'Private':
				case 'Intern':
					this.personForm=true;
					break;
				default:
					this.personForm=false
					// this.createCustomerClientForm.patchValue({person: null});
				break;
			}
		}
		getCustomerContact(customerId:number): string {
			let customer = this.openedProject.client.find(c => c.customer.id == customerId)
			if(customer){
				// return this.openedProject.client.find(c => c.customer.id == customerId).contact
				return customer.contact
			} else {
				return ''
			}
		}
		getCustomerCompanyContact(customerId:number): string { 
			let company = this.openedProject.company.find(c=> c.customer.id == customerId)
			if(company) {
				return company.contact
			} else {
				return ''
			}
		}
			
		createCustomer() {
			let data = null;
			let cat = this.createCustomerForm.get('category').value
			let categoryObject = this.customerCategories.find(category => category.name == cat)
			this.createCustomerForm.patchValue({'category':categoryObject})
			data= this.createCustomerForm.value
			console.log(data)
			if (data != null) {
				this.customerService.createCustomer(data)
				.subscribe({
					next: (data) => {
						console.log("created customer", data)
						// this.customers.push(data);
						this.createCustomerForm.reset()
						this.setCustomerFormVisibility(false);
						this.selectedCustomer(data)
						
					},
					error: (e) => console.log(e)
				})
			}
		}
		saveProjectCustomer(customer:Customer){
			let existingCustomer = this.projectCustomers.find(c => c.customer.id == customer.id)
			let element = <HTMLInputElement>document.getElementById(`contact-${customer.id}`)
			let contact = element.value
			let data
			if(existingCustomer){
				let index = this.projectCustomers.findIndex(c => c.id == existingCustomer.id)
				this.projectCustomers.splice(index,1)
				data={customer:customer, contact:contact}
				this.projectService.updateProjectCustomer(existingCustomer.id, data).subscribe({
					next: (response) => {
						this.projectCustomers.push(response)
					},
					error: (err) => console.log(err)
				})
			} else { 
				data = {customer: customer, contact:contact}
				this.projectService.addProjectCustomer(data).subscribe({
					next: (response) => {
						this.projectCustomers.push(response)
					},
					error: (err) => {
						console.log(err)
					}
				})
			}
		}
		saveProjectCompanyCustomer(customer:Customer){
			let existingCustomer = this.projectCompanyCustomers.find(c => c.customer.id == customer.id)
			let element = <HTMLInputElement>document.getElementById(`companyContact-${customer.id}`)
			let contact = element.value
			let data
			if(existingCustomer){
				let index = this.projectCompanyCustomers.findIndex(c => c.id == existingCustomer.id)
				this.projectCompanyCustomers.splice(index,1)
				data={customer:customer, contact:contact}
				this.projectService.updateProjectCustomer(existingCustomer.id, data).subscribe({
					next: (response) => {
						this.projectCompanyCustomers.push(response)
					},
					error: (err) => console.log(err)
				})
			} else { 
				data = {customer: customer, contact:contact}
				this.projectService.addProjectCustomer(data).subscribe({
					next: (response) => {
						this.projectCompanyCustomers.push(response)
					},
					error: (err) => {
						console.log(err)
					}
				})
			}
		}
		tagFilter(event:any) {
			let searchText = event.target.value
			if(searchText != ''){
				this.filteredTags = []
				this.allTags.forEach(tag =>{
					if(tag.name.toLowerCase().includes(searchText.toLowerCase())){
						this.filteredTags.push(tag)
					}
				})
			} else {
				this.filteredTags = this.allTags
			}
		}
		referenceProjectFilter(event:any) {
			let searchText= event.target.value
			if(searchText != '') {
				this.filteredReferenceProjects = []
				this.allProjects.forEach(project => {
					if(project.title.toLowerCase().includes(searchText.toLowerCase())){
						this.filteredReferenceProjects.push(project)
					} else if (project.projectId && project.projectId.toLowerCase().includes(searchText.toLowerCase())){
						this.filteredReferenceProjects.push(project)
					}
				})
			} else {
				this.filteredReferenceProjects = this.allProjects
			}
		}
		formatCurrency(event: any){
			let pipe = new CurrencyPipe('de-CH')
			let number = event.target.value
			let currency= pipe.transform(number,'CHF')
			let field = event.target.name
			this.createProjectForm.controls[field].setValue(currency) //change Value and Display
			this.createProjectForm.controls[field].setValue(number,{emitModelToViewChange: false}) // changes only value
		} 
		addProjectTag(tag:Tag){
			this.selectedTags.push(tag)
			this.removeFromArray(this.allTags,tag.id)
			this.removeFromArray(this.filteredTags, tag.id)
		}
		removeProjectTag(tag:Tag){
			this.allTags.push(tag)
			this.filteredTags.push(tag)
			this.removeFromArray(this.selectedTags, tag.id)
		}
		addReferenceProject(project:Project){
			this.referenceProjects.push(project)
			this.removeFromArray(this.allProjects,project.id)
			this.removeFromArray(this.filteredReferenceProjects, project.id)
		}		
		removeReferenceProject(project:Project){
			this.allProjects.push(project)
			this.filteredReferenceProjects.push(project)
			this.removeFromArray(this.referenceProjects,project.id)
		}
		selectedCustomer(customer:Customer) {
			// let customer = this.customers.find(cust => cust.id == event.target.value)
			console.log("selected customer ", customer)
			this.selectedCustomers.push(customer)
			let data = {customer: customer}
			this.projectService.addProjectCustomer(data).subscribe({
				next: (response) => {
					this.projectCustomers.push(response)
				},
				error: (err) => {
					console.log(err)
				}
			})
			this.removeFromArray(this.customers,customer.id)
			this.createProjectForm.get('customer').reset('')
		}
		removeSelectedCustomer(index:number) {
			let projectCustomer = this.projectCustomers.find (c => c.customer.id == this.selectedCustomers[index].id)
			let ind = this.projectCustomers.findIndex(c => c.id == projectCustomer.id)
			console.log("pushing ", this.selectedCustomers[index])
			console.log("customers before", this.customers)
			this.customers.push(this.selectedCustomers[index])
			console.log("customers after", this.customers)
			this.selectedCustomers.splice(index,1)
			if(projectCustomer){
				this.projectService.deleteProjectCustomer(projectCustomer.id).subscribe({
					next: (response) => {}
				})
			}
			this.projectCustomers.splice(ind,1)
			this.createFilteredCustomer()
		}
		selectedCompany(event:any) {
			let company = this.customers.find(cust => cust.id == event.target.value)
			this.selectedCompanies.push(company)
			let data = {customer: company}
			this.projectService.addProjectCustomer(data).subscribe({
				next: (response) => {
					this.projectCompanyCustomers.push(response)
				},
				error: (err) => {
					console.log(err)
				}
			})
			this.removeFromArray(this.customers,company.id)
		}
		removeSelectedCompany(index:number) {
			let projectCompanyCustomer = this.projectCompanyCustomers.find(c => c.customer.id == this.selectedCompanies[index].id)
			let ind =this.projectCompanyCustomers.findIndex(c => c.id == projectCompanyCustomer.id)
			this.customers.push(this.selectedCompanies[index])
			this.selectedCompanies.splice(index,1)
			if(projectCompanyCustomer){
				this.projectService.deleteProjectCustomer(projectCompanyCustomer.id).subscribe({
					next: (response) => {}
				})
			}
			this.projectCompanyCustomers.splice(ind,1)
		}
		removeFromArray(array:any[],id:number) {
			array.forEach((el, index)=> {
				if(el.id==id){array.splice(index,1)}
			})
		}
		async searchAdress(){
			let text = this.searchText
			let url = `https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=${text}&type=locations&sr=2056&geometryFormat=geojson`
			let result =  await lastValueFrom(this.http.get(url))
			let features = new GeoJSON().readFeatures(JSON.stringify(result))
			let hiddenLayer = new VectorSource()
			hiddenLayer.addFeatures(features)
			let resultExtent=(hiddenLayer.getExtent())
			resultExtent= [resultExtent[1],resultExtent[0],resultExtent[3],resultExtent[2]] // because API answer return switched X and Y

			this.map.getView().fit(resultExtent)
		}		

		removeArea() { 
			this.geometryArray  =[]
			this.featureCounter=0
			this.geometryCollection = new GeometryCollection()
			this.vectorSource.clear()
		}
		fillData(project: Project){
			this.createProjectForm.patchValue({
				"currState": project.currState,
				"acquisitionId": project.acquisitionId,
				"projectId": project.projectId,
				"title": project.title,
				"department": project.department,
				"township": project.township,
				"client": project.client,
				"company":project.company,
				// "clientContact": project.client.,
				// "companyContact": project.companyContact,
				"offerCreator": project.offerCreator,
				// "offerCreator": this.employees.find(e => e.id == project.offerCreator),
				"offerState": project.offerState,
				"planningGroup": project.planningGroup,
				"SIAphase": project.SIAphase,
				"estimatedCosts": project.estimatedCosts,
				"acceptedCosts": project.acceptedCosts,
				"effectiveCosts": project.effectiveCosts,
				"plannedManager":project.plannedManager,
				// "plannedManager": this.employees.find(e => e.id == project.plannedManager),
				"plannedManagerDep": project.plannedManagerDep,
				// "plannedManagerDep": this.employees.find(e => e.id == project.plannedManagerDep),
				"plannedForeman": project.plannedForeman,
				// "plannedForeman": this.employees.find(e => e.id == project.plannedForeman),
				"plannedForemanDep": project.plannedForemanDep,
				// "plannedForemanDep": this.employees.find(e => e.id == project.plannedForemanDep),
				"isReferenceWeb": project.isReferenceWeb,
				"isReferenceTrainee": project.isReferenceTrainee,
				"manager":project.manager,
				"managerDep": project.managerDep,
				"foreman":project.foreman,
				"foremanDep": project.foremanDep,
				"coReferent":project.coReferent,
				"comment":project.comment,
				"creationDate": project.creationDate,
				"planningStartDate":project.planningStartDate,
				"realisationStartDate": project.realisationStartDate,
				"doneDate": project.doneDate,
				"estimatedDoneDate":project.estimatedDoneDate,
				'costCenter':project.costCenter,
			})
		}
	}
		