import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { lastValueFrom } from 'rxjs';
import { AdditionalCost } from 'src/app/models/additionalCost/additionalCost.model';
import { Customer } from 'src/app/models/customer/customer.model';
import { Department } from 'src/app/models/department/department.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { Project } from 'src/app/models/project/project.model';
import { ProjectTag } from 'src/app/models/projectTag/project-tag.model';
import { ProjectTask } from 'src/app/models/projectTask/projectTask.model';
import { Tag } from 'src/app/models/tag/tag.model';
import { User } from 'src/app/models/user/user.model';
import { AccountService } from 'src/app/services/account.service';
import { CustomerService } from 'src/app/services/customer.service';
import { ProjectService } from 'src/app/services/project.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { downloadExcel } from 'src/app/functions/downloadExcel';
@Component({
	selector: 'app-detailview',
	templateUrl: './detailview.component.html',
	styleUrls: ['./detailview.component.css']
})
export class DetailviewComponent implements OnInit {
	downloadExcel = downloadExcel
	user: User
	modalRef: NgbModalRef
	employees: Employee[]
	// departments: Department[]
	project: Project
	changeAllowed: boolean
	customers: Customer[]
	customerMap: Map<number, Customer>
	allTags: Map<number, Tag> = new Map()
	estimatedTags: Tag[] = []
	effectiveTags: Tag[] = []
	allTasks: Map<number,Task>
	additionalCosts: AdditionalCost[]
	@Output() editProjectEmitter = new EventEmitter();
	@ViewChild('showDetailView') private modalContent: TemplateRef<DetailviewComponent>

	constructor(
		// private wochenplanService: WochenplanService, 
		private modalService: NgbModal, 
		private customerService: CustomerService, 
		private projectService: ProjectService,
		private accountService: AccountService
		) { }
	
	
	ngOnInit(): void {

	}
	//set project and open Modal
	async open(project: Project, changeAllowed: boolean) {
		this.changeAllowed = changeAllowed
		this.user = this.accountService.userValue
		this.customerMap = new Map()
		// this.wochenplanService.getAllDepartments().subscribe({
		// 	next: res => this.departments=res
		// })
		// this.wochenplanService.getAllEmployees().subscribe({
		// 	next: res => this.employees= res
		// })
		this.employees = await lastValueFrom(this.projectService.getAllEmployees())
		this.customerService.getAllCustomers().subscribe({
			next: res => {
				this.customers=res
			}
		})
		this.projectService.getAllTags().subscribe({
			next: res => {
				let tags: Tag[] = res
				this.estimatedTags=[]
				this.effectiveTags=[]
				tags.forEach(tag => {
					this.allTags.set(tag.id,tag)
				})
			}
		})
		this.projectService.getAllAdditionalCosts(project.id).subscribe({
			next: res => {
				this.additionalCosts = res
			}
		})


		this.project=project;

		// this.modalRef = this.modalService.open(this.modalContent, {backdrop: 'static', scrollable: true, centered: true })
		this.modalRef = this.modalService.open(this.modalContent, { size: 'lg', backdrop: 'static', scrollable: true, centered: true })

		this.modalRef.result.then(
			() => {}, //on close
			() => {} //on dismiss
		)
	}
	close() {
		this.modalRef.close()
		// this.closeEmitter.emit("close")
	}
	// getDepartmentName(depId: number){
	// 	return this.departments?.find(dep => dep.id==depId).name
	// }
	// getCustomerName(customerId: number) {
	// 	let customer = this.customers?.find( e=> e.id == customerId)
	// 	if(customer) {
	// 		return customer?.description == null ? (customer.person.firstname + ' ' + customer.person.lastname) : customer.description 
	// 	} else {
	// 		return ''
	// 	}
	// 	// return this.customers?.find( e=> e.id == customerId)?.description
	// }
	getEmployeeName(employeeId: number) {
		let emp = this.employees?.find(emp => emp.id == employeeId)
		return emp?.firstname + ' ' + emp?.lastname
		// return this.employees?.find(emp => emp.id == employeeId).username
	}
	getTagName(tagId:number){
		this.allTags.get(tagId).name
	}
	async addToTagArray(tag: ProjectTag, estimated:boolean) {
		let tagObj = this.allTags.get(tag.tag)
		if(estimated){
			this.estimatedTags.push(tagObj)
		} else {
			this.effectiveTags.push(tagObj)
		}
	}
	changeProject(state: string,projectId: number) {
		this.editProjectEmitter.emit({state,projectId})
		this.close()
	}
	getOfferState(offerState: string): string{
		switch(offerState){
			case 'denied':
				return "Nicht erhalten"
			case 'open': 
				return "In Bearbeitung"
			case 'accepted':
				return 'Erhalten'
			default:
				return "In Bearbeitung"
		}	
	}
	getProjectState(currState: string): string {
		switch(currState) {
			case 'acquisition': 
				return "Akquisition"
			case 'planning':
				return "Planung"
			case 'realisation':
				return "Realisierung"
			case 'done':
				return "Abgeschlossen"
			default:
				return ''
		}
	}
}
