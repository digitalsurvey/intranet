import { Component, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../models/employee/employee.model';
import { WochenplanService } from '../services/wochenplan.service';
import { Task } from '../models/task/task.model';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Department } from '../models/department/department.model'
import { Company } from '../models/company/company.model';
import { Location } from '../models/location/location.model';
import { DateTime } from 'luxon';
import { FieldTaskService } from '../services/field-task.service';
import { FieldTask } from '../models/fieldTask/field-task.model';
import { lastValueFrom,} from 'rxjs';
import { ShowFeldeinsatzComponent } from '../modals/feldeinsatz/show-feldeinsatz/show-feldeinsatz.component';
import { PrintService } from '../services/print.service';
import { EditFeldeinsatzComponent } from '../modals/feldeinsatz/edit-feldeinsatz/edit-feldeinsatz.component';
import { AbsenceService } from '../services/absence.service';
import { DateService } from 	'../services/date.service';
import { AbsenceType } from '../models/absenceType/absence-type.model';
import Holidays from 'date-holidays';
import { formatDate } from '@angular/common';


@Component({
	selector: 'app-wochenplan',
	templateUrl: './wochenplan.component.html',
	styleUrls: ['./wochenplan.component.scss']
})

export class WochenplanComponent implements OnInit {
	@ViewChild('editFeldeinsatz') private editModalComponent: EditFeldeinsatzComponent
	@ViewChild('showFeldeinsatz') private showModalComponent: ShowFeldeinsatzComponent
	shownTask: Task
	dates: Date[] = []
	today: Date = new Date()
	weekNumber: number
  	displayDate: string //additional Date to display in correct locale as string for Label
	holidays: boolean[] = new Array(7).fill(false) //
	selectedDate: Date
	employees: Employee[] = []
	tasks: Task[] = []
	companies: Company[]
	locations: Location[]
	weeklyFieldTasks: FieldTask[]
	fieldTaskEmployees: Employee[]
	fieldTaskMap: Map<string, Map<string, Array<FieldTask>>> = new Map()
	tasksByEmployee: Task[] = []
	taskList: Map<string, Array<Task>> = new Map();
	departments: Department[] = []
	showTaskForm: boolean = false
	addTaskForm: boolean = false
	formData: UntypedFormGroup = new UntypedFormGroup({
		employeeKZ: new UntypedFormControl(''),
		taskName: new UntypedFormControl('', [
			Validators.minLength(2),
			Validators.required,
		]),
		description: new UntypedFormControl(''),
		taskDate: new UntypedFormControl(''),
		homeOffice: new UntypedFormControl(false), //set default to false, because Box is not checked but does not send value
	});
	depFilter: number = 0
	locFilter: number = 0
	compFilter: number = 0
	filteredDepartments:Department[]
	closeResult: string
	selectedTaskID: number
	modalRef: any
	absenceTypes: AbsenceType[]
	absenceByEmployee: Map<number, Map<Date, string>> = new Map() //<EmployeeID, <Date, absenceType>>
	
	constructor(private wochenplanService: WochenplanService,
		private modalService: NgbModal, 
		private fieldTaskService: FieldTaskService, 
		public printService: PrintService,
		private absenceService: AbsenceService,
		public dateService: DateService) {
		
	}
	
	ngOnInit() {
		this.getDates(new Date())
	}
	async loadData() {
		this.absenceTypes= await lastValueFrom(this.absenceService.getAbsenceTypes())
		this.weeklyFieldTasks= await lastValueFrom(this.fieldTaskService.getWeeklyFieldTasks(this.dates[0].toISOString().substring(0,10), this.dates[6].toISOString().substring(0,10)))
		let responseList = await lastValueFrom(this.wochenplanService.getAllData())
		this.employees = responseList[0]
		this.tasks = responseList[1]
		this.departments = responseList[2]
		this.companies = responseList[3]
		this.locations = responseList[4]
		this.getAbsences()
		this.getWeekTasksByEmployees();
		this.createFieldTaskMap()
	}
	async getAbsences(){
		this.absenceByEmployee.clear()
		for (let emp of this.employees) {
			let absenceMap: Map<Date, string> = new Map()
			//get all absences by employe and selected months
			let absence = await lastValueFrom(this.absenceService.getAbsenceByEmployee(emp.id,this.dates[0],this.dates[this.dates.length-1]))
			if (absence.length!=0){
				for (let a of absence) {
					this.dates.forEach((value,index) =>{ //for each day in the selected time
						if(a.status == 'accepted' && (value >= new Date(a.start) && value <= new Date(a.end) || this.dateService.compareDates(value,new Date(a.start)))){
							let type = this.absenceTypes.find(el => el.id == a.type.id).type
							absenceMap.set(value,type)
						}
					})
				}
			}
			this.absenceByEmployee.set(emp.id, absenceMap)
		}
	}

	getTaskByEmployee(ID: number): Task[] {
		let tasks = []
		for (let t of this.tasks) {
			if (t.employee == ID) {
				tasks.push(t)
			}
		}
		return tasks
		
	}
	getFieldTaskEmployees(taskID:number) {
		this.fieldTaskService.getSelectedEmployees(taskID).subscribe({
			next: (res) => { this.fieldTaskEmployees = res }
		})
	}
	createFieldTaskMap() {
		let arr = []
		let i = 0
		for (let da of this.dates)  { //for every Day of week
			this.holidays[i] = (!!this.dateService.checkHolidays(da))
			i++
			let fieldTasksByEmployee: Map<string, Array<FieldTask>> = new Map()
			// Mapping of username to fieldTaskArray for each user
			for (let ft of this.weeklyFieldTasks){ // for every FieldTask in this week
				if(this.compareDates(da,new Date(ft.date))) { // Check if any FieldTasks has got this day
					let operator = this.employees?.find(el => el.id == ft.operator)
					arr = fieldTasksByEmployee.get(operator.username) ? fieldTasksByEmployee.get(operator.username) : []
					arr.push(ft)
					fieldTasksByEmployee.set(operator?.username,arr)
					this.fieldTaskService.getSelectedEmployees(ft.id).subscribe({ // Get every assigned Employee of this Field Task
						next: res => {
							for (let e of res) { // For Each employee of this field Task
								// Check if already Employee has a Task 
								arr = fieldTasksByEmployee.get(e.username) ? fieldTasksByEmployee.get(e.username) : [] 
								arr.push(ft) // Add fieldTask to TaskArray
								fieldTasksByEmployee.set(e.username,arr) // Push Task arry into Map of User:fieldTaskArray
							}
						}
						
					})
				}
			}
			this.fieldTaskMap.set(formatDate(da,'yyyy-MM-dd','de-CH'),fieldTasksByEmployee) // ISO String makes YYYY-MM-DD:HH:MM:SS.GMT so only take first 10 chars to make YYYY-MM-DD like in DB
			// add the Map of fieldTasks by User to the Key of date
		}
	}
	changeWeek(direction:string) {
		let newDate = this.selectedDate
		switch(direction) {
			case 'forward':
				newDate = new Date(newDate.setDate(newDate.getDate()+7))
				break;
			case 'backward':
				newDate = new Date(newDate.setDate(newDate.getDate()-7))
				break;
		}
		this.getDates(newDate)
	}
	getDates(newDate?: Date): void {
		if (newDate === undefined) {newDate = new Date()}
		this.selectedDate = new Date(newDate)
		let selDate = newDate
		for (let i=0; i<=6;i++){
			this.dates[i] = new Date(selDate.setDate(selDate.getDate() - selDate.getDay()+1+i));
			this.dates[i].setHours(0,0,0,0)
		}
		this.weekNumber = DateTime.fromJSDate(this.dates[0]).weekNumber
		this.loadData()
	}

	compareDates(d1: Date, d2: Date): boolean {
		if(d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear() ){
			return true
		}
		return false
	}
	checkDepartments(){
		if(this.locFilter != 0){ //When location filter is set
			this.depFilter == 0
			this.filteredDepartments = [];
			for (let emp of this.employees) {
				if (emp.workplace == this.locFilter) {
					if(!this.filteredDepartments.includes(emp.department)) { //when Department is not in array 
						this.filteredDepartments.push(emp.department);
					} 
				}
			}
		}
	}
	getWeekTasksByEmployees(): void {
		for(let emp of this.employees) {
			let taskArray = []
			let tasksByEmployee = this.getTaskByEmployee(emp.id)
			let counter = 0
			for (let date of this.dates){
				for(let task of tasksByEmployee) {
					let taskDate = new Date(task.date)
					taskDate.setHours(0,0,0,0)
					if (this.compareDates(taskDate, date)) {
						taskArray[counter] = task
					}
				}
				counter++
			}
			this.taskList.set(emp.username, taskArray)
		}
	}
	clearForm(){
		this.formData.setValue({
			'taskName': '',
			'employeeKZ': '',
			'description': '',
			'taskDate': '',
			'homeOffice': false,
		})
	}
	showTask(task: Task,modal: any) : void {
		this.shownTask = task
		let username = this.employees.find(element => element.id === task.employee).username
		this.formData.setValue({
			employeeKZ: username,
			taskName: task.name,
			description: task.description,
			taskDate: task.date,
			homeOffice: task.homeOffice
		})
		this.selectedTaskID = task.id;
		this.modalRef = this.modalService.open(modal)
		this.modalRef.result.then(
			() => {}, //do on close
			() => {this.clearForm()} // do on dismiss
			)
	}
	addTask(username:  string, weekDay: number,modal: any): void {
			this.displayDate = this.dates[weekDay].toLocaleDateString('de-CH',{'day':'2-digit',"month":"2-digit",'year':'numeric'})
			let date= formatDate(this.dates[weekDay],'yyyy-MM-dd','de-CH')
			this.formData.patchValue({
				employeeKZ: username,
				taskDate: date
			})
			this.modalRef = this.modalService.open(modal)
      this.modalRef.result.then(
        () => {}, //do on close
        () => { //do on dismiss
          this.formData.markAsPristine()
          this.formData.markAsUntouched()
        }
      )

		}
		deleteTask() {
			this.wochenplanService.deleteTask(this.selectedTaskID)
			.subscribe({
				next: () => {
					this.wochenplanService.getAllData().subscribe(responseList => {
						this.employees = responseList[0]
						this.tasks = responseList[1]
						this.getWeekTasksByEmployees();
					})
				},
				error: (e) => console.log(e)
			})
			this.modalRef.close()
			this.clearForm()
			this.formData.markAsPristine()
			this.formData.markAsUntouched()
		}
		updateTask(){
			let data = this.formData.value
			data.name = this.formData.get('taskName').value
			this.wochenplanService.updateTask(this.selectedTaskID,data).subscribe({
				next: () => {
					this.wochenplanService.getAllData().subscribe(responseList => {
						this.employees=responseList[0]
						this.tasks = responseList[1]
						this.getWeekTasksByEmployees();
					})
				},
				error: (e) => {
					alert("Da ist etwas schief gegangen, bitte überpüfen Sie die Eingaben")
					console.error(e)
				}
			})
			this.modalRef.close()
			this.clearForm()
			this.formData.markAsPristine()
			this.formData.markAsUntouched()
		}

		submitTask(){
			let id = this.employees.find(element => element.username === this.formData.get('employeeKZ').value).id
			const data = {
				'name': this.formData.get('taskName').value,
				'employee': id,
				'description': this.formData.get('description').value,
				'date': this.formData.get('taskDate').value,
				'homeOffice': this.formData.get('homeOffice').value,
			}
			this.wochenplanService.createTask(data)
			.subscribe({
				next: (res) => {
					this.wochenplanService.getAllData().subscribe(responseList => {
						this.employees = responseList[0]
						this.tasks = responseList[1]
						this.getWeekTasksByEmployees();
					})
				},
				error: (e) => {
					alert("Da ist etwas schief gegangen, bitte überpüfen Sie die Eingaben")
					console.error(e)
				}
			})
      this.modalRef.close()
			this.clearForm()
			this.formData.markAsPristine()
			this.formData.markAsUntouched()
		}


		async openCreateModal() {
		  return await this.editModalComponent.open()
		}
		async openShowModal(taskID:number,event?:Event) {
			event?.stopPropagation()
			return this.showModalComponent.open(taskID)
		}
		print(){
			// let printEmployees: Employee[] = []
			// for (let emp of this.employees) {
			// 	if( (!this.depFilter || this.depFilter && emp.department === this.depFilter)&& (!this.locFilter || this.locFilter && emp.department === this.locFilter)){
			// 		printEmployees.push(emp)
			// 	}
			// }
			// this.printService
			this.printService.printDocument(this.depFilter,this.locFilter,this.dates[0].toISOString().substring(0,10))
		}
		createCopiedFieldTask() {
			this.editModalComponent.openCopied()
		}
	}
	