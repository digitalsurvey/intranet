import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { CreateComponent } from '../modals/news/create/create.component';
import { Department } from '../models/department/department.model';
import { News } from '../models/news/news.model';
import { Zuständigkeit } from '../models/zuständigkeit/zuständigkeit.model';
import { DateService } from '../services/date.service';
import { NewsService } from '../services/news.service';
import { ResponsibilityService } from '../services/responsibility.service';
import { WochenplanService } from '../services/wochenplan.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	@ViewChild('createNews') createNewsModal: CreateComponent
	
	constructor(public dateService: DateService, private newsService: NewsService, private wochenPlanService: WochenplanService, private responsibilityService: ResponsibilityService) { }
	
	news: News[]
	departments: Department[]
	newsByDepartment: Map<number, News[]> = new Map()
	responsibilities: Zuständigkeit[]
	gemeinden: string[] = []
	gemeindeFilter: string 
	filteredGemeinden: string[] = []
	ngOnInit(): void {
		this.dateService.getDates()
		this.getDepartments()
		this.reloadData()
		this.responsibilityService.getAllResponsibilities().subscribe({
			next: (data) => {
				this.responsibilities = data
				this.responsibilities.forEach(zuständigkeit => {
					if(!this.gemeinden.includes(zuständigkeit.gemeinde)){
						this.gemeinden.push(zuständigkeit.gemeinde)
					}
				})
			}
		}
		)
	}
	
	filter(text: string) {
		if(text){
			let filterValue=text.toLowerCase()
			this.filteredGemeinden = this.gemeinden.filter(gemeinde => (gemeinde.toLowerCase().includes(filterValue)))
		} else {
			this.filteredGemeinden =  this.gemeinden
		}
	}
	async getNews(){
		this.news = await lastValueFrom(this.newsService.getNews(formatDate(this.dateService.dates[0],'yyyy-MM-dd','de-CH'),formatDate(this.dateService.dates[this.dateService.dates.length-1],'yyyy-MM-dd','de-CH')))	
	}
	async getDepartments(){
		this.departments = await lastValueFrom(this.wochenPlanService.getAllDepartments())
	}
	getNewsByDepartment() {
		for (let n of this.news){
			let depID =n.department? n.department : 0
			let arr = this.newsByDepartment.get(depID)? this.newsByDepartment.get(depID) : []
			arr.push(n)
			this.newsByDepartment.set(depID, arr)
		}
	}

	async reloadData() {
		this.news=[]
		this.newsByDepartment.clear()
		await this.getNews()
		await this.getNewsByDepartment()
	}
	async changeWeek(direction: string) {
		this.dateService.changeWeek(direction)
		this.reloadData()
	}
	async dateChange(event: any) {
		this.dateService.getDates(event)
		this.reloadData()
	}
	async openCreateNews(){
		return await this.createNewsModal.open()
	}
	async deleteNews(id:number) {
		await lastValueFrom(this.newsService.deleteNews(id))
		this.reloadData()
	}
}
