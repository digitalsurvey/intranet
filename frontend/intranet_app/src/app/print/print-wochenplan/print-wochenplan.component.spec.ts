import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintWochenplanComponent } from './print-wochenplan.component';

describe('PrintWochenplanComponent', () => {
  let component: PrintWochenplanComponent;
  let fixture: ComponentFixture<PrintWochenplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintWochenplanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintWochenplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
