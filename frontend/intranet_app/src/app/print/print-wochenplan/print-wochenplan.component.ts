import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { concat, lastValueFrom } from 'rxjs';
import { Company } from 'src/app/models/company/company.model';
import { Department } from 'src/app/models/department/department.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { FieldTask } from 'src/app/models/fieldTask/field-task.model';
import { FieldTaskService } from 'src/app/services/field-task.service';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { Task } from 'src/app/models/task/task.model';
import { PrintService } from 'src/app/services/print.service';
import { Gnss } from 'src/app/models/gnss/gnss.model';
import { Tachy } from 'src/app/models/tachy/tachy.model';
import { Car } from 'src/app/models/car/car.model';
import { Location } from 'src/app/models/location/location.model';
import { DateTime } from 'luxon';
import { formatDate } from '@angular/common';
import { AbsenceService } from 'src/app/services/absence.service';
import { AbsenceType } from 'src/app/models/absenceType/absence-type.model';
import { DateService } from 'src/app/services/date.service';

@Component({
	selector: 'app-print-wochenplan',
	templateUrl: './print-wochenplan.component.html',
	styleUrls: ['./print-wochenplan.component.scss']
})
export class PrintWochenplanComponent implements OnInit {
	dates: Date[] = []
	today: Date = new Date()
	weekNumber: number
	holidays: boolean[] = new Array(7).fill(false) //

	employees: Employee[] = []
	tasks: Task[] = []
	companies: Company[]
	locations: Location[]
	weeklyFieldTasks: FieldTask[]
	fieldTaskEmployees: Employee[]
	fieldTaskMap: Map<string, Map<string, Array<FieldTask>>> = new Map()
	selectedEmployees: Map<number, Array<Employee>> = new Map()
	selectedTachys: Map<number, Array<Tachy>> = new Map()
	selectedGnss: Map<number, Array<Gnss>> = new Map()
	selectedCars: Map<number, Array<Car>> =new Map()
	tasksByEmployee: Task[] = []
	taskList: Map<string, Array<Task>> = new Map();
	departments: Department[] = []
	
	employeeList:string=''
	tachyList:string=''
	gnssList:string=''
	carList:string=''
	depFilter: number
	locFilter: number
	date: string
	absenceByEmployee: Map<number, Map<Date, string>> = new Map() //<EmployeeID, <Date, absenceType>>
	absenceTypes: AbsenceType[]


	
	constructor(route:ActivatedRoute, private fieldTaskService: FieldTaskService, private wochenplanService: WochenplanService, private printService: PrintService, 
		private dateService: DateService, private absenceService: AbsenceService) {
		this.depFilter = parseInt(route.snapshot.params['depFilter'])

		this.locFilter = parseInt(route.snapshot.params['locFilter'])
		this.date = route.snapshot.params['date']
	}
	
	async ngOnInit(): Promise<void> {
		await this.getDates(new Date(this.date))
		this.printService.onDataReady()
	}
	async loadData() {
		this.weeklyFieldTasks= await lastValueFrom(this.fieldTaskService.getReleasedWeeklyFieldTasks(formatDate(this.dates[0],'yyyy-MM-dd',"de-CH"), formatDate(this.dates[this.dates.length-1],'yyyy-MM-dd',"de-CH")))
		let responseList = await lastValueFrom(this.wochenplanService.getAllData())
		this.absenceTypes= await lastValueFrom(this.absenceService.getAbsenceTypes())

		// for (let emp of responseList[0]){
		// 	if((!this.depFilter || (this.depFilter && emp.department == this.depFilter)) &&
		// 	(!this.locFilter || (this.locFilter && emp.workplace == this.locFilter))) {
		// 		this.employees.push(emp)
		// 	}
		// }
		
		this.employees = responseList[0]
		this.tasks = responseList[1]
		this.departments = responseList[2]
		this.companies = responseList[3]
		this.locations = responseList[4]
		this.getWeekTasksByEmployees();
		this.createFieldTaskMap()
		await this.createSelectedDevicesMap()
		await this.getAbsences()

	}
	getTaskByEmployee(ID: number): Task[] {
		let tasks = []
		for (let t of this.tasks) {
			if (t.employee == ID) {
				tasks.push(t)
			}
		}
		return tasks
		
	}
	async getAbsences(){
		this.absenceByEmployee.clear()
		for (let emp of this.employees) {
			let absenceMap: Map<Date, string> = new Map()
			//get all absences by employe and selected months
			let absence = await lastValueFrom(this.absenceService.getAbsenceByEmployee(emp.id,this.dates[0],this.dates[this.dates.length-1]))
			if (absence.length!=0){
				for (let a of absence) {
					this.dates.forEach((value,index) =>{ //for each day in the selected time
						if(a.status == 'accepted' && (value >= new Date(a.start) && value <= new Date(a.end) || this.dateService.compareDates(value,new Date(a.start)))){
							let type = this.absenceTypes.find(el => el.id == a.type.id).type
							absenceMap.set(value,type)
						}
					})
				}
			}
			this.absenceByEmployee.set(emp.id, absenceMap)
		}
	}
	getFieldTaskEmployees(taskID:number) {
		this.fieldTaskService.getSelectedEmployees(taskID).subscribe({
			next: (res) => { this.fieldTaskEmployees = res }
		})
	}
	createFieldTaskMap() {
		let arr = []
		let i = 0
		for (let da of this.dates)  { //for every Day of week
			this.holidays[i] = (!!this.dateService.checkHolidays(da))
			i++
			let fieldTasksByEmployee: Map<string, Array<FieldTask>> = new Map()
			// Mapping of username to fieldTaskArray for each user
			for (let ft of this.weeklyFieldTasks){ // for every FieldTask in this week
				if(this.compareDates(da,new Date(ft.date))) { // Check if any FieldTasks has got this day
					let operator = this.employees?.find(el => el.id == ft.operator)
					arr = fieldTasksByEmployee.get(operator?.username) ? fieldTasksByEmployee.get(operator.username) : []
					arr.push(ft)
					fieldTasksByEmployee.set(operator?.username,arr)
					this.fieldTaskService.getSelectedEmployees(ft.id).subscribe({ // Get every assigned Employee of this Field Task
						next: res => {
							for (let e of res) { // For Each employee of this field Task
								// Check if already Employee has a Task 
								arr = fieldTasksByEmployee.get(e.username) ? fieldTasksByEmployee.get(e.username) : [] 
								arr.push(ft) // Add fieldTask to TaskArray
								fieldTasksByEmployee.set(e.username,arr) // Push Task arry into Map of User:fieldTaskArray
							}
						}
						
					})
				}
			}
			this.fieldTaskMap.set(formatDate(da,'yyyy-MM-dd',"de-CH"),fieldTasksByEmployee) // en-CA Locale makes Date of Forma YYYY-MM-DD like in DB
			// add the Map of fieldTasks by User to the Key of date
		}
	}
	async createSelectedDevicesMap() {
		for (let task of this.weeklyFieldTasks){
			let responseList = await lastValueFrom(this.fieldTaskService.getAllSelected(task.id))
			this.selectedTachys.set(task.id, responseList[0])
			this.selectedCars.set(task.id,responseList[1])
			this.selectedGnss.set(task.id, responseList[2])
			this.selectedEmployees.set(task.id, responseList[3])
			// console.log("tac", this.selectedTachys)
			// console.log("cars", this.selectedCars)
			// console.log("gnss", this.selectedGnss)
			// console.log("emps", this.selectedEmployees)
		}
	}
	
	async getDates(newDate?: Date): Promise<void> {
		if (newDate === undefined) {newDate = new Date()}
		let selectedDate = newDate
		for (let i=0; i<=6;i++){
			this.dates[i] = new Date(selectedDate.setDate(selectedDate.getDate() - selectedDate.getDay()+1+i));
			this.dates[i].setHours(0,0,0,0)
		}
		this.weekNumber = DateTime.fromJSDate(this.dates[0]).weekNumber
		await this.loadData()
		// this.getWeekTasksByEmployees()
		// this.createFieldTaskMap()
	}
	compareDates(d1: Date, d2: Date): boolean {
		if(d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear() ){
			return true
		}
		return false
	}
	getLocation(id:number): Location{
		return this.locations.find(el => el.id === id)
	}
	getEmployee(id:number): Employee{
		return this.employees.find(el => el.id === id)
	}
	cleanLists(){
		this.employeeList=''
		this.carList=''
		this.gnssList=''
		this.tachyList=''
	}
	addToList(string:string,type:string){
		switch(type){
			case 'employee': {
				this.employeeList = this.employeeList.length == 0 ? string : this.employeeList.concat(',',string)
				break;
			}
			case 'tachy':{
				this.tachyList = this.tachyList.length == 0 ? string : this.tachyList.concat(',',string)

				break;
			}
			case 'car':{
				this.carList = this.carList.length == 0 ? string : this.carList.concat(',',string)
				break;
			}
			case 'gnss':{
				this.gnssList = this.gnssList.length == 0 ? string : this.gnssList.concat(',',string)

				break;
			}
		}
	}
	getWeekTasksByEmployees(): void {
		for(let emp of this.employees) {
			let taskArray = []
			let tasksByEmployee = this.getTaskByEmployee(emp.id)
			let counter = 0
			for (let date of this.dates){
				for(let task of tasksByEmployee) {
					let taskDate = new Date(task.date)
					taskDate.setHours(0,0,0,0)
					if (this.compareDates(taskDate, date)) {
						taskArray[counter] = task
					}
				}
				counter++
			}
			this.taskList.set(emp.username, taskArray)
		}		
	}
}
