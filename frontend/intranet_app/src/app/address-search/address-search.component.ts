import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Feature as GJFeature, FeatureCollection, Geometry as GJGeometry } from 'geojson';
import { View } from 'ol';
import Polygon, { fromExtent } from 'ol/geom/Polygon';
import { lastValueFrom } from 'rxjs';
import { Project } from '../models/project/project.model';

@Component({
	selector: 'app-address-search',
	templateUrl: './address-search.component.html',
	styleUrls: ['./address-search.component.css']
})
export class AddressSearchComponent implements OnInit {
	
	constructor(private http: HttpClient) { }
	@Output() searchResultClickedEmitter =  new EventEmitter<Polygon>();
	searchText: string
	searchResult: GJFeature<GJGeometry, { [name: string]: any; }>[] = []
	SWISSTOPO_ORIGINS = {
		parcel: 'Parzelle',
		address: 'Adresse',
		zipcode: 'Postleitzahl',
		gg25: 'Gemeinde',
		district: 'Quartier',
		kantone: 'Kanton'
	}
	ngOnInit(): void {
	}
	async searchAdress(){
		let text = this.searchText
		if(this.searchText!=''){
			let url = `https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=${text}&type=locations&origins=zipcode,gg25,district,kantone,address,parcel&sr=2056&geometryFormat=geojson&limit=20`
			let res:any =  await lastValueFrom(this.http.get(url))
			let fCol:FeatureCollection = res
			this.searchResult = fCol.features
			this.searchResult = res.features
			this.searchResult.forEach( feature => {
				if (feature.properties['origin'] == 'parcel'){
					let regEx = /(\(CH.*\))|(\(\))/ //replaces "(CH XXXXX)" and "()"
					feature.properties['label'] = feature.properties['label'].replace(regEx,'')
				}
			})
			// let features = new GeoJSON().readFeatures(JSON.stringify(result))
			// let hiddenLayer = new VectorSource()
			// hiddenLayer.addFeatures(features)
			// let resultExtent=(hiddenLayer.getExtent())
			// resultExtent= [resultExtent[1],resultExtent[0],resultExtent[3],resultExtent[2]] // because API answer return switched X and Y
			
			// this.map.getView().fit(resultExtent)
		}
		else{
			this.searchResult=[]
		}
	}
	fitViewToSearchResult(extent: number[]) {
		let view = fromExtent(extent)
		view.scale(2)
		// let resExtent = [extent[1],extent[0],extent[3],extent[2]]
		this.searchResultClickedEmitter.emit(view)
		// this.map.getView().fit(view)
		this.searchText = ''
		this.searchResult = []
	}
	getOrigin(origin: string):string {
		let a =this.SWISSTOPO_ORIGINS[origin as keyof typeof this.SWISSTOPO_ORIGINS]
		return a ? a : origin
	}
	
}
