import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { faUser} from '@fortawesome/free-regular-svg-icons';
import { faLock} from '@fortawesome/free-solid-svg-icons';

import { AccountService } from '../../../services/account.service';
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';

@Component({ templateUrl: 'login.component.html', styleUrls: ['login.component.css'] })
export class LoginComponent implements OnInit {
    form: UntypedFormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    errorMessage: string;
	 faUser = faUser
	 faLock = faLock

    constructor(
        private route: ActivatedRoute,
        private formBuilder: UntypedFormBuilder,
        private router: Router,
        private accountService: AccountService
    ) {
        route.queryParams.subscribe(val => {
            this.returnUrl = val['/project-overview']
        })
     }

    ngOnInit() {
        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        

        this.loading = true;
        this.accountService.login(this.f['username'].value, this.f['password'].value)
            .pipe(first())
            .subscribe({
                complete: () => {
                    let returnUrl = this.returnUrl == undefined ? '/' : this.returnUrl
                    this.router.navigate(['/project-overview']).then(()=> window.location.reload());
                },
                error: (e: HttpErrorResponse) => {
                    console.log(e);
                    if (e.status == HttpStatusCode.Unauthorized) {
                        this.router.navigate(['../forgotPassword'], { 
                            relativeTo: this.route,
                            queryParams: { username: this.f['username'].value, errorMessage: e.error.message }
                        })
                    }
                    this.errorMessage = e.error.message;
                    this.loading = false;
                }
            })
    }
}