import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService } from '../../../services/account.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({ templateUrl: 'forgotPassword.component.html', styleUrls: ['forgotPassword.component.css']})
export class ForgotPasswordComponent implements OnInit {
    form: UntypedFormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    errorMessage: string;
    username: string;

    constructor(
        private route: ActivatedRoute,
        private formBuilder: UntypedFormBuilder,
        private router: Router,
        private accountService: AccountService
    ) {
        this.username = this.route.snapshot.queryParamMap.get('username')
        this.errorMessage = this.route.snapshot.queryParamMap.get('errorMessage')
     }

    ngOnInit() {
        this.form = this.formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(6)]],
            repeatPassword: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        console.log(this.form.value)

        // stop here if form is invalid
        if (this.form.invalid) {
            console.log(this.form)
            return;
        }

        // check equality of the passwords
        if (this.f['newPassword'].value != this.f['repeatPassword'].value) {
            this.errorMessage = "Passwörter sind nicht gleich."
            return;
        }

        this.loading = true;
        this.accountService.forgot(this.username, this.f['newPassword'].value)
            .pipe(first())
            .subscribe({
                complete: () => {
                    this.router.navigate(['../login'], { relativeTo: this.route })
                },
                error: (e: HttpErrorResponse) => {
                    console.log(e);
                    this.errorMessage = e.error.message;
                    this.loading = false;
                }
            })
    }
}