import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService } from '../../../services/account.service';
import { Department } from 'src/app/models/department/department.model';
import { Location } from 'src/app/models/location/location.model';
import { WochenplanService } from 'src/app/services/wochenplan.service';
import { lastValueFrom } from 'rxjs';
import { Role } from 'src/app/models/role/role.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({ templateUrl: 'register.component.html', styleUrls: ['register.component.css']})
export class RegisterComponent implements OnInit {
    form: UntypedFormGroup;
    bigLoading = false;
    loading = false;
    submitted = false;
    errorMessage: string;
    departments: Department[];
    workplaces: Location[];
    roles: Role[];

    constructor(
        private formBuilder: UntypedFormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private wochenplanService: WochenplanService
    ) { }

    ngOnInit() {
        this.bigLoading = true

        this.form = this.formBuilder.group({
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            username: ['', [Validators.required, Validators.maxLength(10)]],
            birthday: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phoneInt: ['', Validators.required],
            phoneMob: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            department: ['', [Validators.required, Validators.min(1)]],
            workplace: ['', [Validators.required, Validators.min(1)]],
            role: ['', [Validators.required, Validators.min(1)]]
        });

        this.loadData()
    }

    async loadData() {
        this.departments = await lastValueFrom(this.wochenplanService.getAllDepartments())
        this.workplaces = await lastValueFrom(this.wochenplanService.getAllLocations())
        this.roles = await lastValueFrom(this.wochenplanService.getAllRoles())
        this.bigLoading = false
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        
        this.convertDateToCorrectFormat()

        this.accountService.register(this.form.value)
            .pipe(first())
            .subscribe({
                complete: () => {
                    this.router.navigate(['../login'], { relativeTo: this.route })
                },
                error: (e: HttpErrorResponse) => {
                    console.log(e);
                    this.errorMessage = e.error.message;
                    this.loading = false
                }
            });
    }

    //Converts the date format (inside the form.birthday) accordingly.
    // From: YYYY-MM-DD
    // To: dd.mm.yyyy
    convertDateToCorrectFormat() {
        var currentDate: any[] = this.form.value.birthday.split()
        currentDate.unshift(currentDate.pop())
        currentDate.push(currentDate.shift())
        this.form.value.birthday = currentDate.join("-")
        console.log(this.form)
    }
}