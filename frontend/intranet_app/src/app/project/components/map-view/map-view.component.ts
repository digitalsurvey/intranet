import { Component, OnInit, Output } from '@angular/core';
import * as ol from 'ol';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import {register} from 'ol/proj/proj4';
import {get as getProjection} from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Draw from 'ol/interaction/Draw';
import WKT from 'ol/format/WKT'
import { containsExtent, getCenter, getTopLeft, getWidth, intersects } from 'ol/extent';
import { MapService } from 'src/app/services/map.service';
import { Overlay } from 'ol';
import { Project } from 'src/app/models/project/project.model';
import TileWMS from 'ol/source/TileWMS';
import Stroke from 'ol/style/Stroke';
import Fill from 'ol/style/Fill';
import Style, { StyleLike } from 'ol/style/Style';
import Polygon, { fromExtent } from 'ol/geom/Polygon';
import { EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RepollProjectsNotificationService } from 'src/app/services/repollProjectsNotificationService';
import { Department } from 'src/app/models/department/department.model';
import { DepartmentService } from 'src/app/services/department.service';
import WMTS, { optionsFromCapabilities } from 'ol/source/WMTS'
import WMTSCapabilities from 'ol/format/WMTSCapabilities'
import { hoverStyle, stylesByStatus, townshipStyle } from './styles';
import Cluster from 'ol/source/Cluster'
import { Point } from 'ol/geom';
import proj4 from 'proj4';
import CircleStyle from 'ol/style/Circle';
import { Text } from 'ol/style';
import { GemeindeService } from 'src/app/services/gemeinde.service';



@Component({
	selector: 'app-map-view',
	templateUrl: './map-view.component.html',
	styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {
	
	constructor(private mapService: MapService,
					private http: HttpClient, 
					private repollProjectSvc: RepollProjectsNotificationService,
					// private departmentService: DepartmentService,
					private gemeindeService: GemeindeService) { }
		@Output() projectClickedEmitter=  new EventEmitter<Project>();
		@Output() moveEmitter = new EventEmitter()
		@Output() hoverEmitter = new EventEmitter()
		container: HTMLElement
		content: HTMLElement
		closer: HTMLElement
		clickedProjects: Project[]
		map: ol.Map;
		view: View;
		defaultExtent: Polygon
		clusterSource: Cluster
		clusterLayer: any
		draw: Draw;
		visibleFeatures: number=0;
		vectorSource: VectorSource=new VectorSource()
		vectorLayer: any
		searchSource: VectorSource =new VectorSource()
		searchLayer: any
		mapExtent: any
		wkt: WKT = new WKT()
		overlay: Overlay
		popupOpen: boolean = false
		departments: Department[]
		styleMap: Map<number, StyleLike> = new Map()
		filter:boolean = false
		hovered:number = 0
		oldStyle:StyleLike



		async ngOnInit() {
			// this.departmentService.getAllDepartments().subscribe({
			// 	next: res => this.departments=res
			// })
			this.container = document.getElementById('popup');
			this.content = document.getElementById('popup-content');
			this.closer = document.getElementById('popup-closer');
		}
		async createMap() {
			proj4.defs("EPSG:2056","+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs");
			register(proj4)
			this.view = new View({
				projection: getProjection("EPSG:2056"),
				center: [2660013.54, 1185171.98],
				zoom: 1,
				extent: [2485071.58, 1075346.31,2828515.82, 1299941.79],
				maxZoom: 20
			})
			let swissTopo = new TileLayer({
				source: new TileWMS({
					url: "https://wms.geo.admin.ch/",
					params: {
						"LAYERS": "ch.swisstopo.pixelkarte-grau",
						"TILED": true,
					},					
				}),
				zIndex:0,
				maxZoom:18
				// additional parameter opacity could be set, like opacity: 0.3
			})
			let epsg2056 = getProjection("EPSG:2056")
			let resp = await fetch(`https://wmts.geo.admin.ch/EPSG/2056/1.0.0/WMTSCapabilities.xml`)
			let capablilities = new WMTSCapabilities().read(await resp.text())
			let options = optionsFromCapabilities(capablilities, {
				layer: "ch.kantone.cadastralwebmap-farbe",
				matrixSet: epsg2056
			})
			let parcels = new TileLayer({
				source: new WMTS(options),
				minZoom:17,
				opacity:1,
				zIndex:1
			})
			let townshipSource = new VectorSource()
			let townshipLayer = new VectorLayer({
				source: townshipSource,
				opacity:0.7,
				zIndex: 9,
				minZoom: 11
			})
			this.overlay = new Overlay({
				element: this.container,
				autoPan: {
					animation: {
						duration: 250,
					},
				},
			});

			this.vectorSource = new VectorSource();
			this.vectorLayer = new VectorLayer({
				source: this.vectorSource,
				zIndex:10,
				
			})
			this.map = new ol.Map({
				layers: [swissTopo, this.vectorLayer, parcels],
				target: 'ol-map',
				view: this.view,
				overlays: [this.overlay],
			})
			// this.defaultExtent = this.map.getView().calculateExtent(this.map.getSize())
			this.map.on('moveend', e => {
				console.log("movedn")
				let i=0
				if(this.filter){
					this.moveEmitter.emit("moved")
				}
				this.mapExtent = this.map.getView().calculateExtent(this.map.getSize())
				let featArray
				if(this.filter){
					featArray = this.searchSource.getFeatures()
				} else {
					featArray = this.vectorSource.getFeatures()
				}
				console.log("feat array in move", featArray)
				featArray.forEach(feat => {
					if(!this.styleMap.has(<number>feat.getId())){
						this.styleMap.set(<number>feat.getId(), feat.getStyle())
					}
					console.log("feature in moved", feat)
					let ext = feat.getGeometry().getExtent()
					if (containsExtent(ext, this.mapExtent)){ //when no project edge is inside View then set Invisble on map (empty Style) and remove from Project lsit 
						feat.setStyle(new Style({}))
						this.mapService.removeProject(<number>feat.getId())
					// }else if(intersects(this.mapExtent, ext)){
					}else if(feat.getGeometry().intersectsExtent(this.mapExtent)){
						feat.setStyle(this.styleMap.get(<number>feat.getId()))
						this.mapService.addProject(<number>feat.getId())
					} else {
						feat.setStyle(this.styleMap.get(<number>feat.getId()))
						this.mapService.removeProject(<number>feat.getId())
					}
				})
				this.repollProjectSvc.notify(null);
				this.clusterFeatures()
			})
			// this.map.on('click', (event) => {
			// 	this.mapClicked(event.coordinate)
			// })
			this.map.on('click', event => this.mapClicked(event))
		}
		hoverStyle(projectId:number){
			if(projectId==0){
				this.vectorSource.getFeatureById(this.hovered).setStyle(this.oldStyle)
				this.searchSource.getFeatureById(this.hovered).setStyle(this.oldStyle)
				this.hovered=0
				return
			}
			if(projectId != this.hovered){
				if(this.hovered){
					this.vectorSource.getFeatureById(this.hovered).setStyle(this.oldStyle)
					this.searchSource.getFeatureById(this.hovered).setStyle(this.oldStyle)
				}
				this.hovered=projectId
				let feature = this.vectorSource.getFeatureById(projectId)
				let feature2 = this.searchSource.getFeatureById(projectId)
				this.oldStyle = feature.getStyle()
				feature.setStyle(hoverStyle)
				feature2.setStyle(hoverStyle)

			}
		}
		clusterFeatures(){
			try {
				this.map.removeLayer(this.clusterLayer)
			} catch {
				console.log("could not remove clusterlayer")
			}
			this.clusterSource = new Cluster({
				distance:30,
				minDistance: 1,
				source: this.searchSource,
				geometryFunction: (feature) => {return new Point(getCenter(feature.getGeometry().getExtent()))}
			})
			this.clusterLayer = new VectorLayer({
				source: this.clusterSource,
				zIndex: 12,
				maxZoom:17,
				style: (feature) => {
					let size = feature.get('features').length;
					let style = new Style({
						image: new CircleStyle({
							radius: 15,
							stroke: new Stroke({
								color: '#fff',
							}),
							fill: new Fill({
								color: '#3399CC',
							}),
						}),
						text: new Text({
							text: size.toString(),
							fill: new Fill({
								color: '#fff',
							}),
						})
					})
					return style
				}
			})
			this.map.addLayer(this.clusterLayer)
		}
		public createFeature(dbText: any, id : number, state: string) {
			let feature = this.getWKTFromDB(dbText)
			feature.setId(id)
			feature.setStyle(stylesByStatus[state as keyof typeof stylesByStatus])
			let existingFeature = this.vectorSource.getFeatureById(id)
			if(existingFeature){
				this.vectorSource.removeFeature(existingFeature)
			}
			this.vectorSource.addFeature(feature)
			this.defaultExtent =fromExtent(this.vectorSource.getExtent())
			this.defaultExtent.scale(1.1)
			console.log("feature in creat", feature)
			if(!this.filter) {
				this.searchSource.addFeature(feature)
			}
		}
		public fitView(){
			let extent= fromExtent(this.vectorSource.getExtent())
			extent.scale(1.1)
			this.map.getView().fit(extent)
		}
		public clickedProject(project: Project){
			let f = this.getWKTFromDB(project.geolocation)
			let view = fromExtent(f.getGeometry().getExtent())
			view.scale(2)
			this.map.getView().fit(view)
		}
		public goToProjectGeolocation(projectGeolocation: string) {
			let f = this.getWKTFromDB(projectGeolocation)
			let view = fromExtent(f.getGeometry().getExtent())
			view.scale(2)
			this.map.getView().fit(view)
		}
		getWKTFromDB(wkt: string){
			let split= wkt.split(';')
			if(split.length>1) {
				return this.wkt.readFeature(split[1])
			} else {
				return this.wkt.readFeature(split)
			
			}
			
			// return this.wkt.readFeature(wkt.split(';')[1])
		}
		closePopUp() {
			this.overlay.setPosition(undefined)
			this.closer.blur()
		}
		// mapClicked(coords: number[]){
		mapClicked(evt: any) {
			
			let features = this.map.getFeaturesAtPixel(evt.pixel)
			console.log("all on click", features)
			let coords = this.map.getCoordinateFromPixel(evt.pixel);
			if(this.popupOpen){this.closePopUp()}
			this.clickedProjects= []
			this.map.forEachFeatureAtPixel(this.map.getPixelFromCoordinate(coords), (feature, layer) => {
				if (feature) {
					if (typeof feature.get('features') === 'undefined') { // not a cluster
						let proj = this.mapService.allProjects.find(proj => proj.id == feature.getId())
						if(!this.clickedProjects.includes(proj)) {
							this.clickedProjects.push(proj)
						}
					} else { // cluster
						let clusteredFeatuers: ol.Feature[] = feature.get('features')
						if(clusteredFeatuers.length > 1) {
							clusteredFeatuers.forEach((element: ol.Feature) => {
								let proj = this.mapService.allProjects.find(proj => proj.id == element.getId())
								if(!this.clickedProjects.includes(proj)) {
									this.clickedProjects.push(proj)
								}
							});
						}
						if (clusteredFeatuers.length == 1) {
							let proj = this.mapService.allProjects.find(proj => proj.id == clusteredFeatuers[0].getId())
							if(!this.clickedProjects.includes(proj)) {
								this.clickedProjects.push(proj)
							}
							}
					}
				}
			},{hitTolerance: 10})
			if (this.clickedProjects.length > 1) {
				this.overlay.setPosition(coords)
				this.popupOpen = true;
			}
			if (this.clickedProjects.length == 1){
				this.projectClickedEmitter.emit(this.clickedProjects[0])
			}
		}
		resized(){
			setTimeout(() =>{
				this.map.updateSize()
			},500)
		}
		popUpClick(project: Project){
			this.projectClickedEmitter.emit(project)
			this.clickedProject(project)
			this.closePopUp()
		}
		destroyMap() {
			this.map.setTarget(null);
			this.map = null;
		}
		addSearchResult(data: Project[], fit?: boolean) {
			this.map.removeLayer(this.vectorLayer) //remove default layer
			this.map.removeLayer(this.searchLayer) //remove existing search layer
			this.searchSource.clear()
			this.searchLayer = new VectorLayer({
				source:this.searchSource,
				zIndex:10
			})
			data.forEach(project => {
				let feature = this.getWKTFromDB(project.geolocation)
				feature.setId(project.id)
				this.searchSource.addFeature(feature)
				feature.setStyle(stylesByStatus[project.currState as keyof typeof stylesByStatus])
			})
			console.log("search features", this.searchSource.getFeatures())
			this.map.addLayer(this.searchLayer)
			if(fit && this.searchSource.getFeatures().length > 0){
				let f = fromExtent(this.searchSource.getExtent())
				f.scale(1.1)
				this.map.getView().fit(f)	
			}
		}
		resetSearchResult(){
			try {
				console.log("resetting")
				this.map.removeLayer(this.searchLayer)
				this.map.addLayer(this.vectorLayer)
			} catch {
				console.log("could not add and remove layers")
			}
		}
		getDepartmentName(depID: number){
			return this.departments.find(dep => dep.id == depID).name;
		}
		handleSearchResultClicked(view: Polygon) {
			this.map.getView().fit(view)
		}
		setDefaultView(): boolean {
			this.map.getView().fit(this.defaultExtent)
			return true
		}

	}