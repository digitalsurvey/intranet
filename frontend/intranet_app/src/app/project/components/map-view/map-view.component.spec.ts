import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, RouterModule } from '@angular/router';
import Feature from 'ol/Feature';
import { Geometry } from 'ol/geom';
import { DepartmentService } from 'src/app/services/department.service';
import { MapService } from 'src/app/services/map.service';
import { ProjectService } from 'src/app/services/project.service';
import { RepollProjectsNotificationService } from 'src/app/services/repollProjectsNotificationService';

import { MapViewComponent } from './map-view.component';

fdescribe('MapViewComponent', () => {
	let component: MapViewComponent;
	let fixture: ComponentFixture<MapViewComponent>;
	let projectData = {
	"currState": "acquisition",
	"acquisitionId": "TEST1",
	"projectId": "",
	"title": "Project at " + new Date().toLocaleTimeString(),
	"department": 1,
	"township": "ZH",
	"client": 2,
	"company": 2,
	"clientContact": "Nein",
	"companyContact": "",
	"offerCreator": 1,
	"offerState": "open",
	"planningGroup": '',
	"SIAphase": "1",
	"estimatedCosts": 123456.0,
	"acceptedCosts": 235467.0,
	"effectiveCosts": '',
	"plannedManager": 1,
	"plannedManagerDep": 14,
	"plannedForeman": 2,
	"plannedForemanDep": 15,
	"isReferenceWeb": false,
	"isReferenceTrainee": false,
	"manager": '',
	"managerDep": '',
	"foreman": '',
	"foremanDep": '',
	"coReferent": '',
	"taskDescription": "",
	"comment": "",
	"creationDate": "2022-05-09",
	"planningStartDate": '',
	"realisationStartDate": '',
	"doneDate": '',
	"estimatedDoneDate": '',
	}
	let pol1 = "SRID=2056;POLYGON((2775072.4271365884 1253477.1836139148,2775069.367705963 1253456.1075362756,2775088.9140682896 1253452.8781372826,2775091.973498915 1253474.804056762,2775072.4271365884 1253477.1836139148))"
	let pol2 = 	"SRID=2056;POLYGON((2775113.729450026 1253463.7561128382,2775112.3697030814 1253451.6883587062,2775124.2674888456 1253451.6883587062,2775127.4968878385 1253462.5663342618,2775113.729450026 1253463.7561128382))"
	let overlayPol1 = "SRID=2056;POLYGON((2775066.4782437063 1253475.9938353384,2775063.7587498175 1253465.7957332549,2775097.0725499564 1253460.696682213,2775100.8118540538 1253479.0532659634,2775068.517864123 1253483.3024751649,2775066.4782437063 1253475.9938353384))"
	let overlayPol2 ="SRID=2056;POLYGON((2775111.6898296094 1253468.685195512,2775107.610588776 1253449.1388331854,2775128.516698047 1253448.119022977,2775133.4457807206 1253468.0053220396,2775111.6898296094 1253468.685195512))"
	let overlayPol12 ="SRID=2056;POLYGON((2775065.6284018657 1253491.2909884634,2775045.912071171 1253440.8103831506,2775139.3946736027 1253432.6519014838,2775154.0119532556 1253482.7925700606,2775065.6284018657 1253491.2909884634))"
	let fit1 = [2516646.7586000003, 1075346.31, 2803380.3214, 1299941.79]
	let fit2 =  [2775112.230045739, 1253451.6883587064, 2775127.636545181, 1253463.7561128382]
	let click1 = [2775079.053258684, 1253466.0136954673] //inside pol 1
	let pixel1 = [185.890625, 195]
	let click2 = [2775118.5341699678, 1253455.367157593] //inside pol 2
	let pixel2 = [452.890625, 267]
	let click3 = [2775094.5794597506, 1253455.9586319192] //inside overlay pol 12
	let pixel3 = [290.890625, 263]
	let clickTest = [2716130.4147725897, 1270975.5183665606]
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ MapViewComponent ],
			imports: [HttpClientTestingModule, RouterModule.forRoot([])],
			providers: [MapService, RepollProjectsNotificationService, DepartmentService]
		})
		.compileComponents();
		TestBed.inject(ProjectService)
	});
	
	beforeEach(() => {
		fixture = TestBed.createComponent(MapViewComponent);
		component = fixture.componentInstance;
		component.ngOnInit()
		// fixture.detectChanges();
	});
	afterEach(() => {
		component.vectorSource.clear()
	})
	afterAll(()=> {
		component.vectorSource.clear()
	})
	
	it('should create', () => {
		expect(component).toBeTruthy();
	});
	it('should create polygon', () => {
		component.createFeature(pol1,1,'acquisition')
		expect(component.vectorSource.getFeatures().length).toBe(1)
		expect(component.vectorSource.getFeatureById(1)).toBeDefined()
	})
	it('creates all polygons', () => {
		component.createFeature(pol1,1,'acquisition')
		component.createFeature(pol2,2,'acquisition')
		component.createFeature(overlayPol1,3,'acquisition')
		component.createFeature(overlayPol2,4,'acquisition')
		component.createFeature(overlayPol12,5,'acquisition')
		expect(component.vectorSource.getFeatures().length).toBe(5)
	})
});
