import { Fill, Stroke, Style } from "ol/style"
import CircleStyle from "ol/style/Circle"

export const clickedStyle = new Style({
    stroke: new Stroke ({
        color: [0,255,0,1],
        width:3
    })
})
const stroke = new Stroke({
    color: [0,0,0,1],
    width: 2
})	
export const hoverStyle = new Style({
    fill: new Fill({
        color: '#00F'
    }),
    stroke: new Stroke({
        color: "#FF0000",
        width:4,
    }),
})

export const townshipStyle = new Style({
	fill: new Fill({
		color: [150,147,235,0.3]
	}),
	stroke: new Stroke({
		color: [0,0,255	,1],
		width: 3,
	})
})
export const stylesByStatus = {
    'acquisition': new Style({
        fill: new Fill ({
            color:[255, 204, 153,0.7]
        }),
        stroke: new Stroke({
            color:[255, 204, 153,1],
            width: 2
        })	
    }),
    'planning': new Style({
        fill: new Fill ({
            color:[134, 226, 134,0.7]
        }),
        stroke: new Stroke({
            color:[134, 226, 134,1],
            width: 2
        })			
    }),
    'realisation': new Style({
        fill: new Fill ({
            color:[102, 204, 255,0.7]
        }),
        stroke: new Stroke({
            color:[102, 204, 255,1],
            width: 2
        })		
    }),
    'done': new Style({
        fill: new Fill ({
            color: [204,204,204,0.9]
        }),
        stroke: new Stroke({
            color:[204,204,204,1],
            width: 2
        })	
    }),
}

export const stylesByDepartment = {
    'Raumplanung Umwelt Energie': new Style({
        fill: new Fill ({
            color:[238,130,238,0.7]
        }),
        stroke: stroke,
    }),
    'Vermessung & Landmanagement': new Style({
        fill: new Fill ({
            color:[255,165,0,0.7]
        }),
        stroke: stroke,
    }),
    'Geoinformatik': new Style({
        fill: new Fill ({
            color:[91,168,91,0.7]
        }),
        stroke: stroke,
    }),
    'Tragwerksplanung': new Style({
        fill: new Fill ({
            color: [173,216,230,0.7]
        }),
        stroke: stroke,
    }),
    'Gemeindebauwesen & Bauherrenvertretung': new Style({
        fill: new Fill ({
            color: [50,50,230,0.7]
        }),
        stroke: stroke,
    }),
}
export const createStyle = new Style({
	fill: new Fill({
		color: [100,100,100,0.7]
	}),
	stroke: new Stroke({
		color: "#ffcc33",
		width: 4
	}),
	image: new CircleStyle({
		radius: 5,
		fill: new Fill({
			color: "#ffcc33"
		}),
	}),
})	