import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {Project} from "../../../models/project/project.model";
import {CreateProjectComponent} from "../../../modals/project/create-project/create-project.component";
import {ProjectService} from "../../../services/project.service";
import {lastValueFrom, Observable, of, Subscription} from "rxjs";
import {
	NewProjectCreatedNotificationService,
	RepollProjectsNotificationService
} from "../../../services/repollProjectsNotificationService";
import {Employee} from "../../../models/employee/employee.model";
import {EmployeeService} from "../../../services/employee.service";
import {AgGridAngular} from "ag-grid-angular";
import {MapViewComponent} from "../map-view/map-view.component";
import {Customer} from "../../../models/customer/customer.model";
import {Department} from "../../../models/department/department.model";
import {
	CellClickedEvent,
	CellMouseOutEvent,
	CellMouseOverEvent,
	ColDef, ColumnApi, FirstDataRenderedEvent,
	GridApi,
	GridReadyEvent,
	GridSizeChangedEvent,
	RowClassRules,
	RowClickedEvent,
} from 'ag-grid-community';
import {CustomerService} from "../../../services/customer.service";
import {MapService} from "../../../services/map.service";
import {DepartmentService} from "../../../services/department.service";
import { DetailviewComponent } from 'src/app/modals/project/detailview/detailview.component';
import { PlanningComponent } from 'src/app/modals/project/planning/planning.component';
import { RealisationComponent } from 'src/app/modals/project/realisation/realisation.component';
import { DatePipe } from '@angular/common';
import { SelectStateChangeComponent } from 'src/app/modals/select-state-change/select-state-change.component';
import { DoneComponent } from 'src/app/modals/project/done/done.component';
import { User } from 'src/app/models/user/user.model';
import { AccountService } from 'src/app/services/account.service';
import {AG_GRID_LOCALE_DE} from "../../../services/translations/ag_grid_DE"
import { GemeindeService } from 'src/app/services/gemeinde.service';
import { downloadExcel } from 'src/app/functions/downloadExcel';


@Component({
	selector: 'app-projectOverview',
	templateUrl: './projectOverview.component.html',
	styleUrls: ['./projectOverview.component.scss']
})
export class ProjectOverviewComponent implements OnInit, OnDestroy {
	@ViewChild('map') displayMap: MapViewComponent;
	@ViewChild('createProject') createProjectModal : CreateProjectComponent
	@ViewChild('detailView') detailViewModal: DetailviewComponent
	@ViewChild('planProject') createPlanningModal : PlanningComponent
	@ViewChild('realizeProject') createRealisationModal : RealisationComponent
	@ViewChild('finishProject') finishProjectModal: DoneComponent
	@ViewChild('selectStateChange') selectStateChange: SelectStateChangeComponent
	@ViewChild(AgGridAngular) agGrid!: AgGridAngular;
	localeText = AG_GRID_LOCALE_DE
	private gridApi!: GridApi;
	private gridColumnApi: ColumnApi;
	user: User
	projectList: Project[] = [];
	customers: Map<number, Customer> = new Map();
	departments: Map<number, Department> = new Map();
	employees: Map<number, Employee>= new Map();
	date: Date
	emp: Employee[]
	hoveredRow:number =-1
	
	repollProjectSvcSubscription: Subscription;
	newProjectCreatedNotificationSvcSubscription: Subscription;
	
	public defaultColDef: ColDef = {
		sortable: true,
		filter: true,
		resizable: true,
	}
	
	public columnDefs: ColDef[] = [
		{ field: 'id', hide: true },
		{field: 'info', headerName:'',suppressAutoSize: false, maxWidth:40, menuTabs: [], suppressMenu: true, sortable: false,
		cellRenderer: (params:any) => {return`<em class="fa-solid fa-info"></em>`},
	},
	{field: 'edit', headerName:'', suppressAutoSize: false, maxWidth:40, suppressMenu: true, sortable: false,
	cellRenderer: (params:any) => {
		if(this.checkChangeAllowed(params.data)){
			return `<em class="fa-solid fa-pen"></em>`}
			else{
				return ''
			}
		},
		hide:true,
	},
	{field: 'excel', headerName:'', suppressAutoSize: false, maxWidth: 40, suppressMenu:true, sortable:false,
	cellRenderer: (params:any) => {
		return `<em class="fa-solid fa-file-excel"></em>`
	}
},
{ field: 'currState', hide:true, headerName: 'Status', initialSort: "desc", suppressAutoSize: true,
	getQuickFilterText: params => this.getStateString(params.data.currState),
	filterValueGetter: params => this.getStateString(params.data.currState),
	comparator: (valueA,valueB) => { //for sorting according to status whereas 1 is higher and -1 is lower
	let res
	switch(valueA){
		case 'acquisition':
		res = 1
		break;
		case 'planning':
		if(valueB == 'acquisition'){
			res = -1
		} else {
			res = 1
		}
		break;
		case 'realisation':
		if(valueB == 'realisation' || valueB =='done'){
			res= 1
		} else { 
			res= -1
		}
		break;
		case 'done':
		res= -1
		break
		default:
		res= -1
	}
	return res
}
},
{ field: 'department', headerName: 'Abteilung',
cellRenderer: (params: any) => params.value.name,
getQuickFilterText: params => params.value,
filterValueGetter: params => params.data.department.name,
},
{ field: 'title', headerName: 'Projektname', suppressSizeToFit: true},
{ field: 'township', headerName: 'Standort',
valueGetter: params => params.data.township.name
},
{field: 'tags', hide: true, 
getQuickFilterText: params => {
	let resultString = ''
	if(params.data.tags.length > 0) {
		for (let tag of params.data.tags){
			resultString= resultString.concat(',',tag.name)
		}
	}
	return resultString
},

}
];
columnTypes: any = {};
changeStateColumnDef={
	field: 'stateChange', minWidth: 180,headerName: 'Status ändern', colId: 'stateChange', sortable: false, suppressMenu:true,
	cellRenderer: (params :any) => this.stateCellRenderer(params),
	
}

rowClassRules: RowClassRules ={ //for adding classes according to status 
	"state-acquisition": 'data.currState == "acquisition"',
	"state-planning" : 'data.currState == "planning"' ,
	"state-realistation": 'data.currState == "realisation"' ,
	"state-done": 'data.currState == "done"',
	// "hovered-Project": (params) => {		
	// 	let val = params.api.getValue("id", params.node)
	// 	return(val == this.hoveredId)
	// }
}

public rowData$!: Observable<any[]>;

constructor(
	private departmentService: DepartmentService,
	private employeeService: EmployeeService,
	private repollProjectSvc: RepollProjectsNotificationService,
	private newProjectCreatedNotificationSvc: NewProjectCreatedNotificationService,
	private mapService: MapService,
	private customerService: CustomerService,
	private projectService: ProjectService,
	private accountService: AccountService,
	private gemeindeService: GemeindeService
	) { }
	
	async ngOnInit() {
		this.user = this.accountService.userValue
		this.updateEmployeeList();
		let departments = await lastValueFrom(this.departmentService.getAllDepartments())
		departments.forEach(dep => {
			this.departments.set(dep.id, dep)
		})
		let customers = await lastValueFrom(this.customerService.getAllCustomers())
		customers.forEach(customer => {
			this.customers.set(customer.id, customer)
		})
		await this.mapService.ngOnInit(false)
		await this.displayMap.createMap()
		
		this.initProjectList();
		this.repollProjectSvcSubscription = this.repollProjectSvc.obs.subscribe(() => this.updateProjectList());
		this.newProjectCreatedNotificationSvcSubscription = this.newProjectCreatedNotificationSvc.obs.subscribe(newProj => {
			this.displayMap.createFeature(newProj.geolocation,newProj.id, newProj.currState)
			this.displayMap.clickedProject(newProj);
		});
		
		this.columnTypes = {
			nonEditableColumn: { editable: false }
		};
		if(this.user.role.id == 4 || this.user.role.id == 2 || this.user.role.id == 5){ //4=projektleiter, 2 = GL, 5= Administraiton
			this.columnDefs.push(this.changeStateColumnDef)
			this.gridApi.setColumnDefs(this.columnDefs)
			this.gridColumnApi.setColumnVisible("edit",true)
			
		}
		
	}
	
	ngOnDestroy() {
		if(this.repollProjectSvcSubscription){
			this.repollProjectSvcSubscription.unsubscribe();
		}
		if(this.newProjectCreatedNotificationSvcSubscription){
			this.newProjectCreatedNotificationSvcSubscription.unsubscribe();
		}
		if(this.displayMap){
			this.displayMap.destroyMap();
			this.displayMap = null;
		}
	}
	async handleCellClick(params:any){
		switch(params.column.colId){
			case 'stateChange':
			if(this.checkChangeAllowed(params.data)){
				if(params.column.getId() =='stateChange'){
					switch(params.data.currState) {
						case 'acquisition':
						break;
						case 'planning':
						return await this.openModalRealisation(params.data.id)
						case 'realisation':
						return await this.openModalFinish(params.data.id,false)
						case 'done':
						return await this.openModalCreate(this.projectList.find(project => project.id ==params.data.id))
						default: 
						return false
					}
				}
			}
			break;
			case 'info':
			this.openDetailView(params.data)
			this.displayMap.goToProjectGeolocation(params.data.geolocation)
			break;
			case 'excel':
			downloadExcel(params.data.id)
			break;
			case 'edit':
			if(this.checkChangeAllowed(params.data)){
				switch(params.data.currState) {
					case 'acquisition':
					return await this.openModalCreate(params.data)
					break;
					case 'planning':
					return await this.openModalPlanning(params.data.id,true)
					case 'realisation':
					return await this.openModalRealisation(params.data.id,true)
					case 'done':
					return await this.openModalFinish(params.data.id, true)
					default: 
					return false
				}
			}
			break;
			default:
			this.displayMap.goToProjectGeolocation(params.data.geolocation );
		}
	}
	checkChangeAllowed(params:Project): boolean {
		
		switch(this.user.role.id) {
			case 2: //Geschäftslietung
			return true
			case 4: //Projektleitung
			if(params.department.id == this.user.department.id){
				return true
			} else {
				return false
			}
			case 5: //administration
			return true
			default:
			return false
		}
	}
	stateCellRenderer(params:any) {
		if(this.checkChangeAllowed(params.data) ){
			switch (params.data.currState) {
				case 'acquisition':
				let select  = document.createElement('select')
				select.addEventListener('change', (e) => {
					this.stateChangeSelected(select.selectedIndex,params.data.id)
				})
				select.id = "stateSelect"
				select.classList.add('btn', 'btn-secondary', 'btn-sm', 'btn-state-change')
				let o0= document.createElement('option')
				let o1 = document.createElement('option')
				let o2 = document.createElement('option')
				o0.selected
				o0.defaultSelected
				o0.innerHTML='Status wählen'
				o1.innerHTML='planen'
				o2.innerHTML="abschliessen"
				select.appendChild(o0)
				select.appendChild(o1)
				select.appendChild(o2)
				return select
				// return `<span class="btn btn-secondary btn-sm btn-state-change">Planen/Abschliessen</span>`
				case 'planning':
				return `<span class="btn btn-secondary  btn-sm btn-state-change">Realisieren</span>`
				case 'realisation':
				return `<span class="btn btn-secondary btn-sm btn-state-change">Abschluss</span>`
				case 'done':
				return `<span class="btn btn-secondary btn-sm btn-state-change">wiedereröffnen</span>`
				default: 
				return params.value
			}
		}
	}
	onGridReady(params: GridReadyEvent) {
		this.gridApi = params.api
		this.gridColumnApi = params.columnApi
	}
	
	clearSelection(): void {
		this.agGrid.api.deselectAll();
	}
	
	initProjectList(): void {
		this.mapService.getVisibleProjects().then(projects => {
			this.projectList = projects;
		}).then(() => {
			this.gridApi.setRowData(this.projectList)
			this.agGrid.api.sizeColumnsToFit();
		}).then(() => {
			this.projectList.forEach(e => {
				this.displayMap.createFeature(e.geolocation,e.id, e.currState)
			});
			this.displayMap.fitView()
		});
	}
	
	updateProjectList(): void {
		this.mapService.getVisibleProjects().then(projects => {
			this.projectList = projects;
		}).then(() => {
			this.gridApi.setRowData(this.projectList)
			this.agGrid.api.sizeColumnsToFit();
		}).then(() => {
			this.projectList.forEach(e => {
				this.displayMap?.createFeature(e.geolocation,e.id, e.currState)
			});
		});
	}
	
	updateEmployeeList(): void {
		let employees: Employee[] = []
		this.employeeService.getAllData()
		.subscribe(responseList => {
			employees = responseList[0]
			for (let employee of employees) {
				this.employees.set(employee.id,employee)
			}
		})
	}
	async openModalPlanning(projectId: number, edit?: boolean) {
		this.createPlanningModal.setProjectId(projectId);
		return await this.createPlanningModal.open(edit);
	}
	
	async openModalRealisation(projectId: number, edit?:boolean) {
		this.createRealisationModal.setProjectId(projectId);
		return await this.createRealisationModal.open(edit);
	}
	async openModalFinish(projectId: number, edit:boolean) {
		this.finishProjectModal.setProjectId(projectId)
		return await this.finishProjectModal.open(edit)
	}
	
	
	/**
	* Retrieves the string which should be displayed for a employee in table.
	* @param emplId employee ID
	*/
	getEmployeeString(emplId: number): string {
		return this.employees.get(emplId)?.username;
	}
	
	mapProjectClicked(proj: any) {
		this.openDetailView(proj)
	}
	async openDetailView(project: Project){
		let changeAllowed = this.checkChangeAllowed(project)
		await this.detailViewModal.open(project,changeAllowed)
	}
	
	async openModalCreate(project?: Project) {
		if(project){
			return await this.createProjectModal.open(project)
		} else {
			return await this.createProjectModal.open()
		}
	}
	
	getStateString(currState: string): string {
		switch (currState) {
			case 'acquisition':
			return "Akquisition"
			case 'planning':
			return "Planung"
			case 'realisation':
			return "Realisation"
			case 'done':
			return "Abgeschlossen"
			default: return ''
		}
		
	}
	
	
	/**
	* Retrieves the string which should be displayed for a department in table.
	* @param depId department ID
	*/
	getDepartmentString(depId: number): string {
		return this.departments.get(depId)?.name;
	}
	
	/**
	* Retrieves the string which should be displayed for a customer in table.
	* @param custId customer ID
	*/
	// getCustomerString(custId: number): string {
	// 	return this.customers.get(custId)?.description;
	// }
	
	async projectTableTextFilterChanged(e:any) {
		
		this.gridApi.setQuickFilter(e.target.value)
		if(this.gridApi.isQuickFilterPresent){ 
			this.displayMap.filter=true
			this.displayMap.setDefaultView()
			this.mapService.reset()
			setTimeout(() => {this.getSearchResults(true)},0) 
		} 
	}
	getSearchResults(fit?: boolean){
		let rowAmount = this.gridApi.getDisplayedRowCount()
		let rows: Project[] = []
		let row
		for (let i=0;i<rowAmount;i++){
			row=this.gridApi.getDisplayedRowAtIndex(i)
			rows.push(row.data)
		}
		this.addSearchLayer(rows,fit)
	}
	mapMovedWithFilter(){
		console.log("get serach result due to move")
	}
	addSearchLayer(searchResult: Project[],fit?: boolean){
		this.displayMap.addSearchResult(searchResult,fit)
	}
	onProjectClicked(e: RowClickedEvent) {
		this.displayMap.goToProjectGeolocation( e.data.geolocation );
	}
	async test(e: CellClickedEvent){
		alert(e.column.getId())
	}
	async stateChangeSelected(selectedOption: number, projectId:number){
		switch(selectedOption) {
			case 0: //nothing selected
			break;
			case 1: //planen
			return await this.openModalPlanning(projectId)
			case 2: //abschluss
			return await this.openModalFinish(projectId,true)
			default:
			break	
		}
	}
	async ADStateChange(selectedOption: number, projectId: number) {
		switch(selectedOption){
			case 0: //nothing
			break;
			case 1: //Projektnummer ändern
			alert("change NO")
			break;
			case 2: //rechnung erstellt
			let data={invoiced: true}
			this.projectService.patchProject(projectId, data).subscribe({
				next: async ()  => {
					await this.mapService.ngOnInit()
					this.updateProjectList()
				}
			})
			break;
			default:
			break;
		}
	}
	closeProject(projectId: number){
		let data = {
			'doneDate' : new DatePipe('en-US').transform(Date.now(), 'YYYY-MM-dd'),
			'currState' : 'done'
		}
		this.projectService.patchProject(projectId,data).subscribe({
			next: () => {
				this.updateProjectList()
				// this.repollProjectSvc.notify(null)
			},
			error: (e) => {
				alert("Projekt konnte nicht abgeschlossen werde, bitte Daten überprüfen")
				console.log(e)
			}
		})
	}	
	onFirstDataRendered(params: FirstDataRenderedEvent) {
		params.api.sizeColumnsToFit();
	}
	onGridSizeChanged(params: GridSizeChangedEvent) {
		if (document.getElementById('grid-wrapper') instanceof HTMLElement) {
			// get the current grids width
			var gridWidth = document.getElementById('grid-wrapper')!.offsetWidth;
			// keep track of which columns to hide/show
			var columnsToShow = [];
			var columnsToHide = [];
			// iterate over all columns (visible or not) and work out
			// now many columns can fit (based on their minWidth)
			var totalColsWidth = 0;
			var allColumns = params.columnApi.getAllColumns();
			if (allColumns && allColumns.length > 0) {
				for (var i = 0; i < allColumns.length; i++) {
					var column = allColumns[i];
					totalColsWidth += column.getMinWidth() || 0;
					if (totalColsWidth > gridWidth) {
						columnsToHide.push(column.getColId());
					} else {
						columnsToShow.push(column.getColId());
					}
				}
			}
			// show/hide columns based on current grid width
			params.columnApi.setColumnsVisible(columnsToShow, true);
			params.columnApi.setColumnsVisible(columnsToHide, false);
			// fill out any available space to ensure there are no gaps
			params.api.sizeColumnsToFit();
		}
	}
	async editProject(emitterEvent: any) {
		let project = this.projectList.find(p => p.id == emitterEvent.projectId)
		switch(project.currState){
			case 'acquisition':
			return await this.openModalCreate(project)
			case 'planning':
			return await this.openModalPlanning(project.id, true)
			case 'realisation':
			return await this.openModalRealisation(project.id, true)
			case 'done':
			return await this.openModalFinish(project.id, true)
		}
	}
	projectHovered(projectId:number){
		this.hoveredRow=projectId
		this.gridApi.redrawRows()
	}
	cellMouseOver(event:CellMouseOverEvent){
		if(event.rowIndex != this.hoveredRow){
			this.hoveredRow=event.rowIndex
			this.displayMap.hoverStyle(event.data.id)
		}
	}
	cellMouseOut(event:CellMouseOutEvent){
		let me = <MouseEvent>event.event
		let toTarget = <Element>me.relatedTarget
		let isCell:boolean=false
		if(toTarget){
			isCell = toTarget.classList.contains('ag-cell-value')
		}
		if(!isCell){
			this.hoveredRow=-1
			this.displayMap.hoverStyle(0)
		}
	}
	async statusChanged(event:any){
		await this.mapService.ngOnInit()
		this.updateProjectList()
	}
	filterChanged() {
		this.getSearchResults(true)
	}
	// downloadExcel(projectID:number){
	// 	const link = document.createElement('a');
	// 	link.setAttribute('target', '_self');
	// 	link.setAttribute('href', `${CREATE_SHEET_URL}/${projectID}`)
	// 	link.setAttribute('download', `products.csv`);
	// 	document.body.appendChild(link);
	// 	link.click();
	// 	link.remove();
	// }
	resetTable() {
		let searchBox = <HTMLInputElement>document.getElementById('quickFilter')
		searchBox.value=''
		this.gridApi.setQuickFilter('')
		this.gridColumnApi.resetColumnState()
		this.gridApi.setFilterModel(null)
		if(this.user.role.id == 4 || this.user.role.id == 2 || this.user.role.id == 5){ //4=projektleiter, 2 = GL, 5= Administraiton
			this.gridColumnApi.setColumnVisible("edit",true)
		}
		this.getSearchResults()
		this.displayMap.setDefaultView()
		this.mapService.reset()
		this.initProjectList()
	}
}