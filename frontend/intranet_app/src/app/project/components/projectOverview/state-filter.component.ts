import {Component} from "@angular/core";
import {IFilterAngularComp} from "ag-grid-angular";
import {IDoesFilterPassParams, IFilterParams} from "ag-grid-community";
import {UntypedFormArray} from "@angular/forms";

@Component({
    selector: 'year-component',
    template: `
        <div style="max-width: 100%; overflow: hidden;" class="p-2">
            <div>
                <p style="font-weight: bolder;"> Aktueller Projektstatus </p>
            </div>
            <div class="card p-2">
                <div class="form-check">
                    <input type="checkbox" [value]="'acquisition'" id="acquisitionCheck" class="form-check-input">
                    <label for="acquisitionCheck" class="form-check-label"> Akquisition </label>
                </div>
                <div class="form-check">
                    <input type="checkbox" [value]="'planning'" id="planningCheck" checked="" class="form-check-input">
                    <label for="planningCheck" class="form-check-label"> Projektierung </label>
                </div>
                <div class="form-check">
                    <input type="checkbox" [value]="'realisation'" id="realisationCheck" checked="" class="form-check-input">
                    <label for="realisationCheck" class="form-check-label"> Realisierung </label>
                </div>
                <div class="form-check">
                    <input type="checkbox" [value]="'done'" id="doneCheck" checked="" class="form-check-input">
                    <label for="doneCheck" class="form-check-label"> Abschluss </label>
                </div>
            </div>
        </div>
  `,
})
export class ProjectStateFilter implements IFilterAngularComp {
    params!: IFilterParams;
    filterSelected: UntypedFormArray = new UntypedFormArray([]);

    agInit(params: IFilterParams): void {
        this.params = params;
    }

    isFilterActive(): boolean {
        return this.filterSelected.length != 0;
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        console.log(this.filterSelected)
        return params.data.value === 'acquisition';
    }

    getModel() {}

    setModel(model: any) {}

    updateFilter() {
        this.params.filterChangedCallback();
    }
}
