import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Customer } from 'src/app/models/customer/customer.model';
import { Department } from 'src/app/models/department/department.model';
import { Project } from 'src/app/models/project/project.model';
import { MapViewComponent } from '../map-view/map-view.component';

import { ProjectOverviewComponent } from './projectOverview.component';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NewProjectCreatedNotificationService, RepollProjectsNotificationService } from 'src/app/services/repollProjectsNotificationService';
import { MockComponent } from 'ng-mocks'
import { Router, RouterModule } from '@angular/router';
import { DepartmentService } from 'src/app/services/department.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { MapService } from 'src/app/services/map.service';
import { CustomerService } from 'src/app/services/customer.service';
import { PlanningComponent } from 'src/app/modals/project/planning/planning.component';
import { RealisationComponent } from 'src/app/modals/project/realisation/realisation.component';

fdescribe('ProjectOverviewComponent', () => {
	let component: ProjectOverviewComponent;
	let fixture: ComponentFixture<ProjectOverviewComponent>;
	let mockMap: jasmine.SpyObj<MapViewComponent>
	mockMap = jasmine.createSpyObj("MapViewComponent", ["clickedProject"])
	let customers: Map<number, Customer> = new Map();
	let departments: Map<number, Department> = new Map();
	let depArray: Department[] = [{"id": 1, "name": "Development", "shortname": "dev", "description": "Development der ds AG", "active": true, "company": 1}, 
	{"id": 3, "name": "Vermessung", "shortname": "VM", "description": "Vermessung DS", "active": true, "company": 1}, 
	{"id": 4, "name": "3D Mobile Mapping", "shortname": "3dmm", "description": "3D Mobile Mapping", "active": true, "company": 1},
	{"id": 5, "name": "Engineering", "shortname": "eng", "description": "Engineering der DS AG", "active": true, "company": 1}, 
	{"id": 8, "name": "Lernende", "shortname": "ll", "description": "Lernende der Firma", "active": true, "company": 2}, 
	{"id": 9, "name": "Administration", "shortname": "ad", "description": "Administration der DS AG", "active": true, "company": 1}, 
]
let custArray: Customer[] = [
	{"id": 2, "person": {"id": 2, "title":"mister", "address": 2, "firstname": "Hans", "lastname": "M\u00fcller", "email": "mueller@mueller.ch", "phone": "+41787901009"}, "company": null, "type": "person", "description": "H. M\u00fcller"}, 
	{"id": 3, "person": null, "company": {"id": 2, "address": 3, "name": "Swisscom AG", "shortname": "SCM"}, "type": "company", "description": "Swisscom"}
]
let testProject1:Project={
	"id":1,
	"currState": "open",
	"acquisitionId": "AQ1",
	"projectId": 1,
	"title": "Test1",
	"department": 1,
	"township": "Bern",
	"client": [1],
	"company": [1],
	"clientContact": "Müller",
	"companyContact": "Keller",
	"offerCreator": 1,
	"offerState": "open",
	"planningGroup": 1,
	"SIAphase": "1",
	"estimatedCosts": 5.0,
	"acceptedCosts": 5.0,
	"effectiveCosts": 4.0,
	"plannedManager": 1,
	"plannedManagerDep": 1,
	"plannedForeman": 1,
	"plannedForemanDep": 1,
	"isReferenceWeb": true,
	"isReferenceTrainee": true,
	"manager": 1,
	"managerDep": 1,
	"foreman": 1,
	"foremanDep": 2,
	"coReferent": 1,
	"taskDescription": "none",
	"comment": "a",
	"creationDate": new Date(),
	"planningStartDate": new Date(),
	"realisationStartDate": new Date(),
	"doneDate": new Date(),
	"estimatedDoneDate": new Date(),
	"geolocation": "SRID=2056;MULTIPOLYGON (((2716098.500893767 1270930.068462107, 2716098.500893767 1270943.5984874626, 2716111.333495135 1270943.8774570576, 2716109.3653221307 1270948.3877354339, 2716124.848134651 1270948.5272202312, 2716126.6614370183 1270929.8362573693, 2716098.500893767 1270930.068462107)), ((2716111.1088821 1270993.0228706272, 2716105.5294902003 1270968.1945766758, 2716158.812682837 1270956.059399295, 2716165.0894987234 1270980.8876932461, 2716111.1088821 1270993.0228706272)))"
}
class fakeRepollProjectsNotificationService{unsubscribe(){}}
class fakeNewProjectCreatedNotificationService{unsubscribe(){}}

beforeEach(async () => {
	await TestBed.configureTestingModule({
		declarations: [ ProjectOverviewComponent,MapViewComponent], 
		imports: [HttpClientTestingModule, RouterModule.forRoot([])],
		providers: [
			{provide: RepollProjectsNotificationService, useValue: fakeRepollProjectsNotificationService},
			{provide: NewProjectCreatedNotificationService, useValue: fakeNewProjectCreatedNotificationService},
			DepartmentService,
			EmployeeService,
			MapService,
			CustomerService
		],
		
	})
	.compileComponents()
	depArray.forEach( dep => {
		departments.set(dep.id,dep)
	})
	custArray.forEach( cust => {
		customers.set(cust.id,cust)
	})
});

beforeEach(() => {
	fixture = TestBed.createComponent(ProjectOverviewComponent);
	component = fixture.componentInstance;
});

it('should create', () => {
	expect(component).toBeTruthy();
});
it('should get correct department name', () => {
	component.departments = departments
	expect(component.getDepartmentString(3)).toEqual("Vermessung")
	expect(component.getDepartmentString(1)).toEqual("Development")
})
it('should get correct customer name', () => {
	component.customers=customers
	expect(component.getCustomerString(2)).toEqual("H. M\u00fcller")
	expect(component.getCustomerString(3)).toEqual("Swisscom")
})
it('should try to open detail view', () => {
	let openModalSpy = spyOn(component, 'openDetailView').and.resolveTo()
	component.projectList.push(testProject1)
	component.mapProjectClicked(testProject1)
	expect(component.openDetailView).toHaveBeenCalledWith(testProject1)
	component.tableProjectClicked(testProject1.id)
	expect(component.openDetailView).toHaveBeenCalledWith(testProject1)
	expect(component.openDetailView).toHaveBeenCalledTimes(2)
})	


});