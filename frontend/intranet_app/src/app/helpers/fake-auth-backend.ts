import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../models/user/user.model';
import { environment } from 'src/environments/environment';
import { Department } from '../models/department/department.model';
import { Location } from '../models/location/location.model'
import { Role } from '../models/role/role.model';
@Injectable()
export class FakeAuthBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        let devHandler = of(null)
        .pipe(mergeMap(handleRoute))
        .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(delay(500))
        .pipe(dematerialize());

        let productionHandler = of(null)
        .pipe(mergeMap(() => next.handle(request)))

        // If environment is set to produciton = true, then call backend endpoint
        return environment.production ? productionHandler : devHandler

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users/forgotPassword') && method === 'POST':
                    return forgotPassword();
                case url.endsWith('/location') && method === 'GET':
                    return locations();
                case url.endsWith('/roles') && method === 'GET':
                    return roles();
                case url.endsWith('/department') && method === 'GET':
                    return departments();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.match(/\/users\/\d+$/) && method === 'GET':
                    return getUserById();
                case url.match(/\/users\/\d+$/) && method === 'PUT':
                    return updateUser();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }    
        }

        // route functions
        function authenticate() {
            const { username, password } = body;
            const user = users.find(x => x.username === username);
            if (!user) return error('Benutzername oder Passwort ist inkorrekt.');
            if (user.password == null) return unauthorized("Benutzername "+username+" hat kein Passwort definiert. Bitte aktualisiere entsprechend.");
            if (user.password != password) return error('Benutzername oder Passwort ist inkorrekt.');
            return ok({
                id: user.id,
                username: user.username,
                firstName: user.firstname,
                lastName: user.lastname,
                token: 'fake-jwt-token'
            })
        }

        function register() {
            const user = body

            if (users.find(x => x.username === user.username)) {
                return error('Benutzername "' + user.username + '" ist bereits vorhanden.')
            }

            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        function departments() {
            return ok(all_departments);
        }

        function locations() {
            return ok(all_locations);
        }

        function roles() {
            return ok(all_roles);
        }

        function getUsers() {
            if (!isLoggedIn()) return unauthorized("Not able to get all Users");
            return ok(users);
        }

        function getUserById() {
            if (!isLoggedIn()) return unauthorized("Not able to get specific user");

            const user = users.find(x => x.id === idFromUrl());
            return ok(user);
        }

        function forgotPassword() {
            const { username, password } = body;
            const userIndex = users.findIndex(x => x.username === username)
            if (userIndex == -1) return unauthorized("Couldn't find user in local storage")
            users[userIndex].password = password
            localStorage.setItem('users', JSON.stringify(users))
            return ok(users[userIndex]);
        }

        function updateUser() {
            if (!isLoggedIn()) return unauthorized("Not able to update user");

            let params = body;
            let user = users.find(x => x.id === idFromUrl());

            // only update password if entered
            if (!params.password) {
                delete params.password;
            }

            // update and save user
            Object.assign(user, params);
            localStorage.setItem('users', JSON.stringify(users));

            return ok();
        }

        function deleteUser() {
            if (!isLoggedIn()) return unauthorized("Not able to delete user.");

            users = users.filter(x => x.id !== idFromUrl());
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        // helper functions

        function ok(body?: any) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message: string) {
            return throwError(() => {
                const error: any = new HttpErrorResponse({status: HttpStatusCode.BadRequest, error: { message: message }})
                return error;
            })
        }

        function unauthorized(message: string) {
            return throwError(() => {
                const error: any = new HttpErrorResponse({status: HttpStatusCode.Unauthorized, error: { message: message}})
                return error;
            })
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }
}

// array in local storage for registered users
const null_password_user: User = {
    id: 0,
    firstname: "none",
    lastname: "password",
    // department: 1,
    workplace: 1,
    // role: 1,
    phoneInt: "1234",
    phoneMob: "5678",
    username: "none_password", 
    password: null,
    birthday: "1818-12-12",
    token: "",
}

const valid_dev_user: User = {
    id: 1,
    firstname: "dev",
    lastname: "dev_home",
    // department: 1,
    workplace: 1,
    // role: 1,
    phoneInt: "1234",
    phoneMob: "5678",
    username: "dev", 
    password: "123456",
    birthday: "1818-12-12",
    token: "",
}

const all_roles: Role[] = [
    {
        "id": 1,
        "role": "Mitarbeiter",
        "active": true,
    },
    {
        "id": 2,
        "role": "Geschäftsleitung",
        "active": true,
    }
]

const all_locations: Location[] = [
    {
        "id": 1,
        "name": "Burgdorf",
        "street": "Kirchbergstrasse 190",
        "zip": "3400",
        "city": "Burgdorf",
        "active": true,
        "canton": 1
    },
    {
        "id": 2,
        "name": "Langnau",
        "street": "Bädligässli 6",
        "zip": "3550",
        "city": "Langnau",
        "active": true,
        "canton": 1
    }
]

const all_departments: Department[] = [
    {
        "id": 1,
        "name": "Development",
        "shortname": "dev",
        "description": "Development der ds AG",
        "active": true,
        "company": 1
    },
    {
        "id": 3,
        "name": "Vermessung",
        "shortname": "VM",
        "description": "Vermessung DS",
        "active": true,
        "company": 1
    },
    {
        "id": 4,
        "name": "3D Mobile Mapping",
        "shortname": "3dmm",
        "description": "3D Mobile Mapping",
        "active": true,
        "company": 1
    },
    {
        "id": 5,
        "name": "Engineering",
        "shortname": "eng",
        "description": "Engineering der DS AG",
        "active": true,
        "company": 1
    },
    {
        "id": 8,
        "name": "Lernende",
        "shortname": "ll",
        "description": "Lernende der Firma",
        "active": true,
        "company": 2
    },
    {
        "id": 9,
        "name": "Administration",
        "shortname": "ad",
        "description": "Administration der DS AG",
        "active": true,
        "company": 1
    },
    {
        "id": 10,
        "name": "Messgehilfen",
        "shortname": "mg",
        "description": "Messgehilfen",
        "active": true,
        "company": 1
    }
]

let users: User[] = JSON.parse(localStorage.getItem('users')) || [null_password_user, valid_dev_user];