import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AbsencesComponent } from './absences/absences.component';
import { ManagerAbsencesComponent } from './absences/manager-absences/manager-absences.component';
import { PersonalAbsencesComponent } from './absences/personal-absences/personal-absences.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DeviceplanComponent } from './deviceplan/deviceplan.component';
import { FeldeinsatzComponent } from './feldeinsatz/feldeinsatz.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { PrintWochenplanComponent } from './print/print-wochenplan/print-wochenplan.component';
import { ProjectOverviewComponent } from './project/components/projectOverview/projectOverview.component';
import { WochenplanComponent } from './wochenplan/wochenplan.component';

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'news', component: DashboardComponent},
  {path: 'wochenplan', component: WochenplanComponent},
  {path: 'feldeinsatz', component: FeldeinsatzComponent},
  {path: 'projectManagement', component: ProjectOverviewComponent},
  {path: 'print', outlet: 'print', component: PrintLayoutComponent, children:[
	  {path: 'wochenplan/:depFilter/:locFilter/:date', component: PrintWochenplanComponent}
  ]},
  {path: 'deviceplan', component: DeviceplanComponent},
  {path: 'absences', component: AbsencesComponent},
  {path: 'personalAbsences', component: PersonalAbsencesComponent},
  {path: 'managerAbsences', component: ManagerAbsencesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
