import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalAbsencesComponent } from './personal-absences.component';

describe('PersonalAbsencesComponent', () => {
  let component: PersonalAbsencesComponent;
  let fixture: ComponentFixture<PersonalAbsencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalAbsencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalAbsencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
