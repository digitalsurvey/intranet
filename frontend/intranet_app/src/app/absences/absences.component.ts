import { Component, OnInit, ViewChild } from '@angular/core';
import { DateTime } from 'luxon';
import { lastValueFrom } from 'rxjs';
import { RequestAbsenceComponent } from '../modals/absence/request-absence/request-absence.component';
import { Department } from '../models/department/department.model';
import { Employee } from '../models/employee/employee.model';
import { Location } from '../models/location/location.model';
import { AbsenceService } from '../services/absence.service';
import { DateService } from '../services/date.service';
import { WochenplanService } from '../services/wochenplan.service';
import { ABSENCE_TYPE_NUMBER } from '../services/server.constants';
import * as xl from 'xlsx' 
import { Absence } from '../models/absence/absence.model';
import { AbsenceType } from '../models/absenceType/absence-type.model';


@Component({
	selector: 'app-absences',
	templateUrl: './absences.component.html',
	styleUrls: ['./absences.component.scss']
})
export class AbsencesComponent implements OnInit {
	@ViewChild('requestAbsenceModal') requestAbsenceModal: RequestAbsenceComponent
	holidayAbsence:AbsenceType = { //for Display Holiday Days in absence planing
		id:undefined,
		type:"Holiday",
		confirm:undefined,
		active:undefined,
		color:"#5A5A5A"

	}
	noAbsence:AbsenceType = {  //for Display no Abnsece Days in absence planing
		id:undefined,
		type:"Holiday",
		confirm:undefined,
		active:undefined,
		color:"unset"

	}
	allAbsenceTypes:AbsenceType[]
	showExcelSpinner: boolean=false
	ABSENCE_TYPE_NUMBER = ABSENCE_TYPE_NUMBER
	monthDisplayAmount: number = 3
	constructor(public dateService: DateService, public wochenPlanService: WochenplanService, private absenceService: AbsenceService) { }
	days: Date[] = []
	startDay: number //sets the weekday for the first day of the month
	daysPerMonth: {
		'name': string,
		'days': number
	}[] = []
	locations: Location[]
	departments: Department[]
	employees: Employee[]
	// daysPerWeek: Map<number, number> = new Map
	daysPerWeek: {
		'weekNumber':number,
		'days': number;
	}[] = []
	depFilter: number = 0
	locFilter: number = 0
	absenceByEmployee: Map<number, any[]>= new Map()
	// absenceByEmployee: Map<number, ({'absence':AbsenceType, 'day':string, "open":boolean} | number)[][]>
	
	ngOnInit(): void {
		//only get new Dates if no date has been selected, this will assure that a selected date stays the same when changning components
		if(this.dateService.dates.length == 0){ 
			this.dateService.getDates()
		}
		this.loadData()
	}
	async loadData(){
		this.getDays()		
		this.employees = await lastValueFrom(this.wochenPlanService.getAllEmployees())
		this.getAbsences()
		this.allAbsenceTypes = await lastValueFrom(this.wochenPlanService.getAllAbsenceTypes())
		this.wochenPlanService.getAllDepartments().subscribe({
			next: (res)=> this.departments=res
		})
		this.wochenPlanService.getAllLocations().subscribe({
			next: (res) => this.locations=res
		})
	}
	getDays() { 
		this.daysPerWeek = []
		this.daysPerMonth=[]
		this.days= []
		let currentWeek: number
		let startDate = new Date(this.dateService.selectedDate)
		startDate.setDate(1)
		currentWeek = DateTime.fromJSDate(startDate).weekNumber
		let daysInWeek = 0
		let currentMonth = startDate.getMonth()
		this.daysPerMonth.push({'name':this.dateService.selectedDate.toLocaleDateString('ch-de',{month:'long'}),'days':this.dateService.getDaysPerMonth(this.dateService.selectedDate)})
		startDate.setHours(0,0,0,0)
		//get the day of the week for the first day of the month where Sunday=0 and Saturday=6
		this.startDay = startDate.getDay() 
		let endDate =new Date(startDate.getFullYear(), startDate.getMonth()+3, startDate.getDate()-1)
		let loopDate = new Date(startDate)
		while(loopDate <= endDate) {
			daysInWeek++
			this.days.push(new Date(loopDate))
			loopDate.setDate(loopDate.getDate()+1)
			if(currentMonth != loopDate.getMonth() && this.daysPerMonth.length <this.monthDisplayAmount) {
				currentMonth = loopDate.getMonth()
				this.daysPerMonth.push({'name':loopDate.toLocaleDateString('ch-de',{month:'long'}),'days':this.dateService.getDaysPerMonth(loopDate)})
			}
			if(currentWeek != DateTime.fromJSDate(loopDate).weekNumber){
				this.daysPerWeek.push({'weekNumber':currentWeek,'days':daysInWeek})
				currentWeek = DateTime.fromJSDate(loopDate).weekNumber
				daysInWeek=0
			}
		}
		if(daysInWeek){ //push if at lest 1 day is the last week of the month, otherwise an empty week might be pushed
			this.daysPerWeek.push({'weekNumber':currentWeek,'days':daysInWeek})
		}
	}
	async getAbsences(){
		this.absenceByEmployee.clear()
		// let tempEmp = [this.employees[0],this.employees[1]]
		for (let emp of this.employees) {
		// for (let emp of tempEmp) {
			if((!this.depFilter || (this.depFilter && emp.department.id === this.depFilter)) &&
			(!this.locFilter || (this.locFilter && emp.workplace === this.locFilter))){
				let empDay:any[]  = [] // {'absence':AbsenceType,'day':string, "open":boolean}[] | number[]
				//get all absences by employe and selected months
				let absence = await lastValueFrom(this.absenceService.getAbsenceByEmployee(emp.id,this.days[0],this.days[this.days.length-1]))
				if (absence.length!=0){
					for (let a of absence) {
						this.days.forEach((value,index) =>{ //for each day in the selected time
							if(a.status != 'denied' && (value >= new Date(a.start) && value <= new Date(a.end) || this.dateService.compareDates(value,new Date(a.start)))){
								// let type  = a.status == 'open'? a.type+10:a.type// set type to 10+type to display as gradient background in overview
								empDay[index]={'absence':a.type,'day':value.getDay().toString(), "open":a.status==='open'}
							}else if (!empDay[index]) { //only set if this day has not yet an absence entry, otherwise all previously entered absences would be set to 0
								if(this.dateService.checkHolidays(value)){
									empDay[index]={'absence':this.holidayAbsence,'day':value.getDay().toString(), "open":false}
								} else {
									// empDay[index]={'absence':this.noAbsence,'day':value.getDay().toString(),"open":false}
									empDay[index]=0
								}
							}

						})
						
					}
				} else { //when no absence set holidays
					empDay = Array(this.days.length).fill(0)
					this.days.forEach((value,index) => {
						if(this.dateService.checkHolidays(value)){
							empDay[index]={'absence':this.holidayAbsence,'day':value.getDay().toString(), "open":false}
						}
					})
				}
				this.absenceByEmployee.set(emp.id, empDay)
			}
		}
	}
	async openModalRequest(){
		return await this.requestAbsenceModal.open()
	}
	
	
	async changeMonth(direction:string){
		// this.loading=true
		this.dateService.changeMonth(direction)
		await this.loadData()
		// this.getDeviceMap()
	}
	async dateChange(e: any) {
		// this.loading=true
		this.dateService.getDates(e)
		await this.loadData()
	}
	getType(obj:{'absence':AbsenceType,'day':string, "open":boolean} | number){
		if(typeof obj === 'number'){
			return "number"
		}
		else {
			return "absence"
		}
	}
	async exportExcel() {
		this.showExcelSpinner=true
		let days= []
		let currentWeek: number
		let startDate = new Date(new Date(this.dateService.selectedDate).getFullYear(),0,1)
		let endDate = new Date(new Date(this.dateService.selectedDate).getFullYear(),11,31)
		currentWeek = DateTime.fromJSDate(startDate).weekNumber
		let currentMonth = startDate.getMonth()
		let monthHeader= [''] // start with an empty value
		let weekHeader=['KW']
		monthHeader.push(startDate.toLocaleDateString('ch-de',{month:'long'}))
		weekHeader.push(currentWeek.toString())
		startDate.setHours(0,0,0,0)
		//get the day of the week for the first day of the month where Sunday=0 and Saturday=6
		//this.startDay = startDate.getDay() 
		let loopDate = new Date(startDate)
		while(loopDate <= endDate) {
			days.push(new Date(loopDate))
			loopDate.setDate(loopDate.getDate()+1)
			if(currentMonth != loopDate.getMonth() && loopDate.getFullYear() == startDate.getFullYear()) {
				currentMonth = loopDate.getMonth()
				monthHeader.push(loopDate.toLocaleDateString('de-ch',{month:'long'}))
			} else {monthHeader.push('')}
			if(currentWeek != DateTime.fromJSDate(loopDate).weekNumber && loopDate.getFullYear() == startDate.getFullYear() ){
				currentWeek = DateTime.fromJSDate(loopDate).weekNumber
				weekHeader.push(currentWeek.toString())
			} else {weekHeader.push('')}
		}

		let data = [monthHeader, weekHeader]
		let totalAbsences = Array(days.length+1).fill(0)
		totalAbsences[0]='Total'
		for (let emp of this.employees) {
			let empArray: string[] = []
			//get all absences by employee for current year
			let absences = await lastValueFrom(this.absenceService.getAbsenceByEmployee(emp.id,startDate,endDate))
			if (absences.length!=0){
				for (let a of absences) {
					days.forEach((value,index) =>{ //for each day in the selected time
						if(a.status != 'denied' && (value >= new Date(a.start) && value <= new Date(a.end) || this.dateService.compareDates(value,new Date(a.start)))){
							let type  = a.type
							empArray[index+1]=type.type
							totalAbsences[index+1]+=1
						}else if (!empArray[index+1]) { //only set if this day has not yet an absence entry, otherwise all previously entered absences would be set to 0
							empArray[index+1]=''
						}
					})
				}
			} else {
				empArray = Array(days.length).fill('')
			}
			empArray[0]=emp.username
			data.push(empArray)
		}
		data.push(totalAbsences)
		let ws: xl.WorkSheet = xl.utils.json_to_sheet(data,{skipHeader:true})


		// let el = document.getElementById("absences-table")
		// let ws: xl.WorkSheet = xl.utils.table_to_sheet(el)

		let colNumber = xl.utils.decode_range(ws['!ref']).e.c
		let colWidths:number[] = Array(colNumber).fill(2)
		colWidths[0]=4
		let colSettings:any=[]
		colWidths.forEach( el => {
			colSettings.push({wch:el})
		})
		ws['!cols']= colSettings
		let wb: xl.WorkBook = xl.utils.book_new();

		xl.utils.book_append_sheet(wb,ws,'Ferienplan')

		// for(let key in ws){
		// 	if (!key.startsWith('!')) {
		// 		ws[key].s = {
		// 			fill:{
		// 				patternType: 'solid',
		// 				bgColor: {rgb:'FF0000'},
		// 				fgColor: {rgb:'00FF00'},
		// 			}
		// 		} 
		// 	}
		// }
		// console.log(ws)

		this.showExcelSpinner=false
		xl.writeFile(wb,"Ferienplan.xlsx")
	}
}
