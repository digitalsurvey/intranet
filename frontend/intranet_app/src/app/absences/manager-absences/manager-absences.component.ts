import { Component, OnInit, ViewChild } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { RequestAbsenceComponent } from 'src/app/modals/absence/request-absence/request-absence.component';
import { DeleteComponent } from 'src/app/modals/delete/delete.component';
import { Absence } from 'src/app/models/absence/absence.model';
import { AbsenceType } from 'src/app/models/absenceType/absence-type.model';
import { Department } from 'src/app/models/department/department.model';
import { Employee } from 'src/app/models/employee/employee.model';
import { AbsenceService } from 'src/app/services/absence.service';
import { DateService } from 'src/app/services/date.service';
import { ABSENCE_SATUS } from 'src/app/services/server.constants';
import { WochenplanService } from 'src/app/services/wochenplan.service';

@Component({
  selector: 'app-manager-absences',
  templateUrl: './manager-absences.component.html',
  styleUrls: ['./manager-absences.component.scss']
})
export class ManagerAbsencesComponent implements OnInit {
	@ViewChild('requestAbsenceModal') requestAbsenceModal: RequestAbsenceComponent
	@ViewChild('deleteAbsenceModal') deleteAbsenceModal: DeleteComponent
	employees: Employee[]
	departments: Department[]
	absences: Absence[]
	empFilter: number = 0
	depFilter: number=0
	stateFilter: string= 'all'
	ABSENCE_STATUS = ABSENCE_SATUS
	types: AbsenceType[]
	constructor(private absenceSerive: AbsenceService, private dateService: DateService, private wochenplanService: WochenplanService) { }
	
	ngOnInit(): void {
		//only get new Dates if no date has been selected, this will assure that a selected date stays the same when changning components
		if(this.dateService.dates.length == 0){ 
			this.dateService.getDates()
		}
		this.loadData()  
	}
	loadData(){
		this.wochenplanService.getAllEmployees().subscribe({
			next: (response) => this.employees=response
		})
		this.wochenplanService.getAllDepartments().subscribe({
			next: (response) => this.departments=response
		})
		this.absenceSerive.getAbsenceTypes().subscribe({
			next: (response) => this.types=response
		})
		this.getAbsences()
	}
	getAbsences(){
		this.absenceSerive.getAllAbsences().subscribe({
			next: (response) => {
				this.absences=response
			}
		})
	}
	openModalRequest(){
		return this.requestAbsenceModal.open()
	}
	getEmployee(id:number): Employee {
		return this.employees?.find(el => el.id == id)
	}
	getType(id:number): string{
		return this.types?.find(el => el.id == id).type
	}
	getStatus(text:string): string {
		return ABSENCE_SATUS[text as keyof typeof ABSENCE_SATUS]
	}
	depFilterChange() {
		console.log("called", this.depFilter)
		this.depFilter==0? this.empFilter =0 :''
	}
	async updateAbsenceStatus(absenceId: number, newStatus: string){
		this.absenceSerive.updateAbsenceStatus(absenceId, newStatus).subscribe({
			error: (e) => {
				alert(e.error.message)
				this.getAbsences()
			},
			complete: () => this.getAbsences()
		})
		//this.getAbsences()
	}
	async updateRecursiveAbsenceStatus(absenceId: number, newStatus: string){
		let choosenAbsence = await lastValueFrom(this.absenceSerive.getAbsenceById(absenceId))
		let recursiveID = choosenAbsence.recursiveID? choosenAbsence.recursiveID: choosenAbsence.id
		
		this.absenceSerive.updateRecursiveAbsenceStatus(recursiveID, newStatus).subscribe({
			error: (e) => {
				alert(e.error.message)
				this.getAbsences()
			},
			complete: () => this.getAbsences()
		})
	}
	async deleteAbsence(absenceId: number){
		await lastValueFrom(this.absenceSerive.deleteAbsence(absenceId))
		this.getAbsences()
	}
	async openModalDelete(absenceId:number){
		return await this.deleteAbsenceModal.open(absenceId,"absence")
	}
	async openModalDeleteRecursive(absenceId:number){
		return await this.deleteAbsenceModal.open(absenceId,"absence",true)
	}
}
