import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerAbsencesComponent } from './manager-absences.component';

describe('ManagerAbsencesComponent', () => {
  let component: ManagerAbsencesComponent;
  let fixture: ComponentFixture<ManagerAbsencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerAbsencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerAbsencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
